SDP Team 1 Final Group Report

  Introduction

[HEY TEAM 1! Things like this in square brackets are comments of 
mine. I'll do a big find/replace before handing it in to make 
sure they are all removed. At the moment, the report could use 
more technical details. A lot of what I have is vague or a bit 
high level. Topics that need the most attention are the Vision 
and the Strategy, but it could probably all at least a bit of 
work. Hopefully this is a decent starting point though. At the 
moment, this exports to roughly 3 and a bit pages in a two-column 
pdf, but there are no graphics/tables or anything like that yet, 
so once we have all the info in here we need I will need a day or 
two to mercilessly cut it back down to five pages again. - Sean]

Throughout the entire system design project, our overall 
philosophy has been to keep things simple. Our original aim was 
to have a system which was easy to develop and run, but 
sophisticated enough in the right areas to give us a good chance 
of winning tournaments. In this report we will describe how we 
went about achieving this goal, and why we made some of the 
choices we did.

  Controller

We use the term ‘controller’ to cover the communication system 
between the physical robot and the strategy system on the 
computer. The primary priorities we identified for the controller 
were speed, efficiency and reliability, with extensibility and 
ease of use being secondary but desirable qualities. Our 
controller is originally based on that built by Team 10 of last 
year, but heavy modification was required to meet our aims.

The original controller from Team 10 was written to work with the 
brick firmware LeJOS 0.8.3, but we made the decision to upgrade 
to LeJOS 0.9.1 to take advantage of the new features [More detail 
on LeJOS in appendix if needed?]. An interpretator class called 
Bot.java was written to increase ease of use; this class allows 
simple commands like bot.kick or bot.rotate(90) to be used in the 
strategy code, which Bot.java converts to a set of commands that 
LeJOS will understand. The relevant commands are then sent to a 
server class that forwards commands over a Bluetooth link to the 
robot, where LeJOS carries out the instructions given. This 
system allows us to add new commands to the controller with ease, 
and helped ease development for those writing strategy code by 
allowing strategy code to be readable and intuitive.

[Anything else?]

  Vision

Unlike most of our system, our vision system is not written in 
Java. Instead, we chose Python as it has support for a highly 
regarded vision framework called OpenCV. Though at first OpenCV 
gave us problems with unhelpful error messages, we found a 
wrapper for the OpenCV library called SimpleCV which circumvented 
many of our issues.

Rather than having a system of sliders to fine tune our vision 
every time we need to use it, our first step is to take a current 
picture of the vision feed without the robots or ball in shot. We 
then subtract this image from every frame in the live feed, 
leaving us with a feed that only includes the robots and ball. 
From this feed, it is simple to find their positions [somehow].

[More information required about how it finds positions, 
directions, why we made the choices we did]

  Robot

The original qualities we wanted for the physical robot were 
robustness, reliability and simplicity. To this achieve this, our 
robot is heavily based upon the most successful robot produced 
last year by the previous winners, Team 11. This design simply 
has two wheels on an axis along the centre of gravity, allowing 
the weight to be distributed evenly. The wheels are placed in 
such a way that the robot can turn in place, allowing for more 
manoeuvring options. Two metal bearings at the front and back 
provide stability. There is a strong frame around the wheels 
which protects the robot against damage in matches.

Our kicker is also simple. Originally had the common “flipper” 
style kicker attached directly to the motor, but this lacked 
power. To address this, we added some gears to the kicker which 
has given it more strength.

On either side of the kicker are two touch sensors, which allow 
the robot to know when it has hit the opposing robot or a wall. 
When it detects a collision, it simply reverses a small distance 
before continuing.

We believe we achieved our goal of having a simple, reliable 
physical robot. It is sturdy enough to withstand fairly hard 
collisions unscathed, and agile enough to keep up with some of 
the smaller, faster robots. [Picture of Robot somewhere around 
here]

  Simulator

For the simulator, our original intention was to have a fully 
fuctional replacement for the real world, that could be 
controlled using the same GUI and run the same code as the real 
world robot. In this, we almost succeeded. We chose to use a 
robot simulation program called Webots, as some of us had 
familiarity with it from a course last semester. The simulator 
sends vision information to the GUI, and can run test scripts 
from the GUI, but it is not capable of running full game 
strategies. After a lot of time trying to fix this, we decided it 
was just something Webots was not capable of handling, and work 
on the simulator stopped. 

The simulator has representations of two robots, modelled very 
closely off of our real world robot, but with a GPS and compass 
attached for position and direction. It also contains the ball, 
which is actually represented by a small round third robot, to 
which we attached a GPS. The GPS and compass attachments mimic a 
perfect vision system. This is useful as the simulator was never 
intended to test the real vision system, which means we can 
concentrate on strategy testing. 

[More about the simulator? I never could get it working for 
whatever reason so this is the extent of my knowledge on it]

  Strategy

We developed and tested two strategy frameworks capable of 
picking and choosing which action to follow next. 

  Subsumption Based

  Utility Based

[Much more information needed here, including information about 
which we used, if any. If not, why not. ]

  Navigation

While we experimented with Potential Field Navigation (a method 
in which the robot is 'attracted' to the target position and 
'repelled' from obstacles to create the most efficient correct 
path), we ended up using A* based grid search. A* requires that 
the playing field be split up into a grid. Each point in the grid 
is given a “cost” based on how far it is from the goal position. 
The algorithm then searches along the path of lowest known cost, 
keeping track of how far it has travelled from the original 
starting position. As it runs, if the current path suddenly costs 
more than another path it has stored, it explores more of the 
lower path instead. If a path hits an obstacle, it is discarded. 
In this way, when a path is found that hits the goal position, it 
is sure to be an efficient. 

We initially chose A* as it is highly regarded as an efficient 
and accurate pathfinding algorithm. Our initial implementation 
worked well, but there was room for improvement. At this point we 
experimented with Potential Field Navigation, but that 
implementation was unable to match the accuracy and speed of A*. 
Potential Field Navigation was discarded, and we instead 
continued improvement on A*. 

[Did we end up using ball projection in the navigation code? If 
we did, a few sentences on that here would be good]

  GUI

In early weeks, we made the decision to spend time creating 
sophisticated development tools that would streamline the 
development process later, when our schedules would get busier. 
Therefore, we spent time developing a single GUI that launches 
and runs our entire system. At startup, the GUI gives you three 
server options: the server for sending commands to the actual 
robot; the server for working with the Webots simulator; or a 
server which sends no commands anywhere, useful for testing the 
GUI itself. Once started, the GUI has 5 tabs. 

• The ‘Game Mode’ tab controls the robot in actual tournament 
  situations. From this tab, we can do the following: select the 
  pitch on which we are playing; configure and start the vision 
  feed; tell the robot which colour tile it is and for which goal 
  it should be aiming; and finally, start the strategy, whether 
  we are playing a normal game, taking a penalty, or defending a 
  penalty.

• The ‘Development’ tab allows us to run test ‘scripts’. These 
  scripts are Java classes that we use to test ideas. These 
  scripts can be dynamically reloaded, meaning we can make 
  changes to a script, recompile that script and the changes will 
  be reflected without having to restart the whole GUI. As this 
  meant we did not have to reconnect to the robot and restart the 
  vision every time we made changes to a test script, this sped 
  up development.

• The ‘World’ tab simply tells us, in simple text, the position 
  and direction information the vision system is currently 
  returning, including the goal location.

• The ‘Draw World’ tab is a simplified graphical representation 
  of the vision feed. The path that the robot intends to take is 
  also shown, and we can click on the picture to position a fake 
  ball. This then allows us to see what path the robot would take 
  if the ball was in that position. Again, this sort of feedback 
  and testing ability sped up development as we did not have to 
  test everything using the physical robot. 

• The ‘Vision Log’ tab shows us the information that has been 
  received from the vision system in the past. [is this even 
  true? more needed? what in particular was this useful for?]

  Results and Conclusion

Probably the best indication of how well we performed is our 
tournament performance throughout the project. In the three 
friendly tournaments and the final, we placed as follows: 

1. First friendly: We made it to the final, but lost to Team 4.

2. Second friendly: We won the tournament.

3. Third Friendly: We made it to the semi final.

4. Final: We made it to the semi final, but were beaten by the 
  team that won overall.

These results seem to indicate that we were somewhat successful. 
Though we did not win the final tournament, we believe that we 
achieved our goal of making a simple, high quality system for 
developing and controlling a football playing robot. [Anything to 
add?]

Appendices

A Our Team

[Members with (optionally) overall general areas. (Just use sheet 
that was used in the presentation, maybe).]

B LeJOS (Maybe)

[If it is decided that we need more information on the advantages 
of the new LeJOS over the old.

C Pictures

[If we decide that having them throughout the main text is taking 
up too much space, though this is to be avoided if we can help 
it]

