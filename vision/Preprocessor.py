import cv
import logging

class Preprocessor:
	cropRect = (0, 45, 640, 400)

	def __init__(self, rawSize, threshold, dmatL, imatL, undistort=True, crop=None):
		self.rawSize = rawSize
		if crop:
			self.cropRect = ( self.cropRect[0], self.cropRect[1] + crop[1], self.cropRect[2], crop[3] )

                self.dmatL = dmatL
                self.imatL = imatL

		self.initMatrices()
		self.cropSize = self.cropRect[2:]
		self.use_undistortion = undistort
        
		if rawSize[0] < 600:
			self.cropSize = rawSize

		self.Idistort = cv.CreateImage(self.rawSize, cv.IPL_DEPTH_8U, 3)
		self.Icrop = cv.CreateImage(self.cropSize, cv.IPL_DEPTH_8U, 3)


	def undistort(self, frame):
		logging.debug("Undistorting a frame")

		assert frame.width == self.Idistort.width
		assert frame.height == self.Idistort.height

		cv.Undistort2(frame, self.Idistort,
                        self.Intrinsic, self.Distortion)
		return self.Idistort

	def initMatrices(self):
		"Initialise matrices for camera distortion correction."
		logging.debug("Initialising camera matrices")
		
		imat = cv.CreateMat(3,3, cv.CV_32FC1)
		dmat = cv.CreateMat(1,4, cv.CV_32FC1)

		for i in range(3):
			for j in range(3):
				imat[i,j] = self.imatL[3*i + j]

		for i in range(4):
			dmat[0,i] = self.dmatL[i]

		self.Distortion = dmat
		self.Intrinsic = imat
