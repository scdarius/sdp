from SimpleCV import *
import cv
import os
import time
import math
import sys
import getopt
import util
from Preprocessor import *
from socketClient import *
from backgroundaveraging import *
class Vision:
    
    #these are the position we will return if we can't find the objects 
    lastBallPosition = (0,0)
    lastBluePosition = (0,0)
    lastBlueDirection = (0,0)
    lastYellowPosition = (0,0)
    lastYellowDirection = (0,0)
    
    #Prepocessor object
    preproc = None

    #openCV storage
    storage = None    
    #Camera object            
    camera = None
    
    ba = None
    
    #Various frames we need to use
    frame = None
    empty_pitch = None
    ball_frame = None
    yellow_frame = None
    blue_frame = None
    debug_frame = None

    # main_pitch
    main_pitch = True
    
    yellow_thresh_min = 12
    yellow_thresh_max = 36
    blue_thresh_min = 80
    blue_thresh_max = 130

    #some constants
    CROP_POSITION = (0, 73, 640, 350)
    GAUSSIAN = [[1.0/273,4.0/273,7.0/273,4.0/273,1.0/273],
                        [4.0/273,16.0/273,26.0/273,16.0/273,4.0/273],
                        [7.0/273,26.0/273,41.0/273,26.0/273,7.0/273],
                        [4.0/273,16.0/273,26.0/273,16.0/273,4.0/273],
                        [1.0/273,4.0/273,7.0/273,4.0/273,1.0/273]]
    
    GAUSSIAN_SMALL =     [[0.01, 0.08, 0.01],
                        [0.08, 0.64, 0.08],
                        [0.01, 0.08, 0.01]]

    SHARPEN = [[0,-1,0],[-1,5,-1],[0,-1,0]]
    EMBOSS = [[-2,-1,0],[-1,1,1],[0,1,2]]
    BLUR = [[1,1,1],[1,1,1],[1,1,1]]
    LIGHTSHARPEN = [[-1,0,0],[0,2,0],[0,0,0]]
    
    def __init__(self, main_pitch = True):
        self.camera = Camera(threaded=True) 
        
        self.storage = cv.CreateMemStorage()
        self.main_pitch = main_pitch
        if main_pitch:
            yellow_thresh_min = 10
            yellow_thresh_max = 36
            blue_thresh_min = 80
            blue_thresh_max = 130
            image_name = "pitch_"
            dmatL = [-0.302536778982, -0.0321101015955, 0.00906433842779, 0.0185320283848]
            imatL = [730.170289462, 0.0, 295.626475125, 0.0, 746.796995716, 229.652107843, 0.0, 0.0, 1.0]
        else:
            yellow_thresh_min = 15
            yellow_thresh_max = 40
            blue_thresh_min = 80
            blue_thresh_max = 130
            
            image_name = "other_pitch_"
            dmatL = [ -4.4208992691,37.5099634722,-0.056793882555,0.00282809696904]
            imatL = [ 1894.60815182, 0.0, 286.560772537, 0.0,1939.78082487, 246.843903659, 0.0, 0.0, 1.0]

        self.preproc = Preprocessor([640, 480], 3, dmatL, imatL)
        empty_pitch_image_path = os.path.join(util.background_folder(), image_name + '5.bmp')
        empty_pitch_image = Image(empty_pitch_image_path)
        self.empty_pitch = self.fixFrame(empty_pitch_image)
        
        
        for i in range(30):
            self.getFrameFromCamera()
            time.sleep(0.025)
        print "Let's get the party started!"
        self.createObjectFrames()
        
    
    def fixFrame(self, frame):
        x, y, w, h = self.CROP_POSITION
        if self.main_pitch:
#            return Image(self.preproc.undistort(frame.getBitmap())).crop(x,y+15,w,h).rotate(-0.7)#these constants are not nice

            return frame.crop(x,y+15,w,h).rotate(-0.7)#these constants are not nice

            #I removed the gaussian blur
            #return Image(self.preproc.undistort(frame.getBitmap())).convolve(self.GAUSSIAN).crop(x,y,w,h)
        else:
            frame =  Image(self.preproc.undistort(frame.getBitmap()))
            #I removed the gaussian blur
            #frame =  Image(self.preproc.undistort(frame.getBitmap())).convolve(self.GAUSSIAN)
            factor = 1.085
            frame = frame.adaptiveScale((640*factor, 480*factor), fit=True).crop(x+30,y+15,w,h).rotate(0.8)#these constants are not nice
            return frame
        
    def getFrameFromCamera(self):
        frame = self.camera.getImage()
        self.frame = self.fixFrame(frame)
        
    def getFrame(self):
        return self.frame

    def connect_components(self, mask, area_threshold=100):
        def contour_iterator(contour, area_threshold):
            res = []
            while contour:
                if cv.ContourArea(contour) > area_threshold:
                    res.append( contour)
                contour = contour.h_next()
            return res
            
        tmp = cv.CreateImage(cv.GetSize(mask), 8, 1)
        element = cv.CreateStructuringElementEx(3,3,1,1,cv.CV_SHAPE_ELLIPSE)
        cv.MorphologyEx(mask, mask, tmp, element, cv.CV_MOP_CLOSE, 6)
        cv.MorphologyEx(mask, mask, tmp, element, cv.CV_MOP_OPEN, 1)
        
        storage = cv.CreateMemStorage(0)
        contours = cv.FindContours(mask, storage, cv.CV_RETR_EXTERNAL, cv.CV_CHAIN_APPROX_SIMPLE, (0,0))
        
        contourList = []
        defectsToFill = []
        contours = contour_iterator(contours, area_threshold)
        for contour in contours:
            hull = cv.ConvexHull2(contour, storage, cv.CV_CLOCKWISE, 1)
            contourList.append(hull)
            if len(contours) == 1:
                chull = cv.ConvexHull2(contour, storage, cv.CV_CLOCKWISE)
                defects = cv.ConvexityDefects(contour,chull,self.storage)
                if defects:
                    defects = sorted(defects , key = lambda(k1,k2,k3,k4):k4)
                    defectsToFill += defects[-2:]
                
        
        CVX_WHITE = cv.CV_RGB(0xff,0xff,0xff)
        CVX_BLACK= cv.CV_RGB(0,0,0)
      
        cv.Zero(mask)
        for contour in contourList:
            cv.DrawContours(mask, contour, CVX_WHITE, CVX_WHITE, -1, cv.CV_FILLED, 8)
        for defect in defectsToFill:
            cv.FillPoly(mask, [list(defect[:3])], CVX_BLACK, lineType=8, shift=0)  
        del contours
    
    def createObjectFrames(self):

        diff_frame = self.frame - self.empty_pitch
        size_IMG = self.frame.size()
        
        #surfaces we need
        green_add_blue = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        ball_frame = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        hsv = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 3 )
        hue_plane = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        blue_range = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        yellow_range = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        #green_range = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        red_obj = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        green_obj = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        blue_obj = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        white = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
        
        cv.Set(white, cv.Scalar(255))

        #split to r, g, b. The binarizations above are the slow part of our system
        cv.Split( diff_frame.getBitmap(), blue_obj, green_obj, red_obj, None )
        #binarize each chanel using adaptive thresholding
        cv.Threshold(green_obj, green_obj, -1, float(255), cv.CV_THRESH_BINARY_INV + cv.CV_THRESH_OTSU)
        cv.Threshold(blue_obj, blue_obj, -1, float(255), cv.CV_THRESH_BINARY_INV + cv.CV_THRESH_OTSU)
        cv.Threshold(red_obj, red_obj, -1, float(255), cv.CV_THRESH_BINARY_INV + cv.CV_THRESH_OTSU)
        
        #inverting them
        cv.Sub(white, green_obj, green_obj)  
        cv.Sub(white, red_obj, red_obj)
        cv.Sub(white, blue_obj, blue_obj)
        
        #figure out the ball_frame
        cv.Add(blue_obj, green_obj, green_add_blue)
        cv.Sub(red_obj, green_add_blue, ball_frame)        
        cv.Erode(ball_frame, ball_frame)
        
        #this part is isolating the green plates.
        #cv.Erode(green_obj, green_obj)
        self.connect_components(green_obj)
        cv.Erode(green_obj, green_obj)
#        cv.Add(red_obj, green_add_blue, green_add_blue)#added red objects as well. should rename to green_add_blue_and red
        green_plates_mask = Image(green_obj)
        green_plates = self.frame - green_plates_mask.invert()
        

        
        
        #we convert this green_plates to HSV and we isolate the blue and yellow hue ranges
        cv.CvtColor( green_plates.getBitmap(), hsv, cv.CV_BGR2HSV )
        cv.Split( hsv, hue_plane, None, None, None )
        cv.InRangeS(hue_plane, cv.Scalar(self.yellow_thresh_min), cv.Scalar(self.yellow_thresh_max), yellow_range)
        cv.InRangeS(hue_plane, cv.Scalar(self.blue_thresh_min), cv.Scalar(self.blue_thresh_max), blue_range)
        #cv.InRangeS(hue_plane, cv.Scalar(40), cv.Scalar(75), green_range)

        self.debug_frame = green_plates
        
#        self.debug_frame = Image(yellow_range)        
        #we prepare the yellow, ball, blue frames for recognition
        if self.main_pitch:
            pass
            #cv.Dilate(yellow_range, yellow_range)    
            y_range = cv.CreateImage( size_IMG, cv.IPL_DEPTH_8U, 1 )
            cv.And(red_obj, green_obj, y_range)
            #self.debug_frame = Image(yellow_range)
            cv.And(y_range, yellow_range, yellow_range)
            
        else:
            cv.Dilate(ball_frame, ball_frame, iterations=2)
            cv.Erode(yellow_range, yellow_range)
            cv.Erode(yellow_range, yellow_range)
            cv.Erode(blue_range, blue_range)
            cv.Erode(blue_range, blue_range)
 
        #self.debug_frame = Image(yellow_range)
        
        cv.Sub(ball_frame, yellow_range, ball_frame) #sometimes the yellow is quite red especially on the main pitch
        cv.Dilate(green_obj, green_obj)
        cv.Sub(ball_frame, green_obj, ball_frame) #sometimes the yellow is quite red especially on the main pitch
#        self.debug_frame= Image(green_obj)
        self.blue_frame = blue_range
        self.yellow_frame = yellow_range
        self.ball_frame = ball_frame

    
    def intersection(self, (point1, point2), (point3, point4)):
        """
        #Computes the intersection point of two lines.
        #A line is a tuple of two points.
        """
        x1, y1 = point1
        x2, y2 = point2
        x3, y3 = point3
        x4, y4 = point4
        x, y = (0, 0)

        up_x = (x1*y2 - y1*x2)*(x3-x4) - (x1-x2)*(x3*y4 - y3*x4)
        up_y = (x1*y2 - y1*x2)*(y3-y4) - (y1-y2)*(x3*y4 - y3*x4)

        down = float((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
        if down == 0:
            down = 0.000001
            
        
        return (up_x/down,up_y/down)
    
    def findBall(self):
        seq = cv.FindContours(self.ball_frame,self.storage,cv.CV_RETR_LIST, cv.CV_CHAIN_APPROX_SIMPLE)
        blobs = []
        while seq:
            area = cv.ContourArea(seq)
            if area > 10:
                blobs.append((area, seq))
            seq = seq.h_next()
        if blobs:
            blobs = sorted(blobs , key = lambda(k1,k2):k1, reverse=True)
            moments = cv.Moments(blobs[0][1])
            position = (moments.m10 / blobs[0][0], moments.m01 / blobs[0][0])
            self.lastBallPosition = position
        else:
            print('Ball could not be found. Returning previous position. Solve this please!')
        return self.lastBallPosition


    def findRobot(self, rcolor = 'b'):
        """
        We use the find countours function from OpenCV to get the T shapes.
        This will return as a tree like structure which and we will walk through it, and always select the largest
        blob as being the robot. Once we have this countour we can get the centroid of the Tshape and the direction 
        point using the convexity defects.
        """
        if rcolor == 'b':
            robot_frame = self.blue_frame
        else:
            robot_frame = self.yellow_frame
        seq = cv.FindContours(robot_frame,self.storage,cv.CV_RETR_LIST, cv.CV_CHAIN_APPROX_SIMPLE)
        blobs = []
        while seq:
            area = cv.ContourArea(seq)
            if area > 10:
                blobs.append((area, seq))
            seq = seq.h_next()
        if blobs:
            blobs = sorted(blobs , key = lambda(k1,k2):k1, reverse=True)
            hull = cv.ConvexHull2(blobs[0][1],self.storage)
            defects = cv.ConvexityDefects(blobs[0][1],hull,self.storage)
            defects = sorted(defects , key = lambda(k1,k2,k3,k4):k4)
            if len(defects) > 1:
                line1 = (defects[-1][0], defects[-1][1])
                line2 = (defects[-2][0], defects[-2][1])
                direction = self.intersection(line1, line2)
                moments = cv.Moments(blobs[0][1])
                position = (moments.m10 / blobs[0][0], moments.m01 / blobs[0][0])
            else:
                if rcolor == 'b':
                    print 'Can not find convexity deffects for the blue robot.'
                    position = self.lastBluePosition
                    direction = self.lastBlueDirection
                else:
                    print 'Can not find convexity deffects for the yellow robot.'
                    position = self.lastYellowPosition
                    direction = self.lastYellowDirection
            if rcolor == 'b':        
                self.lastBluePosition = position
                self.lastBlueDirection = direction
            else:
                self.lastYellowPosition = position
                self.lastYellowDirection = direction
            del hull
            del blobs
            del defects    
            return position, direction
        else:
            if rcolor == 'b':
                print('Blue Robot could not be found. Returning previous position. Solve this please!')
                return self.lastBluePosition, self.lastBlueDirection
            else:
                print('Yellow Robot could not be found. Returning previous position. Solve this please!')
                return self.lastYellowPosition, self.lastYellowDirection
    '''        
    def findCorners(self):
        r, g, b = self.empty_pitch.splitChannels()
        pitch = g.binarize()
        lines = pitch.findBlobs()
        for l in lines:
            l.draw(color=Color.RED)
        self.debug_frame = pitch
    '''
    def close(self):
        self.isDone = True

def find_goals():
    return [(0, 61),(244, 61)]
    
def fix_position(point, center):
    ratio = 1.076
    return ((point[0] - center[0])*ratio + center[0] , (point[1] - center[1])*ratio + center[1])
    
def fix_coordinates(point, c1, c3):
    nw = 244
    nh = 122
    
    scalex = nw / float(c3[0] - c1[0])
    scaley = nh / float(c3[1] - c1[1])
    return ((point[0]-c1[0]) * scalex, (point[1]-c1[1]) * scaley)

def main():

    socket_on = True
    second_pitch = False
    #parse the arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hsp", ["help", "socket_off", "secondary_pitch"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            print '-s (--socket_off) for starting the script without enabling the socket client.' 
            print '-p (--secondary_pitch) for starting the script for the secondary pitch.'
            sys.exit(0)
        if o in ("-s", "--socket_off"):
            socket_on = False
        if o in ("-p", "--secondary_pitch"):
            second_pitch = True

    #the actual vision
    timer = time.time
    if second_pitch:
        vision = Vision(main_pitch = False)
    else:
        vision = Vision(main_pitch = True)
    
    x, y, w, h = vision.CROP_POSITION
    display = Display(resolution=(w, h),title='SDP Group1')
    min_fps = 100000
    
    if socket_on:
        socket = SocketClient()    

    goals =find_goals()
    fps_text = 'FPS: no'
    corners = []
    while not display.isDone():
        _t0 = timer()
        if display.mouseLeft and display.mouseRight:
            corners.append((display.mouseX, display.mouseY))
        vision.getFrameFromCamera()
        display_frame = vision.getFrame()
        
        
        if vision.main_pitch:
            corner1 = (15,12)
            corner2 = (15,330)
            corner3 = (610,330)
            corner4 = (610,12)
        else:
            corner1 = (22,20)
            corner2 = (22,325)
            corner3 = (622,325)
            corner4 = (622,20)
                
        display_frame.dl().line(corner1, corner2, width = 1, color = Color.ORANGE)
        display_frame.dl().line(corner2, corner3, width = 1, color = Color.ORANGE)
        display_frame.dl().line(corner3, corner4, width = 1, color = Color.ORANGE)
        display_frame.dl().line(corner1, corner4, width = 1, color = Color.ORANGE)
        
        
        
        vision.createObjectFrames()
        ball = vision.findBall()
        blue_pos, blue_dir = vision.findRobot(rcolor = 'b')
        yellow_pos, yellow_dir = vision.findRobot(rcolor = 'y')
        
        
        if vision.main_pitch:
            center = ((corner3[0] - corner1[0])/2 , (corner3[1] - corner1[1])/2)
        else:
            center = (282, 163) #the camera is not placed above the center of the pitch
        fixed_ball = fix_position(ball, center)    
        
        fixed_c1 = fix_position(corner1, center)
        fixed_c3 = fix_position(corner3, center)
        
        fix_fixed_ball = fix_coordinates(fixed_ball, fixed_c1, fixed_c3) 
        fix_blue_pos = fix_coordinates(blue_pos, fixed_c1, fixed_c3)
        fix_blue_dir = fix_coordinates(blue_dir, fixed_c1, fixed_c3)
        fix_yellow_pos = fix_coordinates(yellow_pos, fixed_c1, fixed_c3)
        fix_yellow_dir = fix_coordinates(yellow_dir, fixed_c1, fixed_c3)
        
        if socket_on:
            socket.send(fix_fixed_ball[0], fix_fixed_ball[1], fix_blue_pos[0], fix_blue_pos[1], fix_blue_dir[0], fix_blue_dir[1], fix_yellow_pos[0], fix_yellow_pos[1], fix_yellow_dir[0], fix_yellow_dir[1], goals[0][0], goals[0][1], goals[1][0], goals[1][1])
        
        #draw stuff on the frame we want to display
        display_frame[ball[0], 0:h]=Color.RED
        display_frame[0:w, ball[1]]=Color.RED
        if fixed_ball[0]<w and fixed_ball[0] > 0 and fixed_ball[1] <h and fixed_ball[1] > 0:
            display_frame[fixed_ball[0], 0:h]=Color.CYAN
            display_frame[0:w, fixed_ball[1]]=Color.CYAN
#        display_frame[blue_pos[0], 0:h]=Color.BLUE
#        display_frame[0:w, blue_pos[1]]=Color.BLUE
#        display_frame[yellow_pos[0], 0:h]=Color.YELLOW
#        display_frame[0:w, yellow_pos[1]]=Color.YELLOW
        display_frame.dl().line(blue_pos, blue_dir, width = 3, color = Color.ORANGE)
        display_frame.dl().line(yellow_pos, yellow_dir, width = 3, color = Color.BLUE)
        display_frame.dl().text(fps_text, (10, 2), color=Color.RED)

        display_frame.save(display)
        _t1 = timer()
        fps = 1/(_t1-_t0)
        fps_text = 'FPS: '+ str(fps)[:5]
    vision.close()
    
if __name__ == "__main__":
    thePath = os.path.dirname(sys.argv[0])
    if thePath != '':
        os.chdir(thePath)  
    
    main()

