from SimpleCV import *
import time
import os
import sys
import getopt
import util

def main():
	second_pitch = False
	#parse the arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hp", ["help", "secondary_pitch"])
	except getopt.error, msg:
		print msg
		print "for help use --help"
		sys.exit(2)
	# process options
	for o, a in opts:
		if o in ("-h", "--help"):
			print '-p (--secondary_pitch) for starting the script for the secondary pitch.'
			sys.exit(0)
		if o in ("-p", "--secondary_pitch"):
			second_pitch = True
	sleep_time = 0.01 
	if second_pitch:
		filename = "other_pitch_"
	else:
		filename = "pitch_"
        util.create_specialised_background_folder()
	save_name = os.path.join(util.specialised_background_folder(), filename)
	camera = Camera()
	for i in range(30):
		frame = camera.getImage()
		time.sleep(sleep_time)
	for i in range(6):
		frame = camera.getImage()
		frame.save(save_name + str(i) + '.bmp')
		time.sleep(sleep_time)
	
	
if __name__ == "__main__":
	thePath = os.path.dirname(sys.argv[0])
	if thePath != '':
		os.chdir(thePath)  
	
	main()
