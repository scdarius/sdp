from socket import gethostname
import os

background_folder_base = "/afs/inf.ed.ac.uk/group/teaching/sdp/sdp1/backgrounds"

def specialised_background_folder():
        hostname = gethostname()
        ret = os.path.join(background_folder_base, hostname)
        return ret

def create_specialised_background_folder():
        sp = specialised_background_folder()
        if not os.path.exists(sp):
                os.makedirs(sp)

def background_folder():
        sp = specialised_background_folder()
        if os.path.exists(sp):
                return sp
        else:
                return background_folder_base

