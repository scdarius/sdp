from SimpleCV import *
import cv

c = Camera()
d = Display()
unfixed = c.getImage()
c.loadCalibration("MyCam")
while not d.isDone():
	unfixed = c.getImage()
	fixed = c.undistort(unfixed)
	fixed.save(d)

