from SimpleCV import *
import cv
import os
import time
import math
import thread
from socketClient import *

class Vision:
	lastBallPosition = None
	
	lastBluePosition = None
	lastBlueDirection = None
	
	lastYellowPosition = None
	lastYellowDirection = None
	
	camera = None
	weAre = None
	frame = None
	ball_frame = None
	yellow_frame = None
	blue_frame = None
	isDone = True
	
	YELLOW = (122,124,88)
	BLUE = (51,111,146)
	RED = (232,38,21)
	
	def __init__(self):
		#add some nice try except to see if we can get the camera
		self.camera = Camera() 
		
		self.YELLOW = (122,124,88)
		self.BLUE = (51,111,146)
		self.RED = (232,38,21)
		
		self.lastBallPosition = (0,0)
		self.lastBluePosition = (0,0)
		self.lastBlueDirection = (0,0)
		self.lastYellowPosition = (0,0)
		self.lastYellowDirection = (0,0)
		
		self.isDone = False
		thread.start_new_thread(self.getFrameFromCamera, ()) #starting a new thread that gets the frames
		
	def euclidianDistance(self, point1, point2):
		"""
		#Computes the Euclidian distance of two points. 
		#A point is a tuple of two 'numbers' (x,y).
		"""
		x1, y1 = point1
		x2, y2 = point2
		return math.sqrt( (x1-x2) * (x1-x2) + (y1-y2) * (y1-y2))
	
	def intersection(self, (point1, point2), (point3, point4)):
		"""
		#Computes the intersection point of two lines.
		#A line is a tuple of two points.
		"""
		x1, y1 = point1
		x2, y2 = point2
		x3, y3 = point3
		x4, y4 = point4
		x, y = (0, 0)
	
		up = (x4-x3)*(y1-y2) - (y4 - y3) * (x1 - x3)
		down = (y4 - y3)*(x2 - x1) - (x4-x3) * (y2-y1)
		if down == 0:
			down = 0.000001
		ua = up/down
		x = x1 + ua * (x2 - x1)
		y = y1 + ua * (y2 - y1)
		return (x,y)
	

	def getDirectionPointFromConvexHull(self, hull):
		"""
		#Computes a point that together with the center of the blob gives us the direction.
		#This point is computed from the intersection of the longest lines  between 
		#consecutive points in the convex hull.
		"""
		point1 = (0,0)
		point2 = (0,0)
		point3 = (0,0)
		point4 = (0,0)
		hull2 = hull[1:] + [hull[0]] #we create a second list of point, 'rotated' by one so we can check consecutive points
		max_dist = 0
		last_max_dist = 0
	
		for pointA, pointB in zip(hull, hull2): #we loop between pairs of consecutive points to find the longes segments
			dist = self.euclidianDistance(pointA, pointB)
			if dist > max_dist:
				last_max_dist = max_dist
				point3 = point1
				point4 = point2
				point1 = pointA
				point2 = pointB
				max_dist = dist
			else:
				if dist > last_max_dist:
					last_max_dist = dist
					point3 = pointA
					point4 = pointB
		#the longest lines will be (point1, point2) and (point3, point4)
		return self.intersection((point1, point2), (point3, point4))
	
	def getFrameFromCamera(self):
		while not self.isDone:
			self.frame = self.camera.getImage()
			time.sleep(0.03)
			
	def getFrame(self):
		return self.frame.copy()		#i'm not sure if we want to return a copy as it might slow us down
	
	def close(self):
		self.isDone = True
	
	def findBall(self):
	
		no_color = self.frame.colorDistance(self.RED)
		self.ball_frame = (self.frame - no_color).smooth().binarize().dilate(1).erode()
		
		blobs =	self.ball_frame.binarize().findBlobs()
		if blobs:
			self.lastBallPosition = blobs[-1].center()
			return blobs[-1].center() #blobs are sorted by size so we want 
		else:
			print('Ball could not be found. Returning previous position. Solve this please!')
			return self.lastBallPosition

	def findBlueRobot(self):
		no_color = self.frame.colorDistance(self.BLUE)
		self.blue_frame = (self.frame - no_color).smooth().binarize().dilate(1).erode()
		
		blobs =	self.blue_frame.binarize().findBlobs()
		if blobs : 
			position = blobs[-1].center()
			direction = self.getDirectionPointFromConvexHull(blobs[-1].mConvexHull)
			self.lastBluePosition = position
			self.lastBlueDirection = direction
			return position, direction
		else:
			print('Blue Robot could not be found. Returning previous position. Solve this please!')
			return self.lastBluePosition, self.lastBlueDirection
			
	def findYellowRobot(self):
		no_color = self.frame.colorDistance(self.YELLOW)
		self.yellow_frame = (self.frame - no_color).smooth().binarize().dilate(2).erode()
		
		blobs =	self.yellow_frame.binarize().findBlobs()
		if blobs : 
			position = blobs[-1].center()
			direction = self.getDirectionPointFromConvexHull(blobs[-1].mConvexHull)
			self.lastYellowPosition = position
			self.lastYellowDirection = direction
			return position, direction
		else:
			print('Blue Yellow could not be found. Returning previous position. Solve this please!')
			return self.lastYellowPosition, self.lastYellowDirection


def find_goals(robot_pos, size=(640, 480)):
	#the goals are not accurate
	(x,y)= size
	(us_x,us_y)=robot_pos
	if(abs(us_x-0)<abs(us_x-x)):
		return [(0, y/2),(x, y/2)]
	else:
		return[(x,y/2),(0,y/2)]

if __name__ == "__main__":
	vision = Vision()
	display = Display()
	socket = SocketClient()
	
	
	while not display.isDone():
		ball = vision.findBall()
		blue_pos, blue_dir = vision.findBlueRobot()
		yellow_pos, yellow_dir = vision.findYellowRobot()
		
		goals =find_goals(blue_pos)
		our_goal=goals[0]
		their_goal=goals[1]
		
		socket.send(ball[0], ball[1], blue_pos[0], blue_pos[1], blue_dir[0], blue_dir[1], yellow_pos[0], yellow_pos[1], yellow_dir[0], yellow_dir[1], our_goal[0], our_goal[1], their_goal[0], their_goal[1])
		
		display_frame = vision.getFrame()
		
		display_frame[ball[0], 0:480]=Color.RED
		display_frame[0:640, ball[1]]=Color.RED
		
		display_frame[blue_pos[0], 0:480]=Color.BLUE
		display_frame[0:640, blue_pos[1]]=Color.BLUE
		
		display_frame[yellow_pos[0], 0:480]=Color.YELLOW
		display_frame[0:640, yellow_pos[1]]=Color.YELLOW
		
		display_frame.dl().line(blue_pos, blue_dir, width = 2, color = Color.RED)
		display_frame.dl().line(yellow_pos, yellow_dir, width = 2, color = Color.RED)
		
		display_frame.save(display)
	
	vision.close()
	socket.close()
