package scripts;

import strategy.AccessorMethods;
import strategy.Script;

public class ActionSpeedTest extends AccessorMethods implements Script {
	public int run(String param) {
		long time = System.currentTimeMillis();
		moveForward();
		print("MF: " + (System.currentTimeMillis() - time));
		return 50;
	}

}
