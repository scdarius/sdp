package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import strategy.world.WorldState;
import strategy.BallProjection;
import strategy.BallMonitor;
import commander.ControllerGUI;
import strategy.world.WorldState;
import strategy.world.Ball;

public class Bounce extends AccessorMethods implements Script {
	public int run(String param) {	
		ControllerGUI.clear();
		Tile[] bounceTiles = getBounceTiles(); 
		Tile[] upperBouncePath = {WorldState.ball.getLocation(), bounceTiles[0], oppGoal.getLocation()};
		Tile[] lowerBouncePath = {WorldState.ball.getLocation(), bounceTiles[1], oppGoal.getLocation()};
		ControllerGUI.setPath(upperBouncePath);
		ControllerGUI.setPath(lowerBouncePath);
		return 100;
    }
}
