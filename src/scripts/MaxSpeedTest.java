package scripts;

import strategy.AccessorMethods;
import strategy.Script;

/**
* Prints out the locations of all the game objects every 4 seconds.
*
*/

public class MaxSpeedTest extends AccessorMethods implements Script {
	public int run(String param) {
		int speedMax = bot.getSpeedMax();
		print("Max speed: " + speedMax);
		print("Going at max speed");
		bot.setSpeed(speedMax);
		bot.travel(20);
		print("Going at half speed");
		bot.setSpeed(speedMax/2);
		bot.travel(20);
		return -1;
	}
}
