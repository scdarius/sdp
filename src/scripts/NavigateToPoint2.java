package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;

public class NavigateToPoint2 extends AccessorMethods implements Script {
	Tile t = new Tile(100, 100);
	NavigateToPoint np = new NavigateToPoint();
	public int run(String param) {
	
		moveTo(t);
		return 500;
	}
}
