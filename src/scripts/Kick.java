package scripts;

import strategy.AccessorMethods;
import strategy.Script;

/**
 * This needs to be done more intelligently.
 */

public class Kick extends AccessorMethods implements Script {

    @Override
    public int run(String param) {
        if(canKick()) {
            kick();
            return 3000; // stop script
        }
        print("Run..");
        return 100;
    }

	public boolean canKick() {
		double distance = myRobot.distanceTo(ball);
        print("" + distance);

        if(isFacingBall(myRobot)) {
            print("Is facing ball ");
        }  

        if(distance < 45) {
            print("Within distance");
        }    
  
		return (distance < 45) && isFacingBall(myRobot); // TODO: 30 needs to be changed when we have measurements
    }
}
