package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;

public class CornerTest extends AccessorMethods implements Script {
	public int run(String s) {
		
		
		if(inPitchCorner(myRobot)) {
			print("Our robot is in the corner");
		} else if(inPitchCorner(ball)) {
			print("Ball is in the corner!");
		} else {
			print("Nothing in corners");
		}
		return 1000;
	}

}
