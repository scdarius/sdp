package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import commander.ControllerGUI;
import strategy.world.*;

/**
 * Arc movement testing script
 *
 */
public class ArcMovement extends AccessorMethods implements Script {
	public int run(String param)  {
		double rX = myRobot.getLocation().x;
		double rY = myRobot.getLocation().y;

		double bX = ball.getLocation().x;
		double bY = ball.getLocation().y;

		double dX = bX - rX;
		double dY = bY - rY;

		double distG = ball.distanceTo(myRobot);
		System.out.println("Distantce from ball to robot is: " + distG);
		double rToBAngle = Math.atan2(dY, dX);
		System.out.println("Angle between robot and ball is: " + rToBAngle);

		double circleRadius = distG / 2 * Math.sin(rToBAngle);
		double circleAngle = 2 * rToBAngle;
		Tile circleOrigin = new Tile(rX + circleRadius*Math.cos(Math.PI/2), rY + circleRadius*Math.sin(Math.PI/2));
		System.out.println("Made circle with origin: x " + circleOrigin.x + " " +  circleOrigin.y + " radius: " + circleRadius);
		System.out.println("Distance from robot to circle: " + myRobot.distanceTo(circleOrigin) + " Distance from ball to circle: " + ball.distanceTo(circleOrigin));
		int circleAngleDegrees = (int) Math.toDegrees(circleAngle);
		System.out.println("This is: " + circleAngleDegrees + " in degrees");

		moveArc((int)circleRadius, circleAngleDegrees);
		stop();


		return -1;
	}
}
