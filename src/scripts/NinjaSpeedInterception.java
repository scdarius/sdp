package scripts;

import commander.ControllerGUI;

import strategy.*;
import strategy.world.Tile;
import strategy.world.Vector;
public class NinjaSpeedInterception extends AccessorMethods implements Script {
	
	static Tile lastBallPosition = null;
	
    Tile ballPosition = ball.getLocation();
    Tile myRobotPosition = myRobot.getLocation();
	Tile oppRobotPosition = oppRobot.getLocation();
	Tile oppRobotDirection = oppRobot.direction;
	
	boolean faceDown = false;
	
	public int run(String s) {
	    ballPosition = ball.getLocation();
	    myRobotPosition = myRobot.getLocation();
		oppRobotPosition = oppRobot.getLocation();
		oppRobotDirection = oppRobot.direction;
		
		return 500;
     
	}
	
	
	public Vector getGoalDirection() {
		return new Vector(new Tile(myRobotPosition.x, 0), new Tile(myRobotPosition.x, 122));	
	}
	
	public int interceptBall() {
		print("Intercept ball");
		//if(y == ball.getLocation().y) // ball is not moving
			//return 50;

       /* Tile target = new Tile(myRobot.getLocation().x, y);
        ControllerGUI.setPoint(target);
	    ControllerGUI.setLine(ball.getLocation(),target);
	     
	    if(faceDown){
		    if(myRobot.getLocation().y < y) {
			    moveForward();
			    print("Facing down, moving forwards");
		    }
		    else {
			    moveBackward();
			    print("Facing down, moving backwards");
		    }
	    } else{
		    if(myRobot.getLocation().y < y){
			    moveBackward();
			    print("Facing up, moving backwards");
		    }
		    else{
			    moveForward();
			    print("Facing up, moving forwards");
		    }
	    }  */     
           
		return 100;
	}
	
	public boolean faceDirection() {
		//if(y == ball.getLocation().y) {// ball is not moving
		//	print("Ball not moving");
		//	return false;
		//}
		
		print("Facing tile");
		double currAng = myRobot.getAngle();
		if(currAng > 0 &&  currAng < 180) {
			faceDown = true;
			ControllerGUI.setPoint(new Tile(myRobot.getLocation().x , 122));
			return faceTile(new Tile(myRobot.getLocation().x, 122));
		}else {
			print("face up");
			faceDown = false;
			ControllerGUI.setPoint(new Tile(myRobot.getLocation().x, 0));
			return faceTile(new Tile(myRobot.getLocation().x, 0));
		}
	}
}
