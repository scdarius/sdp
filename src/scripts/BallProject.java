package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import strategy.world.WorldState;
import strategy.BallProjection;
import strategy.BallMonitor;
import commander.ControllerGUI;

public class BallProject extends AccessorMethods implements Script {

	//Steering steerer = new Steering();

	public int run(String param) {
		bot.setSpeed(bot.getSpeedMax());
		ControllerGUI.clear();
		Tile[] projections = BallMonitor.calculateProjections(5);
		//if (projections[0]!=null) ControllerGUI.setPoint(projections[0]);
		if(projections[1]!=null) {
			ControllerGUI.setPoint(projections[1]);
			ControllerGUI.setLine(WorldState.ball.getLocation(),projections[1]);
			double x = WorldState.myRobot.getLocation().x;
			double slope = ((WorldState.ball.getLocation().y - projections[1].y)/(WorldState.ball.getLocation().x - projections[1].x));
			double y = slope * (WorldState.myRobot.getLocation().x - WorldState.ball.getLocation().x) + WorldState.ball.getLocation().y;
			Tile target = new Tile(x,y);
			if (target.y >= 0.0 && target.y <= 122) {
				ControllerGUI.setPoint(target);
				//steerer.run(target);
				moveTo(target); //This probs wont work very well
				System.out.println(target.y);
			} else {
				System.out.println("out of bounds");
				if(projections[0] != null){
					moveTo(projections[0]);
				}	
			}
		}
		else{
			System.out.println("nullerific");
			if(projections[0] != null){
				moveTo(projections[0]);
			}
		}
		return 50;
	}
}
