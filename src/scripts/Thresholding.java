package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Robot;
import strategy.world.Tile;

/**
 * Created by IntelliJ IDEA.
 * User: s0936300
 * Date: 13/02/12
 * Time: 17:57
 * To change this template use File | Settings | File Templates.
 */
public class Thresholding extends AccessorMethods implements Script {
    
    public int run(String s) {
        
        if(closeToBall(myRobot) && isFacingBall(myRobot))
            return -1; // stop script

        double distance = myRobot.distanceTo(ball); 
        int threshold = (int)((distance - 50) / 10);

        if(distance > 300) {
            faceTile(ball.getLocation(), 10);

        } else if(distance > 250 && distance > 50) {
            faceTile(ball.getLocation(), threshold );
        } else {
            print("loooool. FAIL");

        }
        
        print("" + distance + " Theshold: " + threshold);

        bot.travel((int)distance / 2);

        return 200;
    }

    /**
     * @param t Tile to check.
     * @param r Robot to
     * @param threshold the threshold of the angle. When we are at a greater distance, this should ideally be higher.
     * @return if we are facing a tile within a given angle threshold.
     */
    public boolean isFacingTile(Tile t, Robot r, int threshold) {
        double newBearing = r.getLocation().bearingTo(t);
        double angle = (((360 - r.getAngle()) + newBearing) % 360);
        if (angle>180)
            angle -= 360;
        if (Math.abs(angle) < threshold)
            return true;
        return false;
    }

    public boolean isFacingBall(Robot r, int threshold) {
        return isFacingTile(ball.getLocation(), r, threshold);
    }

    public boolean faceTile(Tile t, int threshold) {
        if(!isFacingTile(t, myRobot, threshold)) {
            double newBearing = myRobot.getLocation().bearingTo(t);
            double angle = (((360 - myRobot.getAngle()) + newBearing) % 360);
            if (angle>180)
                angle -= 360;
            print("FaceTile: " + angle);
            //int degrees = (int) Compute.angleToTurn(myRobot.getLocation(), t, ourAngle);
            bot.rotate((int) angle);
        }
        return isFacingTile(t, myRobot, threshold);
    }
}
