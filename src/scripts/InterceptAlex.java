package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.ScriptEngine;
import strategy.world.Tile;
import commander.ControllerGUI;



import strategy.world.Vector;

/**
 * This needs to be done more intelligently.
 */

public class InterceptAlex extends AccessorMethods implements Script {
    static Tile lastBallPosition = null;
    
    @Override
    public int run(String param) {
        int refRate = 40;
             
        
        Tile ballPosition = ball.getLocation();
        Tile myRobotPosition = myRobot.getLocation();
		Tile oppRobotPosition = oppRobot.getLocation();
		Tile oppRobotDirection = oppRobot.direction;
		
		
		
		Vector ballDirection = new Vector(oppRobotPosition, oppRobotDirection);
		ballDirection.setPoint(ballPosition);
		Vector goalDirection = new Vector(new Tile(myRobotPosition.x, 0), new Tile(myRobotPosition.x, 122));
        
        Tile intersectPosition = ballDirection.getIntersectionPt(goalDirection);		
		
        if (lastBallPosition == null) {
            lastBallPosition = ballPosition;
            
        }
        else {
            if (lastBallPosition.distanceTo(ballPosition) > 1.5) {
                Steering steerer = new Steering();
        		bot.setSpeed(bot.getSpeedMax()/2);
                refRate= steerer.run(intersectPosition);
            }
            
            
        }
        
        return refRate;
    
    }
}
