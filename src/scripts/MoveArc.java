package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.ScriptEngine;
import strategy.world.Tile;
import commander.ControllerGUI;

import plan.navigation.*;
import strategy.world.Vector;


public class MoveArc extends AccessorMethods implements Script {
    

    public int run(String param) {
    	
    	
    	moveArc(50, 20);
    	wait(4000);
    	moveArc(-50, -20);
    	wait(4000);
		
        return -1;


   }
}
