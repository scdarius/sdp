package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import strategy.world.GameObject;
import java.util.*;

/**
 * This needs to be done more intelligently.
 * 
 * Note threshold is to deal with camera distorting distances.
 */

public class PotentialFieldNavigation extends AccessorMethods implements Script {
	ArrayList<GameObject> obstacles = new ArrayList();
    GameObject target;
    double dt = 0.7;
    double vx = 0;
    double vy = 0;
    double fMax = 4000;
        
    int refreshRate = 500;
    Tile center = new Tile(338, 187);

    public PotentialFieldNavigation() {
        //ball.charge = -1;
        //ball.diam = 5;
        //obstacles.add(ball); // diameter should actually be 4.5
        oppRobot.diam = 15;
        oppRobot.charge = -1;
        oppRobot.mass = 10;
        myRobot.diam = 15;
        myRobot.charge = 1;
        myRobot.mass = 10;
        obstacles.add(oppRobot);
        obstacles.add(myRobot);
        target = ball;
    }

    public int run(String param) {
    	int threshold = 0;
    	
    	double distanceToCenter = Math.abs(ball.getLocation().getX() - center.getX());
    	if(distanceToCenter > 18)
    		threshold = 18;
    	else 
    		threshold = 13;
        
        if(closeToBall(myRobot, threshold) && isFacingBall(myRobot))
           return -1; // stop script

        	
        double distance = myRobot.distanceTo(ball);
        if(distance < 100 && distance >= 50) {
        	bot.setSpeed(bot.getSpeedMax() / 3);
        	refreshRate = 150;
        } else if(distance < 50) { 
        	bot.setSpeed(bot.getSpeedMax() / 4);
        	refreshRate = 25;
        	print("Lowest speed...");
        } else {
        	print("Highest speed");
        	bot.setSpeed(bot.getSpeedMax() / 2);
        }
        
        if(!closeToBall(myRobot, threshold) || !isFacingBall(myRobot)) {
                print("POTENTIAL FIELD!!");
                movePotentialField();
        }
        
        print("Running...");
         

        return refreshRate;
    }
    
    public void movePotentialField() {
         double dirX = 0, dirY = 0;
         double minS = 200;
         Iterator iter = obstacles.iterator();

         while (iter.hasNext()) {
             GameObject ob = (GameObject) iter.next();
             double distSq =ob.distanceDiamSq(myRobot);
             if (distSq < 1)
                 Math.sin(1);
             double dx = ob.charge * (ob.loc.x - myRobot.loc.x) / distSq;
             double dy = ob.charge * (ob.loc.y - myRobot.loc.y) / distSq;
             dirX += dx;
             dirY += dy;
         }
         
         double norm = Math.sqrt(dirX*dirX+dirY*dirY);
         dirX = dirX / norm;
         dirY = dirY / norm;
 
         iter = obstacles.iterator();
         while (iter.hasNext()) {
             GameObject ob = (GameObject) iter.next();
             if(!range(ob, 1200)) continue;
             double distSq =ob.distanceDiamSq(myRobot);
             double dx = (ob.loc.x - myRobot.loc.x);
             double dy = (ob.loc.y - myRobot.loc.y);
             //add normal noise to simulate the sonar effect
             dx = addNoise(dx, 0, 1);
             dy = addNoise(dy, 0 ,1);
             double safety = distSq / ((dx * dirX+dy*dirY));
             if ((safety > 0) &&(safety < minS))
                 minS = safety;
         }
         if (minS < 5) {
             double oc = target.charge;
             target.charge*=minS/5;
             System.out.println(oc +" DOWN TO "+ target.charge);
         }
 
         if (minS > 100) {
             double oc = target.charge;
             target.charge*=minS/100;
             System.out.println(oc +" UP TO "+ target.charge);
         }
 
 
         double vtNorm = minS/2;
         double vtx = vtNorm * dirX;
         double vty = vtNorm * dirY;
         double fx = myRobot.mass * (vtx - vx) / dt;
         double fy = myRobot.mass * (vty - vy) / dt;
         double fNorm =  Math.sqrt(fx * fx + fy * fy);
         print("fNorm: " + fNorm);
         if (fNorm > fMax ) {
             fx *=  fMax / fNorm;
             fy *=  fMax / fNorm;
         }
         print("fx: " + fx);
         print("fy: " + fy);
         vx += (fx * dt) / myRobot.mass;
         vy += (fy * dt) / myRobot.mass;
         
         print("vx: " + vx);
         print("vy: " + vy);
         
         //virtual force component        
         /*if(virtualforce && (target.charge < 1000) && (x > 25) && (y > 25)) {
           System.out.println("Virtual Force");
           target.charge*=minS/100;
           
             vx = vx + 5;
         }*/
         double xNew = myRobot.loc.x + vx * dt;
         double yNew = myRobot.loc.y + vy * dt;
         
         print("old x: " + myRobot.loc.x);
         print("old y: " + myRobot.loc.y);
         
         print("xNew: " + xNew);
         print("yNew: " + yNew);
         
         double angle = Math.toDegrees(Math.atan2(yNew - myRobot.loc.y, xNew - myRobot.loc.x));
         double dist = Math.sqrt(Math.pow(xNew - myRobot.loc.x, 2) + Math.pow(yNew - myRobot.loc.y, 2));
         print("Angle: " + angle);
         print("Distance: " + dist);
         bot.rotate((int)Math.round(angle));
         bot.travel((int)Math.round(dist));

    }
    
     boolean range(GameObject ob, double range) {
        double dist =ob.distanceDiamSq(myRobot);
        if(dist < range)
          return true;
        else 
          return false;
     }
     
     double addNoise(double x, double mean, double stddev) {
       Random r = new Random();
       double noise = stddev*r.nextGaussian() + mean;
       return x + noise;
     }
}
