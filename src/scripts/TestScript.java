package scripts;

import strategy.AccessorMethods;
import strategy.Script;

/**
 * Test script to see if everything is working.
 *
 */
public class TestScript extends AccessorMethods implements Script {
	public int run(String param)  {

        print(param);
		return 100;
	}

    public boolean test() {
        return true;
    }
}
