package scripts;

import strategy.AccessorMethods;
import strategy.Script;

import java.util.Random;

/**
 * This needs to be done more intelligently.
 */

public class Penalty extends AccessorMethods implements Script {
    static int randomInt = 0;
    static int sign = 0;
    static int orientation = 0;
    static int count = -1;
    static int angle = 25;
    @Override
    public int run(String param) {
        //int turnMax = bot.getTurnSpeedMax();
        if (count == -1) {
            Random randomGenerator = new Random();
            randomInt = randomGenerator.nextInt(2);
            randomInt +=8;
            count = 0;
            sign = randomGenerator.nextInt(2);
        //will usually have the values 1 left and -1 right
            if (sign % 2 == 0)
                sign = -1;
            else
                sign = 1;
            
            bot.rotate(sign * angle);
            orientation = sign;
        }
        

       
        
        
        orientation *= -1;
        if (canScore(orientation) && count >= 2) {  //we want to turn a few times before checking if we can kick or not
            kick();
            count = -1;
            return -1;
        }
        if (count % 2 == 0)  
            bot.rotate(-1 * sign * 2 * angle);
        else 
            bot.rotate(sign * 2 * angle);

        if (count >= randomInt) {
            kick();
            count = -1;
            return -1;
        }
        count++;
        //print("count " + count);
        //print("orientation " + orientation);
        return 50;
    }
    public boolean canScore(int orientation) {
        if (myRobot.loc.x > 122) // we are shooting to the left goal else right
            orientation *= -1;
        if (orientation > 0 && (oppRobot.loc.y <= 66 || oppRobot.loc.y >= 96) )
                return true;
        if (orientation < 0 && (oppRobot.loc.y >= 66 || oppRobot.loc.y <= 36) )
                return true;
            
        //This should return true when the other robot is not blocking our way to the goal
		return false;
    }
}
