package scripts;
import strategy.AccessorMethods;

import strategy.Script;

/**
* Prints out the locations of all the game objects every 4 seconds.
*
*/

public class FaceBall extends AccessorMethods implements Script {
    public int run(String param) {
        if(isFacingBall(myRobot)) {
            print("Facing ball");
		bot.stop();
		return -1;		
        } else {
            print("" + myRobot.getLocation().bearingTo(ball.getLocation()));
		faceTile(ball.getLocation());
        }
        return 100;
    }
}
