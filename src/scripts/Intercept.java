package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import strategy.world.WorldState;
import strategy.BallProjection;
import strategy.BallMonitor;
import commander.ControllerGUI;

public class Intercept extends AccessorMethods implements Script{

	//boolean ballMoved = false;
	boolean faceDown = false;
	boolean speedSet = false;
	public int run(String param) {
		if (!speedSet) {
		        bot.setSpeed(bot.getSpeedMax());
		        speedSet = true;
	        }
	        
		Tile[] projections = BallMonitor.calculateProjections(5);
		
		double robotDistanceToKicker = WorldState.myRobot.getLocation().distanceTo(WorldState.oppRobot.getLocation());
		double ballDistanceToKicker = WorldState.ball.getLocation().distanceTo(WorldState.oppRobot.getLocation());
		
		//if(isMoving(ball)){
		//if(WorldState.ball.getLocation().x < 185.0) {
		if(ballDistanceToKicker / robotDistanceToKicker > 0.040) {
			double currAng = myRobot.getAngle();
			//if(!ballMoved){
			if(currAng > 0 &&  currAng < 180){
				print("face down");
				faceTile(new Tile(myRobot.getLocation().x, 122));
				ControllerGUI.setPoint(new Tile(myRobot.getLocation().x , 122));
				faceDown = true;
			}
			else {
				print("face up");
				faceTile(new Tile(myRobot.getLocation().x, 0));
				ControllerGUI.setPoint(new Tile(myRobot.getLocation().x, 0));
			}
			//ballMoved = true;
				
		//}
		//else {
                        double y = WorldState.ball.getLocation().y;
                        
                        if(projections[1]!=null) {
		                double x = myRobot.getLocation().x;
		                double slope = ((ball.getLocation().y - projections[1].y)/(ball.getLocation().x - projections[1].x));
		                y = slope * (myRobot.getLocation().x - ball.getLocation().x) + ball.getLocation().y;
	                }
	                
	                //else y = myRobot.ball.getLocation().y;
	                
	                Tile target = new Tile(myRobot.getLocation().x, y);
                        ControllerGUI.setPoint(target);
		        ControllerGUI.setLine(ball.getLocation(),target);
			
			//if(WorldState.ball.getLocation().x < 150.0) {
			if (ballDistanceToKicker / robotDistanceToKicker > 0.080) {
			        if(Math.abs(myRobot.getLocation().y - y) < 5) {
                                        stop();
                                        print("Face tile");
                                        faceTile(WorldState.ball.getLocation());
                                        
                                        //AStarNew astar = new AStarNew();
                                        //return astar.run("");
			        }
			        else {
                                        if(faceDown) {
                                                if(myRobot.getLocation().y < y) {
                                                    moveForward();
                                                    print("Facing down, moving forwards");
                                                }
                                                else {
                                                    moveBackward();
                                                    print("Facing down, moving backwards");
                                                }
                                        }
                                        else {
                                                if(myRobot.getLocation().y < y) {
                                                    moveBackward();
                                                    print("Facing up, moving backwards");
                                                }
                                                else {
                                                    moveForward();
                                                    print("Facing up, moving forwards");
                                                }
                                        }
			        }
		        }
		}
	return 50;
	}
}
	
		
		
		
		
		/*
		else if(ballMoved && !isMoving(ball)) {
            stop();
			print("A star running ");
			if(faceTile(WorldState.ball.getLocation())) {
			    AStarNew astar = new AStarNew();
                return astar.run("");
            }
		}*/
		
