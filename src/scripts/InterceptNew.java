package scripts;
import commander.ControllerGUI;

import strategy.*;
import strategy.world.Tile;
import strategy.world.WorldState;

public class InterceptNew extends AccessorMethods implements Script {
	boolean faceDown = false;
	Tile[] projections = BallMonitor.calculateProjections(5);
	double y = 0;
	Tile projectedTile;
	boolean setSpeed = false;
	boolean ballMoved = false;
	boolean intercepted = false;
	
	public int run(String s) {
		if(intercepted)
			return -1;
		
		if(!setSpeed)
			setSpeed();
		
	    y = getYCoord(); 
	    
	    if(Math.abs(myRobot.getLocation().y - y) < 18) {
	        print("Less than 18 " + y + " " + myRobot.getLocation().y);
	    	bot.setSpeed((int)(bot.getSpeedMax()));
	    } else if(Math.abs(myRobot.getLocation().y - y) > 18 && Math.abs(myRobot.getLocation().y - y) < 25) {
	    	bot.setSpeed((int)(bot.getSpeedMax()));
	    }
	    
	    if(Math.abs(myRobot.getLocation().y - y) < 14 /*&& ball.distanceTo(myRobot) < 30*/) {
	    	stop();
	    	faceBall();
	    	print("Arrived at destination");
	    	   intercepted = true;
	    	   return -1;
	    }
		
		if(!faceDirection())
			return 100;
		
		return interceptBall();
		
	}
	
	public void setSpeed() {
		bot.setSpeed(bot.getSpeedMax());
		setSpeed = true;
	}
	
	
	public boolean faceDirection() {
		if(!isBallMoving()) {// ball is not moving
			print("Ball not moving");
			return false;
		}
		
		print("Facing tile");
		double currAng = myRobot.getAngle();
		if(currAng > 0 &&  currAng < 180) {
			faceDown = true;
			ControllerGUI.setPoint(new Tile(myRobot.getLocation().x , 122));
			return faceTile(new Tile(myRobot.getLocation().x, 122), 15);
		}else {
			print("face up");
			faceDown = false;
			ControllerGUI.setPoint(new Tile(myRobot.getLocation().x, 0));
			return faceTile(new Tile(myRobot.getLocation().x, 0), 15);
		}
	}
	
	public double getYCoord() {
		projections = BallMonitor.calculateProjections(5);
        if(projections[1]!=null) {
            double x = myRobot.getLocation().x;
            double slope = ((ball.getLocation().y - projections[1].y)/(ball.getLocation().x - projections[1].x));
            return slope * (myRobot.getLocation().x - ball.getLocation().x) + ball.getLocation().y;
        } else {
        	return ball.getLocation().y;
        }
        
	}
	
	public int interceptBall() {
		print("Intercept ball");
		if(!isBallMoving()) // ball is not moving
			return 50;

        Tile target = new Tile(myRobot.getLocation().x, y);
        ControllerGUI.setPoint(target);
	    ControllerGUI.setLine(ball.getLocation(),target);
	     
	    if(faceDown){
		    if(myRobot.getLocation().y < y) {
			    moveForward();
			    print("Facing down, moving forwards");
			    return 75;
		    }
		    else {
			    moveBackward();
			    print("Facing down, moving backwards");
			    return 75;
		    }
	    } else{
		    if(myRobot.getLocation().y < y){
			    moveBackward();
			    print("Facing up, moving backwards");
			    return 75;
		    }
		    else{
			    moveForward();
			    print("Facing up, moving forwards");
			    return 75;
		    }
	    }       
           
	}
	
	public boolean isBallMoving() {
		if(oppRobot.distanceTo(ball) > 20) {
			return true;
		}
		return false;
	}

}
