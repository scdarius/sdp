package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import strategy.world.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Ashley
 * Date: 12/03/12
 * Time: 07:00
 */
public class VectorTest extends AccessorMethods implements Script {
    @Override
    public int run(String param) {
        Vector one = new Vector(new Tile(100, 100));
        Vector two = new Vector(new Tile(50, 0), new Tile(50, 50));
        Tile t = one.getIntersectionPt(two);
        print(t.toString());
        if(one.intersects(wholePitch, two)) {
            print("yes 1");
        }

        Vector three = new Vector(new Tile(500, 500));
        Vector four = new Vector(new Tile(450, 0), new Tile(500, 500));
        Tile t2 = three.getIntersectionPt(four);
        print(t2.toString());
        if(four.intersects(wholePitch, three)) {
            print("yes 2");
        }
        
        Tile center = getCenter(new Tile(-2, 3), new Tile(3, 8), new Tile(8, 3));
        print(center.toString());

        return 0;
    }

    public Tile getCenter(Tile p1, Tile p2, Tile p3) {
        Vector v1 = new Vector(p1);
        Vector v2 = new Vector(p2);
        Vector v3 = new Vector(p3);

        Tile midPoint1 = v1.getMidpoint(v2);
        //print("Midpoint1 " + midPoint1.toString());
        Tile midPoint2 = v3.getMidpoint(v2);
        //print("Midpoint2 " + midPoint2.toString());

        Vector v4 = new Vector(p1, p2);
        Vector v5 = new Vector(p2, p3);

        Tile dir1 = v4.getDirection();
        dir1 = new Tile(0 - dir1.y, dir1.x);

        Tile dir2 = v5.getDirection();
        dir2 = new Tile(0 - dir2.y, dir2.x);

        v4.setDirection(dir1);
        v4.setPoint(midPoint1);
        v5.setDirection(dir2);
        v5.setPoint(midPoint2);
        
        print(v4.toString());
        print(v5.toString());

        return v4.getIntersectionPt(v5);

    }
}
