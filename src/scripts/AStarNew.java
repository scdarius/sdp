package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.ScriptEngine;
import strategy.world.Tile;
import commander.ControllerGUI;

import plan.navigation.*;


public class AStarNew extends AccessorMethods implements Script {
    boolean setSpeed = false;
    
    public void setSpeed() {
		bot.setSpeed((int)(bot.getSpeedMax() / 1.5));
		setSpeed = true;
	}
    
    
    public int run(String param) {
    
        if(!setSpeed)
			setSpeed();
        
    	long time = System.currentTimeMillis(); 
        NavigateToPoint navTile = new NavigateToPoint();
        Tile myRobotLocation = myRobot.getLocation(); //Gets our x y coords as a float
		Tile oppRobotLocation = oppRobot.getLocation();
		Tile ballLocation = ball.getLocation();
	    Tile goalLocation = getGoalScoringTile();	
	    
        if (myRobotLocation.distanceTo(goalLocation) < 11 && ((shootLeft && myRobotLocation.x > goalLocation.x) || (!shootLeft && myRobotLocation.x < goalLocation.x))) {
            if (shootLeft) 
                faceTile(new Tile(0, 61));
            else 
                faceTile(new Tile(244, 61));
            
            // this overshoots as it does not check whether 'faceTile' suceeded. 
            // note: use if(faceTile(blah)) as it will return whether it has succeeded.
            int speedMax = bot.getSpeedMax();
	        bot.setSpeed(speedMax / 3);
	        bot.go();
	        wait(700);
	        bot.setSpeed(speedMax);
	        wait(400);
	        kick();
	        stop();
	        bot.setSpeed(bot.getSpeedMax());
		    return 100;
        }
		
		//declarations of things i need
        Obstacle top = new LineObstacle(new Grid(0, 0), new Grid(244, 0));
		Obstacle bottom = new LineObstacle(new Grid(0, 122), new Grid(244, 122));
		Obstacle left = new LineObstacle(new Grid(0, 0), new Grid(0, 122));
		Obstacle right = new LineObstacle(new Grid(244, 0), new Grid(244, 122));
		
		
		Obstacle opponent = new RoundObstacle(new Grid((int) oppRobotLocation.x, (int) oppRobotLocation.y), 16);
		Obstacle ball = new RoundObstacle(new Grid((int) ballLocation.x, (int) ballLocation.y), 5);
		
		Grid goal = new Grid((int)goalLocation.x, (int)goalLocation.y);
		Robot robot = new Robot(new Grid((int)myRobotLocation.x, (int)myRobotLocation.y), 16);
		
		//When would it ever be true that distanceTo is less than zero?
		if (robot.distanceTo(opponent) < 0) //this should make us go away from the opponent
		    opponent = new RoundObstacle(new Grid((int) oppRobotLocation.x, (int) oppRobotLocation.y), 6);

		if (robot.distanceTo(ball) < 0){ 
					robot = new Robot(new Grid((int)myRobotLocation.x, (int)myRobotLocation.y), 10);
					ball = new RoundObstacle(new Grid((int) ballLocation.x, (int) ballLocation.y), 2.5); 
		}
		    
		    
		Obstacle obstacles[] = {opponent, ball, top, bottom, left, right};
		RoundObstacle robstacles[] = {(RoundObstacle) opponent, (RoundObstacle) ball};
		
		
		PathFinder aStar = new PathFinder(robot, goal, obstacles, 244, 122);
		
		Grid path[] = aStar.findPath();
		
		int last_path_length = 0;
		print("Taken : "+(System.currentTimeMillis() - time));
		do {
			last_path_length = path.length;
			path = RamerDouglasPeuckerFilter.filter(path, 1);
			path = ExtraPointsFilter.filter(path, robstacles, robot.radius);
		} while (path.length != last_path_length);
        
        path = ConnectPointsFilter.filter(path, 10);
        
        Tile tilePath[] = new Tile[path.length-1];
        System.out.println(goal);
        for (int i=1; i< path.length; i++) {
            //System.out.println(path[i]);
            tilePath[i-1] = new Tile((double)path[i].x, (double)path[i].y);
        }
        bot.setSpeed((int)(bot.getSpeedMax() / 1.5));
        print("Taken : "+(System.currentTimeMillis() - time));

		//Steering steerer = new Steering();
		
//		bot.setSpeed(bot.getSpeedMax()/2);
		
       //int refRate = steerer.run(tilePath[0]);
        int refRate = navTile.run(tilePath[0]);
		ControllerGUI.clear();
		ControllerGUI.setPath(tilePath);
		
        return refRate;
    }

    
}
