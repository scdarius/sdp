package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;

/**
 * This needs to be done more intelligently.
 *
 * Note threshold is to deal with camera distorting distances.
 */

public class NavigateToPoint extends AccessorMethods implements Script {

    int refreshRate = 500;
    Tile center = new Tile(122, 61);
    public int run(String dummy){
    	return 100; //shouldn't really use this without a tile
    }

    public int run(Tile param) {
        int threshold = 1;

        if(myRobot.distanceTo(param) < threshold && isFacingTile(param, myRobot))
            return -1; // stop script


        double distance = myRobot.distanceTo(param);
        if(distance < 100 && distance >= 40) {
            bot.setSpeed(bot.getSpeedMax());
            refreshRate = 150;
        } else if(distance < 40 && distance > 20) {
            bot.setSpeed((int)(bot.getSpeedMax() / 1.5));
            refreshRate = 25;
            print("Lowest speed...");
        } else if(distance < 20) {
        	bot.setSpeed(bot.getSpeedMax() / 2);
        	refreshRate = 25;
        } else {
            bot.setSpeed(bot.getSpeedMax());
            refreshRate = 200;

        }

        if(faceTile(param)) {
            if(myRobot.distanceTo(param) >= threshold) {
                moveForward();
            }
        }

        print("Running...");

        return refreshRate;
    }
}
