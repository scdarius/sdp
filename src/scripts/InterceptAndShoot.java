package scripts;

import strategy.BallMonitor;
import strategy.world.Tile;
import strategy.*;

import commander.ControllerGUI;

public class InterceptAndShoot extends AccessorMethods implements Script {
	
	InterceptNew in = new InterceptNew();
	AStarNew asp = new AStarNew();
	public int run(String s) {
		int wait = in.run("");
		
		if(wait != -1) // it has run
			return wait;
		
		wait = asp.run("");
		return wait;
		
	}

	

}
