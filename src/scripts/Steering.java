package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.ScriptEngine;
import strategy.world.Tile;
import commander.ControllerGUI;

import plan.path.Finder;
import plan.path.pathfinding.Path;

public class Steering extends AccessorMethods implements Script{


	public int run(String param){
			return -1;
	}

	
	public int run(Tile dest){
		if(myRobot.distanceTo(dest) < 15) {
			NavigateToPoint np = new NavigateToPoint();
			return np.run(""); 
		}
		
		if(myRobot.distanceTo(dest) < 9) {
			return -1;
		}
			//Angle from x axis to ball from current position
			int angleToBall = (int) myRobot.getLocation().bearingTo(dest);
			//Current orientation
			int robotAngle = (int) myRobot.getAngle();

			//Robot angles go from -180 to 180, whereas angleToBall goes from 0 to 360, make them both the same
			if(robotAngle < 0){
				robotAngle += 360;
			}

			int angleToTurn = (angleToBall - robotAngle);
			if (angleToTurn < 0){
				angleToTurn = angleToTurn + 360;
			}

			System.out.println("Angle between ball and robot: " + angleToTurn + "degrees");

			if(angleToTurn  <= 180){
					int turnRatio = (int) (200 * angleToTurn/180.0);
					//Turn right
					//If its too big a turn, just rotate
					if(turnRatio > 160){
						faceTile(dest);
						moveForward();
					}else{
						steer(turnRatio, 360);
						System.out.println("Turn Ratio: " + (turnRatio) + " angle to turn: " + angleToTurn);
					}
			}else{
					int turnRatio = (int) (200 * (angleToTurn - 180)/180.0);
					//Turn left
					if(-(200 - turnRatio) < -160){
						faceTile(dest);
						moveForward();
					}else{
						steer(-(200 - turnRatio), 360);
						System.out.println("Turn Ratio: " + (-(200 - turnRatio)) + " angle to turn: " + angleToTurn);
					}

			}

			
				
			return 100;
	}
}
