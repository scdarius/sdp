package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;

public class SlowQuickDribble extends AccessorMethods implements Script {
    public int run(String param) {
        
	int speedMax = bot.getSpeedMax();

	bot.setSpeed(speedMax / 5);
	bot.go();
	wait(1000);
	bot.setSpeed(speedMax);
	wait(750);
	bot.stop();

        return -1; // wait 50ms before restarting checks
    }

}
