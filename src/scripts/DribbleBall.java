package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;

/**
 * So we can test this with the simulator.
 * The idea is very much based on control flow atm and is hierarchical.
 * If we have travelled 30cm, it will stop and quit. This is always checked before
 * we do ANYTHING else.
 * Next to it will check if we are facing the ball. If we are not, we start the checks again.
 * If we are, the script continues, aka, the robot moves forward.
 * Once we are facing the ball, we can continue deciding what to do next.
 * TODO: Handle if ball is near wall.
 */
public class DribbleBall extends AccessorMethods implements Script {
    int distance = 0;
    Tile initialLocation =  myRobot.getLocation();
    public int run(String param) {
        double distanceTravelled = getDistanceTraveled();

        if(distanceTravelled >= 30)
            return -1; // travelled required distance so stop.

        if(!faceBall())
            return 10; // return and try again until we are facing the ball.
        
        if(isFacingBall(myRobot) && closeToBall(myRobot))
            moveForward();

       return 100; // wait 50ms before restarting checks
    }
    
    public double getDistanceTraveled() {
        Tile currentLocation = myRobot.getLocation();
        return initialLocation.distanceTo(currentLocation);
    }

}
