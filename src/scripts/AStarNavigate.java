package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.ScriptEngine;
import strategy.world.Tile;
import commander.ControllerGUI;

import plan.path.Finder;
import plan.path.pathfinding.Path;

/**
 * This needs to be done more intelligently.
 * 
 * Note threshold is to deal with camera distorting distances.
 */

public class AStarNavigate extends AccessorMethods implements Script {
	Finder findy = new Finder();
	ScriptEngine scriptEng;
	NavigateToPoint navTile = new NavigateToPoint();
	public int ballOffset = 21;

	public int run(String param){
		int refRate = 150;
		
		bot.setSpeed(bot.getSpeedMax() /2);
		Tile myRobotLocation = myRobot.getLocation(); //Gets our x y coords as a float
		Tile oppRobotLocation = oppRobot.getLocation();
		Tile ballLocation = ball.getLocation();
		long time = System.currentTimeMillis();
		setUpMap();
		
		

		int x = (int)(myRobotLocation.x);
		int y = (int)(myRobotLocation.y);
		int bx = (int)(ballLocation.x);
		int by = (int)(ballLocation.y);

		if(x >= 244|| x < 0 || y  >=122|| y < 0 || bx >= 244 || bx < 0|| by >=122 || by < 0){
			System.out.format("One of these has gone over: x = %d y = %d bx = %d by = %d%n" , x, y , bx , by);
			return refRate;
		}


		if(oppRobotLocation.x >= 0 && oppRobotLocation.x < 244 && oppRobotLocation.y >= 0 && oppRobotLocation.y < 122){
			findy.map.fillEnemyRobot(oppRobotLocation);
			findy.map.setUnit(oppRobotLocation, 3);
		}

		findy.map.fillFromCentre(ballLocation, 15, 15, 5);
		findy.map.fillFromCentre(myRobotLocation,0,0,2);
		findy.map.setUnit(myRobotLocation, 2);
		findy.map.printMap();
		//bot.setTurnSpeed(bot.getTurnSpeedMax());

		//Needs to be worked into finder, have just hard coded the conversion for now

		int gsbX = (int) getGoalScoringTile().x;
		int gsbY = (int) getGoalScoringTile().y;

        
		//if shooting left:
		//and if within boundaries
		int actualBX = bx;
		int actualBY = by;

		Path ansPath = null;

		System.out.format("x: %d y : %d bx: %d by: %d gsbX: %d gsbY: %d", x, y, bx, by, gsbX, gsbY);

		if(gsbX >=0 && gsbX < 244 && gsbY >= 0 && gsbY < 244){
				ansPath = findy.answer(x, y, gsbX, gsbY);
		}

        
		if(ansPath == null){
			if(bx + 21 <= findy.map.getWidthInTiles() - 1 && oppGoal.loc.x == 0){
				bx = actualBX + 21;
			}
			else if(bx - 21 > 0 && oppGoal.loc.x != 0){
				bx = actualBX - 21;
			}
			

			ansPath = findy.answer(x, y, bx, by);
		}

		if (ansPath == null){
			System.out.format("pathfinder has returned a null path with: x: %d y: %d bx: %d by: %d" , x, y, bx, by);
			
	        if(bx + 21 <= findy.map.getWidthInTiles() - 1 && oppGoal.loc.x == 0){
		        bx = actualBX + 21;
	        }
	        else if(bx - 21 > 0 && oppGoal.loc.x != 0){
		        bx = actualBX - 21;
	        }
			
			
			/*
			if(by - 21 > 0) {
				bx = actualBX;
				by = by - 21;
			}
			else if(by + 21 <= findy.map.getHeightInTiles() - 1){
				bx = actualBX;
				by = by + 21;
			}*/
			ansPath = findy.answer(x,y, bx, by);
		}
		print("Taken : "+(System.currentTimeMillis() - time));
		if(ansPath == null){
			System.out.format("pathfinder has returned a null path with: x: %d y: %d bx: %d by: %d" , x, y, bx, by);
			return refRate;
		
		}else{
			int px;
			int py;

			for (int i = 0; i < ansPath.getLength(); i++){
				px = ansPath.getStep(i).getX();
				py = ansPath.getStep(i).getY();
				findy.fill(px,py,1,1,9);
			}
			
				findy.fill(actualBX, actualBY, 1,1,7);

			findy.map.printMap();
			
			//Tile[] ansTiles = findy.simplePath(ansPath, 50,30);
			Tile[] ansTiles = findy.simplePath(ansPath, 30,18);
			for (int i= 0; i < ansTiles.length; i++){
				System.out.println("X: " + ansTiles[i].x + " Y: " + ansTiles[i].y);
			}


				Steering steerScript = new Steering();
				steerScript.run(new Tile(ansTiles[0].x, ansTiles[0].y));
			
			
			/*
			faceBall();
			int speedMax = bot.getSpeedMax();
			bot.setSpeed(speedMax / 5);
			bot.go();
			wait(1000);
			bot.setSpeed(speedMax);
			wait(400);
			kick();
			stop();
			*/
		

		ControllerGUI.clear();
		ControllerGUI.setPath(ansTiles);
		System.out.println("refRate is: " + refRate);
		return refRate;
		}
	}

	private void setUpMap(){
		findy.map.fillArea(0,0, 244, 122, 0); //initialize terrain
		//Borders are set to unreachable
	}


}
	
