package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;

/**
 * This needs to be done more intelligently.
 *
 * Note threshold is to deal with camera distorting distances.
 * TODO: Is the threshold now necessary as we have plane correction?
 */

public class NavigateToBall extends AccessorMethods implements Script {

    int refreshRate = 500;
    Tile center = new Tile(338, 187);

    public int run(String param) {
        int threshold = 0;

        double distanceToCenter = Math.abs(ball.getLocation().getX() - center.getX());
        if(distanceToCenter > 18)
            threshold = 18;
        else
            threshold = 13;


        double distance = myRobot.distanceTo(ball);
        if(distance < 100 && distance >= 50) {
	    print("Normal Speed");
            bot.setSpeed(bot.getSpeedMax() / 3);
            refreshRate = 150;
        } else if(distance < 50) {
            bot.setSpeed(bot.getSpeedMax() / 4);
            refreshRate = 25;
            print("Lowest speed...");
        } else {
            print("Highest speed");
            bot.setSpeed(bot.getSpeedMax() / 2);
        }

        if(faceBall()) {
            if(!closeToBall(myRobot, threshold)) {
                moveForward();
            }
        }

        print("Running...");

        return refreshRate;
    }
}
