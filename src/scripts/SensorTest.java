package scripts;

import strategy.AccessorMethods;
import strategy.Script;

/**
* Prints out the locations of all the game objects every 4 seconds.
*
*/

public class SensorTest extends AccessorMethods implements Script {
	public int run(String param) {
		while (!bot.leftSensorPressed() && !bot.rightSensorPressed()) {
			System.out.println(canGetInPositionToShot());
		
		}
		
		return -1;
	}
}
