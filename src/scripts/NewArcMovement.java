package scripts;

import strategy.AccessorMethods;
import strategy.Movement;
import strategy.Script;
import strategy.world.Tile;
import strategy.world.Vector;

public class NewArcMovement extends AccessorMethods implements Script {
	public int run(String param) { 
		Tile[] path = Movement.getPath(getGoalScoringTile());

	        Tile center = getCenter(myRobot.getLocation(), path[1], path[2]);

	        double radius = center.distanceTo(myRobot.getLocation());

		// distance to third tile
	        double goalDistance = myRobot.distanceTo(path[2]);

	        double angle = Math.asin(goalDistance / (2 * radius)); // in radians -.-

		double arcLength = angle * radius;

	        print("" + angle + " goal distance " + goalDistance + " circle center " + center.toString() + " " + arcLength);   

	        return 500;
	                
	}
	
    public Tile getCenter(Tile p1, Tile p2, Tile p3) {
        Vector v1 = new Vector(p1);
        Vector v2 = new Vector(p2);
        Vector v3 = new Vector(p3);

        Tile midPoint1 = v1.getMidpoint(v2);
        //print("Midpoint1 " + midPoint1.toString());
        Tile midPoint2 = v3.getMidpoint(v2);
        //print("Midpoint2 " + midPoint2.toString());

        Vector v4 = new Vector(p1, p2);
        Vector v5 = new Vector(p2, p3);

        Tile dir1 = v4.getDirection();
        dir1 = new Tile(0 - dir1.y, dir1.x);

        Tile dir2 = v5.getDirection();
        dir2 = new Tile(0 - dir2.y, dir2.x);

        v4.setDirection(dir1);
        v4.setPoint(midPoint1);
        v5.setDirection(dir2);
        v5.setPoint(midPoint2);
        
        print(v4.toString());
        print(v5.toString());

        return v4.getIntersectionPt(v5);

    }

}
