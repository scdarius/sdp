package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import commander.ControllerGUI;
/**
 * This needs to be done more intelligently.
 *
 * Note threshold is to deal with camera distorting distances.
 */

public class NavigateToGoalScoringTile extends AccessorMethods implements Script {
    int refreshRate = 500;
    public int run(String param) {
		ControllerGUI.clear();
		ControllerGUI.setPoint(getGoalScoringTile());
        ControllerGUI.setLine(getGoalScoringTile(), oppGoal.getLocation());
		double distance = myRobot.getLocation().distanceTo(getGoalScoringTile());
        if (distance < 10) {
            print("CLOSE ENOUGH!");
            if (faceBall())
                print("FACING THE RIGHT WAYY!!!");
           	 int speedMax = bot.getSpeedMax();
	        bot.setSpeed(speedMax / 3);
	        bot.go();
	        wait(500);
	        bot.setSpeed(speedMax);
	        wait(400);
	        kick();
                return -1;
        } else {
            print("Turning to face ball");
            faceTile(oppGoal.getLocation());
            if(distance < 100 && distance >= 50) {
                bot.setSpeed(bot.getSpeedMax() / 3);
                refreshRate = 150;
            } else if(distance < 50) {
                bot.setSpeed(bot.getSpeedMax() / 4);
                refreshRate = 25;
                print("Lowest speed...");
            } else {
                print("Highest speed");
                bot.setSpeed(bot.getSpeedMax() / 2);
            }
            moveForward();
        }

        print("Running...");

        return refreshRate;
    }
}
