package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import commander.ControllerGUI;
/**
 * This needs to be done more intelligently.
 *
 * Note threshold is to deal with camera distorting distances.
 */

public class TestGuiDraw extends AccessorMethods implements Script {

	public int run(String param) {
		
		ControllerGUI.clear();
		Tile t1 = getBestTileToShootAt();
		Tile t2 = myRobot.getLocation();
		
		if(t1 == null){
			ControllerGUI.clear();
		}
		
		else{
			ControllerGUI.setLine(t1,t2);
		}
		
		
		//ControllerGUI.paintLine(getGoalScoringTile(), oppGoal.getLocation());
		return 100;
    }
}
