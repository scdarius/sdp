package scripts;

import strategy.AccessorMethods;
import strategy.Script;

/**
 * Most basic game script ever.
 * Basically if they are ridiculously closer to tha ball than us, 'defend'
 * otherwise run the current NavigateAndShoot script.
 * TODO: PENALTY TAKING HANDLED BY SCRIPT ENGINE. NOT THIS.
 */
public class GameScript extends AccessorMethods implements Script {

    boolean attackMode = true;

    /**
     * Defined script strategies.
     */
    // this needs changing since NavigateAndShoot is specific to the milestone.
    NavigateAndShoot ns = new NavigateAndShoot();
    InterceptScript is = new InterceptScript();
    
    public int run(String param) {
        attackMode = setMode();
        
        if(attackMode) 
            return attackStrategy();
        
        
        return defendStrategy();
    }

    public int attackStrategy() {
        int refreshRate = ns.run("");

        // if we havn't returned -1 it means the script has executed.
        if(ns.run("") != -1)
            return refreshRate;
        
        print("Couldn't decide on an attack strategy?");
        return 750;
    }
    
    public int defendStrategy() {
        int refreshRate = is.run("");
        if(refreshRate != -1)
            return refreshRate;
        
        print("Couldn't decide on a defense strategy");
        return 750;
    }

    public boolean setMode() {
        double ourDistance = myRobot.distanceTo(ball);
        double oppDistance = oppRobot.distanceTo(ball);

        // if we are closer to the ball then they are...ATTACK.
        if(oppDistance > ourDistance)
            return true;

        // if they are closer than us.
        if(oppDistance < ourDistance) {
            // if they they are much much closer than us. Defend.
            double difference = oppDistance - ourDistance;
            if(difference > 30) {
                return false;
            } else {
                // do something clever rather than just go after the ball and attack?
                return true;
            }
        }
        return true;
    }
    
}
