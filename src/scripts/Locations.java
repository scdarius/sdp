package scripts;

import strategy.AccessorMethods;
import strategy.Script;

/**
* Prints out the locations of all the game objects every 4 seconds.
*
*/

public class Locations extends AccessorMethods implements Script {
	public int run(String param) {
		String ball_loc = ball.getLocation().toString();
		String myRobot_loc = myRobot.getLocation().toString();
		String oppRobot_loc = oppRobot.getLocation().toString();
		double distanceTo = myRobot.distanceTo(ball);

		print("Ball: " + ball_loc + " My Robot: " + myRobot_loc + " Their Robot " + oppRobot_loc);
		print("Our direction " + myRobot.getAngle() + " Their angle " + oppRobot.getAngle());
		print("Distance to ball " + distanceTo);
		return 4000;
	}
}
