package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.ScriptEngine;
import strategy.world.Tile;
import commander.ControllerGUI;

import plan.navigation.*;
import strategy.world.Vector;


public class AStarArc extends AccessorMethods implements Script {
    int speedMax;
    boolean setToMax = true;
    boolean setToMedium = false;
    boolean setToMin = false;
    private void setSpeed(Tile robotPos, Tile goal) {
                
        //return maxspeed/4;
         if (robotPos.distanceTo(goal) > 20) {
            if (setToMax)
                return;
            bot.setSpeed(speedMax);
            setToMax = true;
            setToMedium = false;
            setToMin = false;
            return;
         }
         if (robotPos.distanceTo(goal) > 15) {
            if (setToMedium)
                return;
            bot.setSpeed(speedMax/2);
            setToMax = false;
            setToMedium = true;
            setToMin = false;           
            return;
         }
         if (robotPos.distanceTo(goal) > 5) {
            if (setToMin)
                return;
            bot.setSpeed(speedMax/3);
            setToMax = false;
            setToMedium = false;
            setToMin = true;           
            return;
         }
    }
    public AStarArc() {
        //speedMax = bot.getSpeedMax();
        speedMax = 42;
  		bot.setSpeed(speedMax);
    }
    
    public int run(String param) {
    	long time = System.currentTimeMillis(); 
        NavigateToPoint navTile = new NavigateToPoint();
        Tile myRobotLocation = myRobot.getLocation(); //Gets our x y coords as a float
		Tile oppRobotLocation = oppRobot.getLocation();
		Tile ballLocation = ball.getLocation();
	    Tile goalLocation = getGoalScoringTile();	
		
		//declarations of things i need
        Obstacle top = new LineObstacle(new Grid(0, 0), new Grid(244, 0));
		Obstacle bottom = new LineObstacle(new Grid(0, 122), new Grid(244, 122));
		Obstacle left = new LineObstacle(new Grid(0, 0), new Grid(0, 122));
		Obstacle right = new LineObstacle(new Grid(244, 0), new Grid(244, 122));
		
		
		Obstacle opponent = new RoundObstacle(new Grid((int) oppRobotLocation.x, (int) oppRobotLocation.y), 16);
		Obstacle ball = new RoundObstacle(new Grid((int) ballLocation.x, (int) ballLocation.y), 5);
		
		Grid goal = new Grid((int)goalLocation.x, (int)goalLocation.y);
		Robot robot = new Robot(new Grid((int)myRobotLocation.x, (int)myRobotLocation.y), 16);
		
		//When would it ever be true that distanceTo is less than zero?
		if (robot.distanceTo(opponent) < 0) //this should make us go away from the opponent
		    opponent = new RoundObstacle(new Grid((int) oppRobotLocation.x, (int) oppRobotLocation.y), 6);

		if (robot.distanceTo(ball) < 0){ 
					robot = new Robot(new Grid((int)myRobotLocation.x, (int)myRobotLocation.y), 10);
					ball = new RoundObstacle(new Grid((int) ballLocation.x, (int) ballLocation.y), 2.5); 
		}
		    
		    
		Obstacle obstacles[] = {opponent, ball, top, bottom, left, right};
		RoundObstacle robstacles[] = {(RoundObstacle) opponent, (RoundObstacle) ball};
		
		
		PathFinder aStar = new PathFinder(robot, goal, obstacles, 244, 122);
		
		Grid path[] = aStar.findPath();
		
		int last_path_length = 0;
		print("Taken : "+(System.currentTimeMillis() - time));
		do {
			last_path_length = path.length;
			path = RamerDouglasPeuckerFilter.filter(path, 1);
			path = ExtraPointsFilter.filter(path, robstacles, robot.radius);
		} while (path.length != last_path_length);
        
        path = ConnectPointsFilter.filter(path, 10);
        
        Tile tilePath[] = new Tile[path.length-1];
        System.out.println(goal);
        for (int i=1; i< path.length; i++) {
            //System.out.println(path[i]);
            tilePath[i-1] = new Tile((double)path[i].x, (double)path[i].y);
        }
        
        print("Taken : "+(System.currentTimeMillis() - time));
        //these conditions should be changed and the end of this script should be different
        if (myRobotLocation.distanceTo(goalLocation) < 4) {
            Tile shoot;

            if (shootLeft) 
                shoot =new Tile(0, 61);
            else 
                shoot = new Tile(244, 61);
            if ( ! isFacingTile(shoot, myRobot) ){
                stop();
                print("turning");
                faceTile(shoot);            
            }
            bot.setSpeed(speedMax);
            bot.go();
            wait(200);
            kick();
            stop();     
		    return -1;
        }
        
        
        int refRate = 50;
        //get to the first point in the path
        int angle = (int) Math.toDegrees(getAngle(myRobot.loc, myRobot.direction, tilePath[0]));
        int radius = getRadius(myRobot.loc, myRobot.direction, tilePath[0]);
        if (angle == 0) angle++;
        

		double newBearing = myRobot.getLocation().bearingTo(tilePath[0]);
		double angleToGoal = (((360 - myRobot.getAngle()) + newBearing) % 360);
		if (angleToGoal > 180)
			angleToGoal -= 360;
		
		print("angle to next tile: " + angleToGoal);
        
        if ((angleToGoal > 45) || (angleToGoal < -45))    
        {
            if ((angleToGoal > 135) || (angleToGoal < -135)) {
                angle *=-1;
            }
            else {
                print("Faster to turn. Angle to goal is: " + angleToGoal);  
                faceTile(tilePath[0]);
                return 0;
            }
        }
        
        
         
        //we need to decide if we wantto move to the left or to the right. we can do this by negating the radius.
        //negative radius will move to the left, positive to the right
        Tile center = getCenter(myRobot.loc, myRobot.direction, tilePath[0]);
        newBearing = myRobot.getLocation().bearingTo(center);
		double an = (((360 - myRobot.getAngle()) + newBearing) % 360);
		if (an > 180) {
		    radius *= -1;
		    angle *= -1;
        }
      
//        print("angle: " + angle + " radius: " + radius);
        setSpeed(myRobot.loc, goalLocation)  ;
        moveArc(radius, angle);

		ControllerGUI.clear();
		ControllerGUI.setPath(tilePath);
		
        return refRate;
    }
    public int getRadius(Tile p1, Tile p2, Tile p3) { 
	    Tile center = getCenter(p1, p2, p3);
	    double radius = center.distanceTo(p1);
		return (int) radius;
    }
	       
	
	public double getAngle(Tile p1, Tile p2, Tile p3) { 
		Tile center = getCenter(p1, p2, p3);
	    double radius = center.distanceTo(p1);
		// distance to third tile
	    double dist = p1.distanceTo(p3);

	    double angle = Math.asin(dist / (2 * radius)); // in radians -.-

	    return angle;
	}  
	
    
    public Tile getCenter(Tile p1, Tile p2, Tile p3) {
        Vector v1 = new Vector(p1);
        Vector v2 = new Vector(p2);
        Vector v3 = new Vector(p3);
            
        Tile midPoint1 = v1.getMidpoint(v2);
        //print("Midpoint1 " + midPoint1.toString());
        Tile midPoint2 = v3.getMidpoint(v2);
        //print("Midpoint2 " + midPoint2.toString());

        Vector v4 = new Vector(p1, p2);
        Vector v5 = new Vector(p2, p3);

        Tile dir1 = v4.getDirection();
        dir1 = new Tile(0 - dir1.y, dir1.x);

        Tile dir2 = v5.getDirection();
        dir2 = new Tile(0 - dir2.y, dir2.x);

        v4.setDirection(dir1);
        v4.setPoint(midPoint1);
        v5.setDirection(dir2);
        v5.setPoint(midPoint2);
        
        print(v4.toString());
        print(v5.toString());

        return v4.getIntersectionPt(v5);

    }

    
}
