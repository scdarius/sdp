package scripts;

import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.GameArea;
import strategy.world.Tile;

public class ShootingObstructed extends AccessorMethods implements Script {
	public int run(String s) {

		if(isShootingObstructed(myGoal.getLocation())) {
			print("Obstructed");
		} else {
			print("Not obstructed");
		}
		
		Tile t = ball.getLocation();
		if(t.isOccupied()) {
			print("occupied");
		} else {
			print("unoccupied");
		}
		
		Tile t2 = new Tile(10, 10);
		if(t2.isOccupied()) {
			print("occupieds");
		} else {
			print("unoccupieds");
		}
		
	
		
		return 1000;
	}

}
