package scripts;
import commander.*;
import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.*;

public class TestDraw extends AccessorMethods implements Script {
	public int run(String param) {
		Tile one = new Tile(10,10);
		Tile two = new Tile(20,15);
		Tile three = new Tile(35, 7);
		Tile[] tiles = new Tile[3];
		tiles[0] = one;
		tiles[1] = two;
		tiles[2] = three;
		ControllerGUI.clear();
		ControllerGUI.setPath(tiles);
        //ControllerGUI.paintPathNotFound();
		return 10;
	}
}
