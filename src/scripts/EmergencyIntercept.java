package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.world.Tile;
import strategy.world.WorldState;
import strategy.BallProjection;
import strategy.BallMonitor;
import commander.ControllerGUI;

public class EmergencyIntercept extends AccessorMethods implements Script{
	boolean ballMoved = false;
	boolean faceDown = false;
	Tile prevBall = null;
	Tile thisBall = null;
	double bDiff = 0;
	public int run(String param){
		bot.setSpeed(bot.getSpeedMax());
		thisBall = ball.getLocation();
		if(prevBall == null){
			prevBall = ball.getLocation();
			return 50;
		}

		//Is the ball moving? Difference between current ball position and last position
		bDiff = prevBall.distanceTo(thisBall);
		if(bDiff > 2){	
			double currAng = myRobot.getAngle();
			if(!ballMoved){
				if(currAng > 0 &&  currAng < 180){
					//face down
					faceTile(new Tile(myRobot.getLocation().x, 122));
					System.out.println("Escaped the facedown");
					ControllerGUI.setPoint(new Tile(myRobot.getLocation().x , 122));
					faceDown = true;
				}else{
					//face up
					faceTile(new Tile(myRobot.getLocation().x, 0));
					System.out.println("Escaped the faceup");
					ControllerGUI.setPoint(new Tile(myRobot.getLocation().x, 0));
				}
				ballMoved = true;
			}
			else{
				if(Math.abs(myRobot.getLocation().y - ball.getLocation().y) < 5){
					stop();
					System.out.println("Stopping");
				}
				else if(faceDown){
					if(myRobot.getLocation().y < ball.getLocation().y){
						moveForward();
						System.out.println("Facing down, moving forwards");
					}
					else{
						moveBackward();
						System.out.println("Facing down, moving backwards");
					}
				}
				else{
					if(myRobot.getLocation().y < ball.getLocation().y){
						moveBackward();
						System.out.println("Facing up, moving backwards");
					}
					else{
						moveForward();
						System.out.println("Facing up, moving forwards");
					}
				}
			}
		}
		else if(ballMoved && bDiff <= 2){
			kick();
			return -1;
		}
		prevBall = ball.getLocation();
		return 50;
	}
}
	
