package scripts;

import strategy.AccessorMethods;
import strategy.Script;

/**
 * Since i'm assuming we'll be needing the change the real 'NavigateAndShoot'. Keep this
 * for the second part of Ms3.
 */
public class MS3NavigateAndShoot extends AccessorMethods implements Script {

        NavigateToBall nvb = new NavigateToBall();
        public int run(String param) {
            int returnWait = shoot();

            // if we needed to shoot. Return for next check.
            if(returnWait != -1)
                return returnWait;


            returnWait = nvb.run("");
            if(returnWait != -1)
                return returnWait;

            print("Cant navigate and can't shoot...missing ball?");
            return 2000;
        }

        public int shoot() {
            if(!canShoot(myRobot, myGoal))
                return -1;

            kick();

            return 100;
        }
}
