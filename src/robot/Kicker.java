package robot;

import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.Motor;
import lejos.nxt.UltrasonicSensor;

public class Kicker extends Thread{
        Controller control;

	public int kickerSpeed = 900;
	public int kickerAngle = 60;

	public Kicker(Controller control){
		this.control = control;
	}
	
	public void run(){
                control.kickerMotor.setSpeed(kickerSpeed);
                control.kickerMotor.resetTachoCount();
                control.kickerMotor.rotateTo(-kickerAngle);
		control.kickerMotor.rotate(kickerAngle);
	}
}
