package robot;

import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.navigation.DifferentialPilot;

public class Sensors extends Thread {
        public static final int rotationAngle = 45;
        public static final int backwardsDistance = 8;

        TouchSensor left;
        TouchSensor right;

        Controller control;

        public Sensors(Controller c, TouchSensor left, TouchSensor right){
                control = c;
                this.left = left;
                this.right = right;
        }

        public void react() {
                boolean leftSensorPressed = left.isPressed();
                boolean rightSensorPressed = right.isPressed();
                DifferentialPilot pilot = control.pilot;

                control.controlsBlocked = leftSensorPressed || rightSensorPressed;

                if (control.controlsBlocked) {
                        int lastSpeed = control.currentSpeed;
                        LCD.clear();
                        pilot.setTravelSpeed(control.maxSpeed);
                        pilot.travel(-backwardsDistance);
				
				
				
				/*
				*This section checks if a sensor is pressed, waits 0.75 secs, if the sensor is still pressed the robot moves back. 
				*NOTE: This Untested - I do not have a key.
				* Jack.
				*/
                        if (leftSensorPressed) {
                                try{
                                	Thread.sleep(750); 
                                }
                                catch(Exception e){
                                	
                                }
                                leftSensorPressed = left.isPressed(); 
                                if (leftSensorPressed){ 
                                	pilot.rotate(control.fixAngle(rotationAngle));
                        	  }
                        } 
                        
                        else if (rightSensorPressed) {
                                try{
                                	Thread.sleep(750); 
                                }
                                catch(Exception e){
                                	
                                }
                                rightSensorPressed = right.isPressed(); 
                                if (rightSensorPressed){ 
                                	pilot.rotate(control.fixAngle(-rotationAngle));
                        	  }
                                
                        }

                        control.controlsBlocked = false;
                        control.setSpeed(lastSpeed);
                }
        } 

        public void run()
        {
                while (!this.isInterrupted()) {
                        react();
                }
        }

}
