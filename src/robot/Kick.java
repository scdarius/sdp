package robot;

import lejos.nxt.Motor;
import lejos.robotics.navigation.DifferentialPilot;

public class Kick {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DifferentialPilot pilot = new DifferentialPilot(8, 16, Motor.A, Motor.C, false);
		Controller control = new Controller(pilot);
		
		control.kick();
		try {
			Thread.sleep(1000);
		} catch (Exception e){
			// 
		}
	}

}
