package robot;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Hashtable;

import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.robotics.navigation.DifferentialPilot;

public class BotComm
{	

        public enum Message {
                QUIT(-1, 0),

                GO(1, 0), 
		STOP(2, 0), 
		KICK(3, 0), 
		FLOAT(4, 0),
                SET_SPEED(5, 1), 
		ROTATE(6, 1),
                ACCELERATE_ON(7, 0), 
		ACCELERATE_OFF(8, 0), 
		DEFEND(9, 0), 
		TRAVEL(10, 1),
                ARC(11, 2),
		GET_MOVE_SPEED_MAX(12, 0),
		GET_LEFT_SENSOR_STATUS(13, 0),
		GET_RIGHT_SENSOR_STATUS(14, 0),
		//GET_DISTANCE_US(15,0),
                REVERSE(16, 0), 
		IS_MOVING(17,0),
                CONTROLS_BLOCKED(18, 0),
                //DISABLE_SPEED_REGULATION(19, 0),
                //ENABLE_SPEED_REGULATION(20, 0),
		STEER(21,2),
		ROTATE_IMMEDIATE(30,2);

                public final int code;
                public final int argumentCount;

                Message(int code, int argumentCount) {
                        this.code = code;
			this.argumentCount = argumentCount;
                }
        }

	public static final int COMMAND_SUCCESS = 0;
	public static final int COMMAND_FAILURE = 1;
	public Boolean quit = false;

        // Map for converting the network codes to robot messages
        public static Hashtable<Integer, Message> codeToMessage;

	DifferentialPilot pilot;
	Controller control;
        BTConnection btc;
	DataInputStream dis;
	DataOutputStream dos;

	Sensors sensor;
	
        static {
                codeToMessage = new Hashtable<Integer, Message>();
                for (Message message : Message.values())
                {
                        codeToMessage.put(message.code, message);
                }
        }

 	/**
	 * Constructor method. Takes robot dimensions as arguments, sets the pilot object.
	 * @param a The wheel diameter, in any standard unit.
	 * @param b The wheelbase width, in the same units as a.
	 */
	public BotComm(int wheelDiameter, int wheelbaseWidth, BTConnection btc) {
		this.pilot = new DifferentialPilot(wheelDiameter, wheelbaseWidth, Motor.A, Motor.C, true);
		this.control = new Controller(pilot);
                this.btc = btc;
                this.dis = btc.openDataInputStream();
                this.dos = btc.openDataOutputStream();

	}
	
        int readInt() throws IOException {
                LCD.clear();
		LCD.drawString("Reading...", 0, 0);
		LCD.refresh();
                
                int ret = dis.readInt();
                LCD.clear();
                LCD.drawString("Got int" + ret, 0, 0);
		LCD.refresh();

                StringBuilder strBuilder = new StringBuilder();
                LCD.clear();
                strBuilder.append("Message: ");
                strBuilder.append(ret);
                LCD.drawString(strBuilder.toString(), 0, 0);
                LCD.refresh();

                return ret;
        }
	/**
	 * The wait method. Takes two streams as input. Reads integer from the server,
	 * processes it, and then writes out to the server with a 0 or 1 for. It then
	 * loops again. Escapable by sending a -1 to the robot from the server.
	 * @throws IOException Thrown if there's a failure either on rserver.send(2);eading the input
	 * stream or on writing to output stream.
	 */
	boolean listen() throws IOException{
//		LCD.clear();
//		LCD.drawString("Getting message", 0, 0);
//		LCD.refresh();

		int messageCode = readInt();
                Message message = codeToMessage.get(messageCode);

                int argCount = message.argumentCount;

                ArrayList<Integer> arguments = new ArrayList<Integer>(argCount);
                for (int i = 0; i < argCount; ++i)
                {
			dos.writeInt(0);
                        dos.flush();
                        arguments.add(readInt());
                }

		// try
		int result = process(message, arguments);

                writeOnScreen("Sending back: " + result);

		dos.writeInt(result);
		writeOnScreen("Wrote int");
		dos.flush();
		writeOnScreen("Flushed");
		//System.gc();
		writeOnScreen("Garbage collected?");
		if (quit) return false;
		return true;
		// except
		/*} else {
			dos.writeInt(1);
			dos.flush();
			return false;
		}*/
	}

        void writeOnScreen(String message) {
        	LCD.clear();
                LCD.drawString(message, 0, 0);
		LCD.refresh();
        }

	/**
	 * Processes the integer message from wait().
	 * @param message The message given.
	 * @param arguments ArrayList of message arguments
	 * @return Command success value or result of a query
	 */
	
        int process(Message message, ArrayList<Integer> arguments) {

                writeOnScreen("" + message);

                switch (message) {
                        case GO:
                                writeOnScreen("start go");
                                control.go();
                                writeOnScreen("end go");
                                return COMMAND_SUCCESS;
                        case REVERSE:
                                control.reverse();
                                return COMMAND_SUCCESS;
                        case STOP:
                                control.stop();
                                return COMMAND_SUCCESS;
                        case KICK:
                                control.kick();
                                return COMMAND_SUCCESS;
                        case FLOAT:
                                control.floatWheels();
                                return COMMAND_SUCCESS;
                        case SET_SPEED:
                                control.setSpeed(arguments.get(0)); 
                                return COMMAND_SUCCESS;
                        case ROTATE:
                                control.rotate(arguments.get(0));
                                return COMMAND_SUCCESS;
                        case ROTATE_IMMEDIATE:
                                control.rotateContinue(arguments.get(0));
                                return COMMAND_SUCCESS;
                        /*case ENABLE_SPEED_REGULATION:
                                control.enableSpeedRegulation();
                                return COMMAND_SUCCESS;
                        case DISABLE_SPEED_REGULATION:
                                control.disableSpeedRegulation();
                                return COMMAND_SUCCESS;*/
                        case ACCELERATE_ON:
                                control.ballCarrier = true;
                                return COMMAND_SUCCESS;
                        case ACCELERATE_OFF:
                                control.ballCarrier = false;
                                return COMMAND_SUCCESS;
                        case DEFEND: // TODO: do we need this? no
	         		control.setSpeed(15);
	         		control.travel(10);
	         		control.travel(-10);
	         		control.stop();
	         		control.setSpeed(control.maxSpeed);
	         		return COMMAND_SUCCESS;
                        case TRAVEL: // TODO: do we need this? yes
        			int dist = arguments.get(0);
        			int a = (int) (dist * 1);
        			control.travel(a);
	         		return COMMAND_SUCCESS;
                        case ARC:
                                int radius = arguments.get(0);
                                int angle = arguments.get(1);
                                control.arc(radius, angle);
                                return COMMAND_SUCCESS;
                        case QUIT:
				quit = true;
                                return COMMAND_SUCCESS;
			case GET_MOVE_SPEED_MAX:
				return (int) Math.round(control.getMoveMaxSpeed());
			case GET_LEFT_SENSOR_STATUS:
				return control.leftSensorPressed() ? 1 : 0;
			case GET_RIGHT_SENSOR_STATUS:
				return control.rightSensorPressed() ? 1 : 0;
			/*case GET_DISTANCE_US:
				return control.getDistanceUS();*/
                	case IS_MOVING:
                		return control.isMoving() ? 1 : 0;
                        case CONTROLS_BLOCKED:
                                return control.isBlocked() ? 1 : 0;
			case STEER:
				int turnRatio = arguments.get(0);
				int rads = arguments.get(1);
				control.steer(turnRatio, rads);
                }
                return COMMAND_FAILURE;
        }

        void disconnect() throws IOException 
        {
                control.stopSensors();
                dos.close();
                dis.close();
                btc.close();
        }


	/**
	 * The BlueTooth connection method. Connects, and returns the connection as an object.
	 */
	static BTConnection connect(){
		LCD.drawString("Awaiting...", 0, 0);
		BTConnection btc = Bluetooth.waitForConnection();
		LCD.refresh();
		LCD.clear();
		LCD.drawString("Connected...", 0, 0);
		LCD.refresh();
		return btc;
	}
	
	/**
	 * The main method. Initializes objects, runs the wait loop.
	 * @param args (Message line) arguments.
	 * @throws Exception IOException from the bluetooth communications.
	 */
	public static void main(String [] args)  throws Exception {
		// Voicebox voice = new Voicebox();
		while (true)
		{
			BTConnection btc = connect();
            BotComm bot = new BotComm(14, 14, btc); //the size of the wheel is actually 8.5 but we have gears with ratio 1.5 which gives us 12.75, almost 13
		
		    /* The waiting loop */
			boolean listening = true;
			while(listening){
				try {
					LCD.clear();
					LCD.drawString("Listening...", 0, 0);
					LCD.refresh();
					listening = bot.listen();
				} catch (IOException e){
                                        bot.disconnect();
				}
			}
                        bot.disconnect();
			try {
				Thread.sleep(1000);
			} catch (Exception e){
			}
		}
	}
}
