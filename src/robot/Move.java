package robot;

import lejos.nxt.Motor;
import lejos.robotics.navigation.DifferentialPilot;


public class Move{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DifferentialPilot pilot = new DifferentialPilot(6, 15, Motor.A, Motor.C, false);
		Controller control = new Controller(pilot);
		control.go();
		try {
			Thread.sleep(1000);
		} catch (Exception e){
			
		}
		control.floatWheels();
		control.kick();
	}

}
