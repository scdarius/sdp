package robot;

import lejos.nxt.LCD;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.MotorPort;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.nxt.TouchSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

/** 
 * The Control class. Handles the actual driving and movment of the bot, once
 * BotCommunication has processed the commands.
 * @author shearn89
 */
public class Controller {
	public DifferentialPilot pilot;
        public NXTRegulatedMotor kickerMotor;
	public int maxSpeed= 50; // 20 for wimps
	public int turnSpeed = 100;
        public int currentSpeed = maxSpeed;
	public float rotationCalibrationFactor = 1;// / 1.5f;
	public boolean ballCarrier = false;
	public boolean controlsBlocked = false;

	private TouchSensor left;
	private TouchSensor right;
	private UltrasonicSensor back;
        private Kicker kicker = null;
        public Sensors sensors;
	/**
	 * Constructor method: takes the Pilot object from the main program.
	 * @param pilot
	 */
	public Controller(DifferentialPilot p){
		pilot = p;
		setSpeed(maxSpeed);
		//pilot.setTurnSpeed(pilot.getTurnSpeed());
		//pilot.regulateSpeed(true);
                kickerMotor = new NXTRegulatedMotor(MotorPort.B);
		//Motor.A.smoothAcceleration(true);
		//Motor.C.smoothAcceleration(true);
	//	pilot.setTurnSpeed(turnSpeed);
	//	pilot.regulateSpeed(true);
        //        kickerMotor = Motor.B;
	//	Motor.A.smoothAcceleration(true);
	//	Motor.C.smoothAcceleration(true);
		left = new TouchSensor(SensorPort.S1);
		right = new TouchSensor(SensorPort.S2);
                sensors = new Sensors(this, left, right);
                sensors.start();

                
	}
	/**
	 * Makes the robot reverse until it receives a stop() command.
	 */
	public void reverse(){
                if (controlsBlocked) return;
		pilot.backward();
	}
	/**
	 * Makes the robot go until it receives a stop() command.
	 */
	public void go(){
                if (controlsBlocked) return;
		pilot.forward();
	}

	/** Moves forward with a ratio between the left and right wheel speeds
	 */

	public void steer(int turnRate, int radius){
		if(controlsBlocked) return;
		pilot.steer((float) turnRate, (float) radius, true);
	}



	/**
	 * Floats the motors, allowing the robot to drift.
	 */
	public void floatWheels(){
                if (controlsBlocked) return;
		//Motor.A.flt();
		//Motor.C.flt();
	}
	/**
	 * Makes the robot stop.
	 */
	public void stop(){
                if (controlsBlocked) return;
		pilot.stop();
	}
	/**
	 * The robot will rotate deg degrees.
	 * @param deg The degree to rotate. +ve is right, -ve left.
	 */
	public void rotate(int deg){
                if (controlsBlocked) return;
		pilot.rotate(fixAngle(deg)/*, true*/);
	}

	public void rotateContinue(int deg){
                if (controlsBlocked) return;
		pilot.rotate(fixAngle(deg), true);
	}
	
	/*
        public void disableSpeedRegulation() {
                if (controlsBlocked) return;
		pilot.regulateSpeed(false);
        }

        public void enableSpeedRegulation() {
                if (controlsBlocked) return;
		pilot.regulateSpeed(true);
		Motor.A.smoothAcceleration(true);
		Motor.C.smoothAcceleration(true);
        }
        */

        public float fixAngle(int deg)
        {
                return deg * rotationCalibrationFactor;
        }
	/**
	 * Moves the robot dist distance. Only used by the Sensors class
	 * @param dist the distance to move.
	 */
	public void travel(int dist){
                if (controlsBlocked) return;
		pilot.travel(dist);
	}
	/**
	 * Sets the motor speed.
	 * @param speed
	 */
	public void setSpeed(int speed) {
                if (controlsBlocked) return;
		pilot.setTravelSpeed(speed);
                currentSpeed = speed;
	}
	/**
	 * Kicks the ball.
	 */
	public void kick(){
                if (controlsBlocked) return;
                if (kicker == null || !kicker.isAlive())
                {
                        kicker = new Kicker(this);
                        kicker.start();
                }
	}

	/**
	 * Moves the robot along an arc
	 */
        public void arc(int radius, int angle)
        {
                if (controlsBlocked) return;
                pilot.arc(radius, angle, true);
        }

        public void arc(int radius, int angle, boolean immediateReturn)
        {
                if (controlsBlocked) return;
                pilot.arc(radius, angle, immediateReturn);
        }


	/**
	 * Returns robot's maximum movement speed
	 */
	public double getMoveMaxSpeed()
	{
		return pilot.getMaxTravelSpeed();
	}

	public boolean leftSensorPressed()
	{
		return left.isPressed();
	}

	public boolean rightSensorPressed()
	{
		return right.isPressed();
	}
	
	/**
	*returns true if robot is moving
	*/
	public boolean isMoving(){
		/*if (Motor.A.getSpeed() != 0 || Motor.C.getSpeed() !=0){
			return true;
		}
		else{
			return false;
		}*/
		return pilot.isMoving();
	}

        public boolean isBlocked() {
                return controlsBlocked;
        }
	
        public void stopSensors()
        {
                sensors.interrupt();
        }
        /*
	public void reactToSensors() {
                int lastSpeed = currentSpeed;
                boolean leftSensorPressed = left.isPressed();
                boolean rightSensorPressed = right.isPressed();

                controlsBlocked = leftSensorPressed || rightSensorPressed;

                if (controlsBlocked) 
                        LCD.clear();
                        setSpeed(maxSpeed);

                if (leftSensorPressed || rightSensorPressed)
                        travel(-5);

                if (leftSensorPressed) {
                        rotate(45);
                } else if (rightSensorPressed) {
                        rotate(-45);
                }

                controlsBlocked = false;
                setSpeed(lastSpeed);
        }
        */

}
