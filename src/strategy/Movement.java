package strategy;

import plan.navigation.ConnectPointsFilter;
import plan.navigation.ExtraPointsFilter;
import plan.navigation.Grid;
import plan.navigation.LineObstacle;
import plan.navigation.Obstacle;
import plan.navigation.PathFinder;
import plan.navigation.RamerDouglasPeuckerFilter;
import plan.navigation.Robot;
import plan.navigation.RoundObstacle;
import strategy.world.Tile;
import strategy.world.Vector;
import strategy.world.WorldState;

public class Movement {
	public static Tile[] getPath(Tile goalLocation) {
	       Tile myRobotLocation = WorldState.myRobot.getLocation(); //Gets our x y coords as a float
			Tile oppRobotLocation = WorldState.oppRobot.getLocation();
			Tile ballLocation = WorldState.ball.getLocation();
		    
			
			//declarations of things i need
	        Obstacle top = new LineObstacle(new Grid(0, 0), new Grid(244, 0));
			Obstacle bottom = new LineObstacle(new Grid(0, 122), new Grid(244, 122));
			Obstacle left = new LineObstacle(new Grid(0, 0), new Grid(0, 122));
			Obstacle right = new LineObstacle(new Grid(244, 0), new Grid(244, 122));
			
			
			Obstacle opponent = new RoundObstacle(new Grid((int) oppRobotLocation.x, (int) oppRobotLocation.y), 14);
			Obstacle ball = new RoundObstacle(new Grid((int) ballLocation.x, (int) ballLocation.y), 4);
			
			Grid goal = new Grid((int)goalLocation.x, (int)goalLocation.y);
			Robot robot = new Robot(new Grid((int)myRobotLocation.x, (int)myRobotLocation.y), 14);
			
			if (robot.distanceTo(opponent) < 0) //this should make us go away from the opponent
			    opponent = new RoundObstacle(new Grid((int) oppRobotLocation.x, (int) oppRobotLocation.y), 6);
			if (robot.distanceTo(ball) < 0) 
			    robot = new Robot(new Grid((int)myRobotLocation.x, (int)myRobotLocation.y), 10);
			    ball = new RoundObstacle(new Grid((int) ballLocation.x, (int) ballLocation.y), 2.5);
			    
			    
			Obstacle obstacles[] = {opponent, ball, top, bottom, left, right};
			RoundObstacle robstacles[] = {(RoundObstacle) opponent, (RoundObstacle) ball};
			
			
			PathFinder aStar = new PathFinder(robot, goal, obstacles, 244, 122);
			
			Grid path[] = aStar.findPath();
			
			int last_path_length = 0;
			do {
				last_path_length = path.length;
				path = RamerDouglasPeuckerFilter.filter(path, 1);
				path = ExtraPointsFilter.filter(path, robstacles, robot.radius);
			} while (path.length != last_path_length);
	        
	        path = ConnectPointsFilter.filter(path, 10);
	        
	        Tile tilePath[] = new Tile[path.length-1];
	        System.out.println(goal);
	        for (int i=1; i< path.length; i++) {
	            //System.out.println(path[i]);
	            tilePath[i-1] = new Tile((double)path[i].x, (double)path[i].y);
	        }
	        return tilePath;
	}
    
    class ArcThreePoints {
        // bit longer than it ought to be as spotted an 'add' bug used in the midpoint, so working
        // around it.
        public Tile getCenter(Tile p1, Tile p2, Tile p3) {
            Vector v1 = new Vector(p1);
            Vector v2 = new Vector(p2);
            Vector v3 = new Vector(p3);

            Tile midPoint1 = v1.getMidpoint(v2);
            //print("Midpoint1 " + midPoint1.toString());
            Tile midPoint2 = v3.getMidpoint(v2);
            //print("Midpoint2 " + midPoint2.toString());

            Vector v4 = new Vector(p1, p2);
            Vector v5 = new Vector(p2, p3);

            Tile dir1 = v4.getDirection();
            dir1 = new Tile(0 - dir1.y, dir1.x);

            Tile dir2 = v5.getDirection();
            dir2 = new Tile(0 - dir2.y, dir2.x);

            v4.setDirection(dir1);
            v4.setPoint(midPoint1);
            v5.setDirection(dir2);
            v5.setPoint(midPoint2);


            return v4.getIntersectionPt(v5);
            
        }    
    }

}
