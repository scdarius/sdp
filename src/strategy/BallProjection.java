package strategy;

import strategy.world.Tile;
import strategy.world.Ball;
import strategy.world.Robot;
import strategy.world.WorldState;

import java.util.ArrayList;
import java.awt.geom.*;

/**
 * Calculating ball projections.
 *
 * All methods here and in BallMonitor represent the ball's motion as broken down into line segments, with points
 * being stored as previously recorded ball locations (taken once every frame).
 * 
 * All calculations here use a fixed time unit of 1; i.e. a fixed frame rate is assumed. Because of this, velocity
 * is not taken into consideration and a displacement of, say, 3 cm between one frame and the next is taken to
 * indicate a velocity of 3 cm per frame.
 *
 * If the frame rate varies beyond reasonable fluctuations that may be ignored, a way to take velocity into account
 * should be devised.
 */

public class BallProjection {

	/**
	 * 4 constants hold the values for the pitch edges. Two more constants hold the directions for CW and CCW,
	 * according to methods in the Line2D class. The geometry classes are used for the sole purpose of determining
	 * which edge the ball is going to hit.
	 *
	 * It might be a good idea to define additional constants that represent lines near the pitch edges (e.g. 10cm
	 * off the edges) that will be used to calculate Edge Projection so that the robot could navigate to these points
	 * without getting stuck against the wall.
	 */
		
	public static final double EDGE_LEFT = 0.0;
	public static final double EDGE_RIGHT = 244.0;
	public static final double EDGE_BOTTOM = 122.0;
	public static final double EDGE_TOP = 0.0;
	
	public static final int CW = 1;
	public static final int CCW = -1;
	
	public static final Point2D.Double CORNER_TOP_LEFT = new Point2D.Double (0.0, 0.0);
	public static final Point2D.Double CORNER_TOP_RIGHT = new Point2D.Double (244.0, 0.0);
	public static final Point2D.Double CORNER_BOTTOM_RIGHT = new Point2D.Double (244.0, 122.0);
	public static final Point2D.Double CORNER_BOTTOM_LEFT = new Point2D.Double (0.0, 122.0);	
	
	/**
	 * outOfBounds and reflect are used to calculate projection when the ball goes "out of bounds" i.e.
	 * bounces off a wall. They may be redundant if wall bouncing is not used as part of strategy.
	 */
	 
	public static boolean outOfBounds(Tile projection) {
		return (projection.x < EDGE_LEFT || projection.x > EDGE_RIGHT
		        || projection.y > EDGE_BOTTOM || projection.y < EDGE_TOP);
	}
	
	/**
	 * The basic reflection method that will calculate the "actual" location of a "theoretical" projection.
	 * It will even work for "double" reflections, e.g. reflection in a corner when the "theoretical" projection
	 * is out of bounds on both x and y.
	 */
	 
	public static Tile reflectPosition(Tile projection) {
		double differenceX = 0.0;
		double differenceY = 0.0;
		
		if (projection.x < EDGE_LEFT) differenceX = projection.x - EDGE_LEFT;
		if (projection.x > EDGE_RIGHT) differenceX = projection.x - EDGE_RIGHT;
		if (projection.y > EDGE_BOTTOM) differenceY = projection.y - EDGE_BOTTOM;
		if (projection.y < EDGE_TOP) differenceY = projection.y - EDGE_TOP;
		
		double reflectionX = projection.x - 2 * differenceX;
		double reflectionY = projection.y - 2 * differenceY;
		
		Tile reflection = new Tile(reflectionX, reflectionY);
		
		return reflection;
	}
		
	/**
	 * "Timed projection"; this should be the most reliable projection to use for low speeds because it only looks at
	 * a given number of previous locations, i.e. it keeps itself up-to-date.
	 *
	 * Calculates the ball's expected location, assuming linear motion is preserved along the same path.
	 * The projection is found by subtracting the difference in coordinates, separately for x and for y, thus simply
	 * "translating" the right-angled triangle that has the ball's path as a hypotenuse.
	 *
	 * The method uses the ArrayLists filled with previously stored ball locations and the distances between them.
	 * When given a "time" parameter (i.e. the number of frames we want to project ahead) it will take "time" number
	 * of elements from the ArrayLists and project that far ahead.
	 * 
	 * If given a number larger than the size of the list, it will disregard it and use all available elements instead.
	 * The "time" parameter might be set to a fixed amount that may be determined via experiments.
	 */
	 
	public static Tile timedProjection (ArrayList<Tile> locations, ArrayList<Double> distances, int time) {
		Tile end = locations.get(locations.size() - 1);
		Tile start = new Tile();
		
		if (time > locations.size()) start = locations.get(0);
		else if (time < 2) start = locations.get(locations.size() - 2);
		else start = locations.get(locations.size() - time);
		
		double displacementX = end.getX() - start.getX();
		double displacementY = end.getY() - start.getY();
		double projectionX = end.getX() + displacementX;
		double projectionY = end.getY() + displacementY;
		
		Tile projectionTile = new Tile(projectionX, projectionY);
		
		if (outOfBounds(projectionTile)) return reflectPosition(projectionTile);
		
		return projectionTile;
	}
	
	/**
	 * "Edge intersection"; this calculates where the ball would hit an edge if it preserves its trajectory. It will always
	 * use all of the previously stored locations in order to calculate the projection; i.e. the first element in the list is
	 * the start point, and the last (most recent) element is the end point.
	 *
	 * This seems risky but works fine if good reset conditions are enforced; e.g. when the ball's velocity and angle of motion
	 * changes too drastically, the lists are cleared, storing new locations starts over, and a new projection is calculated.
	 */
	 
 	public static Tile edgeProjection(ArrayList<Tile> locations) {
		        
	        double edgeX = 0.0, edgeY = 0.0, projectionEdge = 0.0;
	        int relativeX = 0, relativeY = 0, relativeCorner = 0;
	        
	        double smallSide, largeSide, ratio, scaleInput, scaleOutput, unknown;
	        
	        Tile startTile = locations.get(0);
	        Tile endTile = locations.get(locations.size() - 1);
	        
	        Point2D.Double startPoint = new Point2D.Double (startTile.x, startTile.y);
	        Point2D.Double endPoint = new Point2D.Double (endTile.x, endTile.y);
	        Line2D.Double ballPath = new Line2D.Double (startPoint, endPoint);
	        
	        /**
	         * edgeX: the edge on which the value of X is fixed
	         * edgeY: the edge on which the value of Y is fixed
	         * relativeCorner: direction towards the shared corner between the X and the Y edge
	         * relativeX: direction towards the lone corner on the X edge
	         * relativeY: direction towards the lone corner on the Y edge
	         *
	         * The sole purpose of the wall of If blocks below is to determine which edge the ball is going to hit. It's
	         * not as easy as it might seem; it's actually a pain in the ass to do it, and this is the most reliable way
	         * from all the ones I tried.
	         *
 	         * Long story short, using the ball's angle of movement is not sufficient to determine the edge it's going to
	         * hit, and neither is the quadrant it's in. It can always hit two edges and I couldn't come up with a reliable
	         * way of determining the correct edge by using only angle and location.
	         *
	         * A few Line2D and Point2D objects are created only for the sake of using the relativeCCW() method in Line2D
	         * that will check in what direction a line segment needs to be turned in order to face a given point. It 
	         * returns 1 if CW, -1 if CCW (see constants above) and 0 if the line is already aligned towards the point.
	         *
	         * Each If block describes one angle quadrant (e.g. ball going top right, bottom left, etc.) and sets the
	         * above mentioned variables to the corresponding values.
	         *
	         */
	        
	        // ball going towards Right or Bottom edge
	        if (endTile.x > startTile.x && endTile.y > startTile.y) {
	                edgeX = EDGE_RIGHT;
	                edgeY = EDGE_BOTTOM;
	                relativeX = ballPath.relativeCCW(CORNER_TOP_RIGHT);
	                relativeCorner = ballPath.relativeCCW(CORNER_BOTTOM_RIGHT);
	                relativeY = ballPath.relativeCCW(CORNER_BOTTOM_LEFT);
	                if (relativeCorner == 0) return new Tile(CORNER_BOTTOM_RIGHT.getX(), CORNER_BOTTOM_RIGHT.getY());
	        }
	        
	        // ball going towards Right or Top edge
	        if (endTile.x > startTile.x && endTile.y < startTile.y) {
	                edgeX = EDGE_RIGHT;
	                edgeY = EDGE_TOP;
	                relativeX = ballPath.relativeCCW(CORNER_BOTTOM_RIGHT);
	                relativeCorner = ballPath.relativeCCW(CORNER_TOP_RIGHT);
	                relativeY = ballPath.relativeCCW(CORNER_TOP_LEFT);
	                if (relativeCorner == 0) return new Tile(CORNER_TOP_RIGHT.getX(), CORNER_TOP_RIGHT.getY());
	        }
	        
	        // ball going towards Left or Top edge
	        if (endTile.x < startTile.x && endTile.y < startTile.y) {
	                edgeX = EDGE_LEFT;
	                edgeY = EDGE_TOP;
	                relativeX = ballPath.relativeCCW(CORNER_BOTTOM_LEFT);
	                relativeCorner = ballPath.relativeCCW(CORNER_TOP_LEFT);
	                relativeY = ballPath.relativeCCW(CORNER_TOP_RIGHT);
	                if (relativeCorner == 0) return new Tile(CORNER_TOP_LEFT.getX(), CORNER_TOP_LEFT.getY());
	        }
	        
	        // ball going towards Left or Bottom edge
	        if (endTile.x < startTile.x && endTile.y > startTile.y) {
	                edgeX = EDGE_LEFT;
	                edgeY = EDGE_BOTTOM;
	                relativeX = ballPath.relativeCCW(CORNER_TOP_LEFT);
	                relativeCorner = ballPath.relativeCCW(CORNER_BOTTOM_LEFT);
	                relativeY = ballPath.relativeCCW(CORNER_BOTTOM_RIGHT);
	                if (relativeCorner == 0) return new Tile(CORNER_BOTTOM_LEFT.getX(), CORNER_BOTTOM_LEFT.getY());
	        }
	        
	        // This will then choose the correct edge out of the two possible. The rationale is that the line segment
	        // representing the ball's motion will be aligned CW to one corner and CCW to the other corner. The two corners
	        // that are in the same direction relative to the line will represent the wrong edge.
	        
	        if (relativeX == CW && relativeCorner == CCW && relativeY == CCW) projectionEdge = edgeX;
	        if (relativeX == CW && relativeCorner == CW && relativeY == CCW) projectionEdge = edgeY;
	        if (relativeX == CCW && relativeCorner == CW && relativeY == CW) projectionEdge = edgeX;
	        if (relativeX == CCW && relativeCorner == CCW && relativeY == CW) projectionEdge = edgeY;
	        
	        // This will then calculate the actual projection on the previously selected edge. This is done by simply scaling
	        // the right-angled triangle that has the ball's path as hypotenuse, until the correct leg coincides with the correct
	        // edge of the pitch.
	        
	        if (projectionEdge == edgeY) {
	                smallSide = Math.abs(endTile.y - startTile.y);
	                largeSide = Math.abs(edgeY - startTile.y);
	                ratio = largeSide / smallSide;
	                scaleInput = endTile.x - startTile.x;
	                scaleOutput = scaleInput * ratio;
	                unknown = startTile.x + scaleOutput;
	                return new Tile(unknown, edgeY);
	        }
	        
	        if (projectionEdge == edgeX) {
	                smallSide = Math.abs(endTile.x - startTile.x);
	                largeSide = Math.abs(edgeX - startTile.x);
	                ratio = largeSide / smallSide;
	                scaleInput = endTile.y - startTile.y;
	                scaleOutput = scaleInput * ratio;
	                unknown = startTile.y + scaleOutput;
	                return new Tile(edgeX, unknown);
	        }
	        
	        // The default value in case something goes wrong is the centre of the pitch.
	        return new Tile(122.0, 61.0);
	}

	/**
	 * This method will calculate where the ball would go if it were kicked from its current location, in the same direction that
	 * the robot (our or opposing)is facing, regardless of the ball's own direction. It simply calls edgeProjection using the robot's
	 * position and the ball's latest location as start and end points.
	 *
	 * This seems a bit flimsy at first, too, but could actually be quite useful in several cases, e.g.
	 *      - the ball is moving slowly or is almost stationary
	 *      - the robot is close to the ball or is moving towards it
	 *
	 * In practice, both of these would be true most of the time. Also, this might be very useful for "predicting" the opposing robot's
	 * actions without overcomplicating things; it might actually be all the opponent prediction we may ever need.
	 *
	 * Depending on choice, this may or may not be calculated when the robot is not facing the ball. If we don't want to
	 * calculate it, this method should be used in conjunction with isFacingBall(). I don't see a reason not to calculate
	 * the projection anyway - for example, the other robot might suddenly turn and face the ball.
	 */
	
	
	public static Tile projectPotentialKick (Robot r, Ball ball) {
	        Tile robot = r.getLocation();
	        Tile ballTile = ball.getLocation();
	        
	        ArrayList<Tile> points = new ArrayList<Tile>();
	        
	        points.add(robot);
	        points.add(ballTile);
	        
	        return edgeProjection(points);
	}
}
