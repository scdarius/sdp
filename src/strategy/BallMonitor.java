package strategy;

import strategy.BallProjection;

import strategy.world.Tile;
import strategy.world.Ball;
import strategy.world.Robot;
import strategy.world.WorldState;

import java.util.ArrayList;

public class BallMonitor {

        public static final double DISTANCE_MIN_THRESHOLD = 0.5;
        public static final double DISTANCE_MAX_THRESHOLD = 25.0;
        public static final double ANGLE_THRESHOLD = 10.0;
        
        public static ArrayList<Tile> ballLocations = new ArrayList();
	
	public static ArrayList<Double> distances = new ArrayList();
	
	public static void updateLocations(Tile latest) {
	        ballLocations.add(latest);
	}
	
	public static void resetLocations() {
	        ballLocations.clear();
	}
	
	public static void updateDistances(Tile latest) {
	        double latestDistance = ballLocations.get(ballLocations.size() - 1).distanceTo(latest);     
	        distances.add(latestDistance);
	}
	
	public static void resetDistances() {
		distances.clear();
	}
	
	/**
	 * This is the method that calculates all projections. The rationale is as follows: the different projections will
	 * be calculated once every frame and will be updated together with the WorldState. It should be decided what projections
	 * should be calculated and when - this should be based on decisions about velocity (i.e. distance, assuming frame rate
	 * is constant) and angle thresholds.
	 *
	 * Changed the reset/calculation decision-making as follows:
	 *
	 * - Arrays will be updated only if the ball's bearing has not changed more than a previously set threshold. Currently, this
	 *   is done by comparing the last two segments, i.e. comparing the bearing from the second last location to the current one
	 *   with the bearing from the last location to the current one. If the angle changed too drastically, the arrays will be
	 *   cleared and calculations start again.
	 *
	 * - Once the above condition is satisfied, projections will be calculated taking velocity into account. Distance travelled
	 *   is used as a substitute for velocity - this uses a constant time unit of 1, i.e. it assumes a constant frame rate. If
	 *   the ball is moving fast enough, the Edge and Kick projections will be calculated; if it's not, only the timed projection
	 *   will be calculated.
	 *
	 * Since Timed Projection will work well at low speeds (the projection it returns will get closer and closer to the ball's 
	 * actual location as speed decreases, and will coincide with it if the ball becomes stationary) it can always be used as a
	 * navigation target. The robot could by default move to the Timed Projection (i.e. "intelligently" follow the ball) and if
	 * the ball is moving fast (i.e. too fast to catch it normally) then it could move to the Edge / Kick Projection.
	 */
	
	public static void maintainArrays() {
	        Tile latest = WorldState.ball.getLocation();
		if (latest != null) {
			if (ballLocations.size() < 3) {
				updateLocations(latest);
				updateDistances(latest);
			}
			
			else {
			        int arraySize = ballLocations.size();
			        double previousBearing = ballLocations.get(0).bearingTo(ballLocations.get(arraySize - 2));
			        double latestBearing = ballLocations.get(0).bearingTo(ballLocations.get(arraySize - 1));
			        
			        if (Math.abs(latestBearing - previousBearing) < ANGLE_THRESHOLD) {
			                
			                updateLocations(latest);
				        updateDistances(latest);
			        }
			        
			        else {
			                resetLocations();
			                resetDistances();
			        } 
			}
		}	
	}
	
	public static Tile[] calculateProjections (int time) {
		
		Robot myRobot = WorldState.myRobot;
		Robot oppRobot = WorldState.oppRobot;
		Ball ball = WorldState.ball;
		
		Tile[] projections = new Tile[4];
		
		Tile latest = ball.getLocation();
		int arraySize = ballLocations.size();
	        double latestDistance = latest.distanceTo(ballLocations.get(arraySize - 1));
                
		if (ballLocations.size() > 3) {
			if (latestDistance > DISTANCE_MIN_THRESHOLD && latestDistance < DISTANCE_MAX_THRESHOLD) {
			        
			        projections[0] = BallProjection.timedProjection(ballLocations, distances, time);
			        projections[1] = BallProjection.edgeProjection(ballLocations);
			        projections[2] = BallProjection.projectPotentialKick(myRobot, ball);
			        projections[3] = BallProjection.projectPotentialKick(oppRobot, ball);
			}
			else {
			        projections[0] = BallProjection.timedProjection(ballLocations, distances, time);
			        projections[1] = null;
			        projections[2] = null;
			        projections[3] = null;
			}
		}
					
		return projections;
                
		
	}
}
