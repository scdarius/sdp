package strategy;
import strategy.world.Tile;
/**
 * Start of Compute re-write.
 * Am currently only putting methosd in here that have been generally checked.
 */
public class Calculations {
	/**
	 * Finds the angle between 2 points and the x-axis.
	 * @param a first point
	 * @param b second point
	 * @return angle from a->b, in radians within a limit of 0-360 degrees.
     	 * TODO: Would it be better to use mod here, rather than - 2pi if the angle > 2pi?
	 */
	public static double angleInRadians(Tile a, Tile b){
		double angle = Math.atan2(b.y - a.y, b.x - a.x );
		if(angle > (2 * Math.PI))
			return angle - (2 * Math.PI);

		return angle;
	}
	
	/**
	 * Same as angleInRadians(), but in degrees.
	 * @param a First point
	 * @param b Second point
	 * @return angle.
	 */
	public static double angleInDegrees(Tile a, Tile b){
		return Math.toDegrees(angleInRadians(a, b));
	}

    /**
     * Calculate angle required to turn from Tile A to Tile b.
     * @param a
     * @param b
     * @param currentAngle that we are heading.
     * @return angle to turn to reach requested destination.
     */
    public static double angleToTurn(Tile a, Tile b, double currentAngle){
        // System.err.println("Getting angle to turn: ");
        double angleToTurn = angleInRadians(a,b)- currentAngle;
        if (angleToTurn < -Math.PI) {
            angleToTurn = 2*Math.PI + angleToTurn;
        }
        if (angleToTurn > Math.PI) {
            angleToTurn = angleToTurn - 2*Math.PI;
        }
        return Math.toDegrees(angleToTurn);
    }
}
