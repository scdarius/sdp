package strategy.old;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import strategy.world.WorldState;
/**
 * 
 * @author ale
 *
 */

public class SingleClientServer implements Runnable {
    private PrintWriter out;
	private ServerSocket serverSock;
	private Socket clientSocket;
        private int PortNumber;    
	
	public SingleClientServer(String whichPort){
		if(whichPort.equals("sim")){
			PortNumber = 4561;
		}
		else{
			PortNumber = 4560;
		}
                System.out.print("Waiting for vision on port: ");
                System.out.print(PortNumber);
                System.out.println();
	}

	public void run() {
		this.go();
		
	}

	private void go() {
		try {
			System.out.println("connecting...");
			serverSock = new ServerSocket(PortNumber);
			clientSocket = serverSock.accept();
			System.out.println("got a connection");
            WorldState.visionConnected = true;
            WorldState.quitVision = false;
			out = new PrintWriter(clientSocket.getOutputStream());
			InputStreamReader in = new InputStreamReader(clientSocket.getInputStream());
			BufferedReader reader = new BufferedReader(in); 
			ComputeAndAcknowledge(reader);
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	//socket.send(ball[0], ball[1], blue_pos[0], blue_pos[1], blue_dir[0], blue_dir[1], yellow_pos[0], yellow_pos[1], yellow_dir[0], yellow_dir[1], our_goal[0], our_goal[1], their_goal[0], their_goal[1])
	private void ComputeAndAcknowledge(BufferedReader _reader) {
		String message;
		try {
			while ((message = _reader.readLine()) != null && !WorldState.quitVision) {
				WorldState.update(message);
				//System.out.println("Recieved: " + message);
			}
			System.out.println(">> Connection to vision dropped...");
			WorldState.visionConnected = false;
			clientSocket.close();
			serverSock.close();
			
			try {
				Thread.sleep(100);
			} catch (Exception e){
				e.printStackTrace();
			}
			
			if (!WorldState.quitVision){
				go();
			} else{
				System.exit(0);
			}
		} catch(IOException ex){
			System.out.println("I/O Exception: "+ ex);
		}
		
	}
}
