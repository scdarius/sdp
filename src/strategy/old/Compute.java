package strategy.old;
import java.awt.geom.Point2D;
import strategy.world.Tile;

public class Compute {
	/**
	 * Compute the Euclidean distance between a and b.
	 * @param a The first point in 2d
	 * @param b The second point in 2d
	 * @return The distance as a double.
	 */
	public static double distance(Point2D.Float a ,Point2D.Float b ){
		return PixelToMM(Math.sqrt(Math.pow((a.x-b.x),2)+Math.pow((a.y-b.y),2)));
	}
	
	/**
	 * Finds the angle between 2 points and the x-axis.
	 * @param a first 2d point
	 * @param b second 2d point
	 * @return angle from a->b, in radians.
	 */
	public static double angleInRadians(Point2D.Float a, Point2D.Float b){
		return Math.atan2(b.y - a.y, b.x - a.x );
	}
	
	public static double angleInRadians(Tile a, Tile b){
		return Math.atan2(b.y - a.y, b.x - a.x );
	}
	
	/**
	 * Same as angleInRadians(), but in degrees.
	 * @param a First 2d point
	 * @param b Second 2d point
	 * @return angle.
	 */
	public static double angleInDegrees(Point2D.Float a, Point2D.Float b){
		return Math.toDegrees(Math.atan2(b.y-a.y, b.x-a.x));
	}

	
	
	/**
	 * Compute the angle to turn in order to arrive at a certain point
	 * @param a first point
	 * @param b second point
	 * @param currentAngle The robot's current angle to the x-axis.
	 * @return The angle in degrees.
	 */
	public static double angleToTurn(Point2D.Float a, Point2D.Float b, double currentAngle){
		// System.err.println("Getting angle to turn: ");
		double angleToTurn = angleInRadians(a,b)- currentAngle; 
		if (angleToTurn < -Math.PI) {
			angleToTurn = 2*Math.PI + angleToTurn;
		}
		if (angleToTurn > Math.PI) {
			angleToTurn = angleToTurn - 2*Math.PI;
		}
		return Math.toDegrees(angleToTurn);
	}

	
	/**
	 * Transform distances from pixels to mm.
	 * @param distanceInPixels The pixel distance
	 * @return the mm distance.
	 */
	public static double PixelToMM(double distanceInPixels){
		return distanceInPixels*(2438.4/640);
	}
}
