package scripts;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.ScriptEngine;
import strategy.world.Tile;
import commander.ControllerGUI;

import plan.navigation.*;
import strategy.world.*;
import java.util.Random;

public class StartPositionShoot extends AccessorMethods implements Script {
    static int randomInt = -1;
    @Override
    public Tile getGoalScoringTile() {
        double goalX = 0;
        double goalY = 61;       	
        double robotLength = 21;
        Random randomGenerator = new Random();
        if (randomInt< 0)
            randomInt = randomGenerator.nextInt(2);
        double dirX, dirY, newX, newY;
	    if (WorldState.shootLeft) {
	        print("Shooting Left");
	        goalX = 85;
	    }
	    else {
	        print("Shooting Right");
	        goalX = 158;
	    }
	    if (randomInt % 2 == 0)
	        goalY = 122;
	    else goalY = 0;
	    //get the direction 
	    dirX = ball.getLocation().x - goalX;
	    dirY = ball.getLocation().y - goalY;
	    //normalize the direction to a unit vector
	    double len = Math.sqrt(Math.pow((dirX), 2)+ Math.pow((dirY),2));
	    
	    dirX /= len;
	    dirY /= len;
	    
	    //add the robotLength as a magnitude
	    
        dirX *= robotLength;
	    dirY *= robotLength;
	    
	    //add the position of the ball to it
	    
	    newX = dirX + ball.getLocation().x;
	    newY = dirY + ball.getLocation().y;
	    //print("ballX is: " + ball.getLocation().x);
	    //print("ballY is: " + ball.getLocation().y);
	   // print("newX is: " + newX);
	    //print("newY is: " + newY);
	    
	    return new Tile(newX, newY);
	
	}
    public int run(String param) {
    	long time = System.currentTimeMillis(); 
        NavigateToPoint navTile = new NavigateToPoint();
        Tile myRobotLocation = myRobot.getLocation(); //Gets our x y coords as a float
		Tile oppRobotLocation = oppRobot.getLocation();
		Tile ballLocation = ball.getLocation();
	    Tile goalLocation = getGoalScoringTile();	
		
        print("Taken : "+(System.currentTimeMillis() - time));
        //these conditions should be changed and the end of this script should be different
        if (myRobotLocation.distanceTo(goalLocation) < 3) {
            /*
            if (shootLeft) 
                faceTile(new Tile(0, 61));
            else 
                faceTile(new Tile(244, 61));
            */
            faceBall();
            int speedMax = bot.getSpeedMax();
	        bot.setSpeed(speedMax / 5);
	        bot.go();
	        wait(1000);
	        bot.setSpeed(speedMax);
	        wait(400);
	        kick();
	        stop();
		    return -1;
        }
        int refRate = navTile.run(goalLocation);
		
        return refRate;
    }


}
