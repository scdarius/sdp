/**
 * Provides access to all algorithms, methods for scripts to implement AI and planning strategies. 
 * At the moment the methods are taken from a multitude of classes, so need work on simplifying/refactoring
 * stuff.
 * @warning While loops and any other type of looping will effectively pause the script whilst the loop commences
 * aka, script/whatever the strategy chooser will be cant rechoose strategy whilst the loop happens
 * so if something changes and we thus need to change strategy, loop would have to finish.
 * So try to stay away from using while/for loops (when being used in Scripts).
 * @note Try to depend on boolean methods that thus give feedback, rather than void/methods that give us no information about
 * if we suceeded.
 *
 */

package strategy;

import commander.ControllerGUI;

import server.Bot;
import strategy.world.*;

import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.lang.Math;

public abstract class AccessorMethods extends WorldState {

	public static Bot bot = ControllerGUI.bot;
	public static Tile[] lArea = new Tile[2];    
	

	static int cornerDistance = 22; // distance from the corner
    public GameArea topLeftArea = new GameArea(new Tile(0, 0), new Tile(cornerDistance, cornerDistance));
	public GameArea topRightArea = new GameArea(new Tile(244, 0), new Tile(244 - cornerDistance, cornerDistance));
	public GameArea bottomLeftArea = new GameArea(new Tile(0, 122), new Tile(cornerDistance, 122 - cornerDistance));
    public GameArea bottomRightArea = new GameArea(new Tile(244, 122), new Tile(244 - cornerDistance, 122 - cornerDistance));

    public GameArea wholePitch = new GameArea(new Tile(0, 0), new Tile(244, 122));
    // take 10cm of the pitch.
    public GameArea gamePlayArea = new GameArea(new Tile(10, 10), new Tile(234, 110));
    



	// ------ basic methods for scripts
	public void kick() {
		bot.kick();
	}

	public void stop() {
		bot.stop();
	}

	public void moveForward() {
		bot.go();
	}

	/**
	 * Moves forward for a given amount of time.
	 * @param ms of time to move forward.
	 */
	public void moveForward(int ms) {
		moveForward();
		wait(ms);
		stop();
	}
	
	public void travel(int dist) {
		System.out.println("Into travel method, travelling requisite distance " + dist);
		bot.travel(dist);
	}


	public void moveBackward(){
		bot.reverse();
	}

	public void moveBackward(int ms){
		moveBackward();
		wait(ms);
		stop();
	}

	public void moveArc(int radius, int angle) {
		bot.arc(radius, angle);
	}
	
	public boolean moveTo(Tile t) {
		double distance = myRobot.getLocation().distanceTo(t);
		if(distance < 5) {
			bot.stop();
			return true;
		}
		if(faceTile(t)) {
	        moveForward();
	        return true;
		}
		return false;
	}
	
	public void goToTile(Tile t) {
		System.out.println("Trying to face tile..");
		faceTile(t);
		System.out.println("Has faced tile, is now trying to get distance");
		double distance = myRobot.getLocation().distanceTo(t);
		System.out.println("Is now attempting to travel the requisite distance");
		travel((int)distance);
	}
	
	public int walkPath(Tile[] path) {
		Tile endPoint = path[path.length - 1];
		
		if(myRobot.distanceTo(endPoint) > 70)
			bot.setSpeed(bot.getSpeedMax());
		else {
			bot.setSpeed(bot.getSpeedMax() / 3);
		}
		
		if(path.length == 1) {
			moveTo(path[0]);
			return 250;
		}
		Tile current = path[0];
		Tile next = path[1];
		if(myRobot.distanceTo(current) < 25) {
			moveTo(next);
			return 500;
		} else {
			moveTo(current);
			return 250;
		}
		
	}

	/**
	 * Pauses system.
	 * @param ms to wait
	 */
	public void wait(int ms) {
		try {
			Thread.sleep(ms);
		} catch(Exception e) {}
	}

	/**
	 * Prints a message to the terminal.
	 * @param s String to print.
	 */
	public void print(String s) {
		ControllerGUI.terminal.print(">> " + s);
	}

	// ------ Methods to deal with the ball.

	/**
	 * Returns if a robot is close to the ball, does not determine if we are facing it.
	 * @param r robot that we wish to check.
	 * @return whether the given robot is close to the ball.
	 */
	public boolean closeToBall(Robot r) {
		Tile robotLocation = r.getLocation();
		Tile ballLocation = ball.getLocation(); 
		double distance = robotLocation.distanceTo(ballLocation);
		// At least for the second pitch, 100 seems to be ok
		return distance < 13; 
	}

	public boolean closeToBall(Robot r, int threshold) {
		Tile robotLocation = r.getLocation();
		Tile ballLocation = ball.getLocation(); 
		double distance = robotLocation.distanceTo(ballLocation);
		// At least for the second pitch, 100 seems to be ok
		return distance < threshold; 
	}

	public boolean hasBall(Robot r) {
		return closeToBall(r) && isFacingBall(r);
	}

	/**
	 * @param r robot that we wish to check.
	 * @return whether a robot can kick (the robot is close and facing the ball)
	 */
	public boolean canKick(Robot r) {
		double distance = r.distanceTo(ball);
		return (distance < 13) && isFacingBall(r); 
	}

	// Remove this?
	public boolean canKick(Robot r, int threshold) {
		double distance = r.distanceTo(ball);
		return (distance < threshold) && isFacingBall(r); 
	}

	/**
	 * @param r Robot that we wish to check.
	 * @param g Goal we want to see if it can shoot in (will be either 'myGoal' or 'oppGoal'
	 * TODO: It only currently see's if we are facing the goal center and can thus shoot.
	 * It needs to take into account how big the goal is (so we can shoot in 
	 * the corners)
	 * @return  whether given robot can kick the ball and is facing the goal.
	 */
	public boolean canShoot(Robot r, Goal g) {
		
		
		if(canKick(r) && isFacingTile(g.getLocation(), r))
			return true;
		return false;
	}
	

	public Robot getClosestRobot(GameObject go) {
		double myDistance = myRobot.distanceTo(go); 
		double theirDistance = oppRobot.distanceTo(go);

		if(myDistance < theirDistance)
			return myRobot;

		return oppRobot;
	}

	/**
	 * Try to face ball
	 * @return success Are we facing the ball now?
	 */
	public boolean faceBall() {
		if(!isFacingBall(myRobot))
			return faceTile(ball.getLocation());

		return true;
	}

	// ------- Movement methods
	/**
	 * Given an object, returns true if it is moving
	 */
	public boolean isMoving(GameObject go) {

		//Case when it is our robot, so we do not have to check vision
		if(go == myRobot){
			return bot.isMoving();
		}

		else{
			Tile initialLocation = go.getLocation();
			wait(50);
			Tile newLocation = go.getLocation();
			double distance = initialLocation.distanceTo(newLocation);
			// if there is distance between now and half a second later, object has moved.
			return distance > 1;
		}

	}

	/**
	 * Rotates our robot to face a given tile.
	 * @param t to change direction to face.
	 * @return whether we suceeded.
	 */
	public boolean faceTile(Tile t) {
		if(!isFacingTile(t, myRobot)) {
			double newBearing = myRobot.getLocation().bearingTo(t);
			double angle = (((360 - myRobot.getAngle()) + newBearing) % 360);
			if (angle>180)
				angle -= 360;
			System.out.println("Amount i need to turn is: " + angle);
			bot.rotate((int) angle);
		}
		return isFacingTile(t, myRobot);
	}
	
	/**
	 * Rotates our robot to face a given tile.
	 * @param t to change direction to face.
	 * @angle threshold
	 * @return whether we suceeded.
	 */
	public boolean faceTile(Tile t, int threshold) {
		if(!isFacingTile(t, myRobot, threshold)) {
			double newBearing = myRobot.getLocation().bearingTo(t);
			double angle = (((360 - myRobot.getAngle()) + newBearing) % 360);
			if (angle>180)
				angle -= 360;
			System.out.println("Amount i need to turn is: " + angle);
			bot.rotate((int) angle);
		}
		return isFacingTile(t, myRobot, threshold);
	}

	/**
	 * Returns if robot is facing the ball.
	 * @param r Robot we wish to check if it's facing the ball.
	 */
	public boolean isFacingBall(Robot r) {
		return isFacingTile(ball.getLocation(), r);
	}

	/**
	 * Sees if a robot is facing a given tile.
	 * @param t tile to check.
	 * @param r Robot that we want to check.
	 */
	public boolean isFacingTile(Tile t, Robot r) {
		double newBearing = r.getLocation().bearingTo(t);
		double angle = (((360 - r.getAngle()) + newBearing) % 360);
		if (angle>180)
			angle -= 360;
		if (Math.abs(angle) < 10 )
			return true;
		return false;
	}

	/**
	 * @param t Tile to check.
	 * @param r Robot to
	 * @param threshold the threshold of the angle. When we are at a greater distance, this should ideally be higher.
	 * @return if we are facing a tile within a given angle threshold.
	 */
	public boolean isFacingTile(Tile t, Robot r, int threshold) {
		double newBearing = r.getLocation().bearingTo(t);
		double angle = (((360 - r.getAngle()) + newBearing) % 360);
		if (angle>180)
			angle -= 360;
		if (Math.abs(angle) < threshold)
			return true;
		return false;
	}
	
	/**
     * Calculates where we need to be to shoot.
     * @return tile we need to be at.
     */
	public Tile getGoalScoringTile() {
        double goalX = 0;
        double goalY = 61;       	
        double robotLength = 21;
        
        double dirX, dirY, newX, newY;
	    if (WorldState.shootLeft) {
	        //print("Shooting Left");
	        goalX = 0;
	    }
	    else {
	        //print("Shooting Right");
	        goalX = 244;
	    }
	    
	    if(getBestTileToShootAt() != null){
	    
	    
	    	goalX = getBestTileToShootAt().getX();
	    	goalY = getBestTileToShootAt().getY();
	    }
	    
	    
	    
	    //get the direction 
	    dirX = ball.getLocation().x - goalX;
	    dirY = ball.getLocation().y - goalY;
	    //normalize the direction to a unit vector
	    double len = Math.sqrt(Math.pow((dirX), 2)+ Math.pow((dirY),2));
	    
	    dirX /= len;
	    dirY /= len;
	    
	    //add the robotLength as a magnitude
	    
        dirX *= robotLength;
	    dirY *= robotLength;
	    
	    //add the position of the ball to it
	    
	    newX = dirX + ball.getLocation().x;
	    newY = dirY + ball.getLocation().y;
	    //print("ballX is: " + ball.getLocation().x);
	    //print("ballY is: " + ball.getLocation().y);
	   // print("newX is: " + newX);
	    //print("newY is: " + newY);
	    
	    return new Tile(newX, newY);
	
	}
	
	/**
	*@return: Tile[0]
	*/
	/*public Tile[] getShootingData(){
	
		return new Tile (0,0);
	
	}*/

	public Tile getOnsideGoalScoringTile() {
		
			int inaccessibleY = 12;
			Tile preferedLocation = getGoalScoringTile();
			Tile ballLocation = ball.getLocation();
			if(ballLocation.getY() < inaccessibleY ){
				if(shootLeft){
					if (ballLocation.getX() > 222){
						System.out.println("in top right corner!");
						preferedLocation = new Tile(232,32);
						return preferedLocation;
					}else{
						
						System.out.println(" to close to top wall!");
						preferedLocation = new Tile((ballLocation.getX()+5),(ballLocation.getY()+5));
						return preferedLocation;
					}
				}else{
					if (ballLocation.getX() < 22){
						System.out.println("in top left corner!");
						preferedLocation = new Tile(12,32);
						return preferedLocation;
					}else{
						System.out.println(" to close to top wall!");
						preferedLocation = new Tile((ballLocation.getX()-5),(ballLocation.getY()+5));
						return preferedLocation;
					}

				}
			}
			if (ballLocation.getY() > 122-inaccessibleY){
				if(shootLeft){
					if (ballLocation.getX() > 222){
						System.out.println("in bottom right corner!");
						preferedLocation = new Tile(232,90);
						return preferedLocation;
					}else{
						System.out.println(" to close to bottom wall!");
						preferedLocation = new Tile((ballLocation.getX()+5),ballLocation.getY()-5);
						return preferedLocation;
					}
				}else{
					if (ballLocation.getX() < 22){
						System.out.println("in bottom left corner!");
						preferedLocation = new Tile(12,90);
						return preferedLocation;
					}else{
						System.out.println(" to close to bottom wall!");
						preferedLocation = new Tile((ballLocation.getX()-5),ballLocation.getY()-5);
						return preferedLocation;
					}
				}
			}
		return preferedLocation;
		
	  

		
	}

    /**
     * Calculates where we need to be to shoot.
     * @return tile we need to be at.
     */
	public Tile getGoalScoringTile2() {
		double goalScoringBearing = Math.toRadians(ball.getLocation().bearingTo(oppGoal.getLocation()));
		double robotLength = 42;
		double newX, newY;
        if (WorldState.shootLeft) {
            print("Shooting Left");
            newX = ball.getLocation().x - (Math.cos(goalScoringBearing) * (robotLength/2));
		    newY = ball.getLocation().y - (Math.sin(goalScoringBearing) * (robotLength/2));
        } else  {
            print("Shooting Right");
            newX = ball.getLocation().x - (Math.cos(goalScoringBearing) * (robotLength/2));
            if (ball.getLocation().y < 61) {
                print("Upper");
    		    newY = ball.getLocation().y - (Math.sin(goalScoringBearing) * (robotLength/2));
            } else {
                print("Lower");
    		    newY = ball.getLocation().y - (Math.sin(goalScoringBearing) * (robotLength/2));
            }
        }
		return new Tile(newX, newY);
	}

    public double  getGoalScoringBearing() {
        return ball.getLocation().bearingTo(oppGoal.getLocation());
    }

    public boolean inPitchCorner(GameObject go) {
        Tile location = go.getLocation();
		System.out.println(location.toString());
        if(topLeftArea.inArea(location) || topRightArea.inArea(location) || bottomLeftArea.inArea(location)
                || bottomRightArea.inArea(location)){
			System.out.println("in corner");	
			return true;
		}
		return false;
    }
    
    /**
     * Is it our corner or theirs?
     * 'our' pitch corner is defined by the corners we start from.
     * @param go
     * @return whether it is in our corner or the opponents
     */
    public boolean inOurPitchCorner(GameObject go) {
    	Tile location = go.getLocation();
    	if(!shootLeft)
    		return (topRightArea.inArea(location) || bottomRightArea.inArea(location));
    	else 
    		return (topLeftArea.inArea(location) || bottomLeftArea.inArea(location));
    		
    }
    
    public GameArea getPitchCorner(GameObject go) {
        Tile location = go.getLocation();
        
        if(topLeftArea.inArea(location))
        	return topLeftArea;
        if(topRightArea.inArea(location))
        	return topRightArea;
        if(bottomLeftArea.inArea(location))
        	return bottomLeftArea;
        if(topRightArea.inArea(location))
        	return topRightArea;
        
        return null;
    }
    

	public String worldToStringAcc() {
                String s = ball.getLocation() + "\nOur robot: " + myRobot.getLocation() + "\nOpponent robot: "
                                   + oppRobot.getLocation() + "\n Our goal: " + myGoal.getLocation() + "\nOpponent goal: "
                                   + oppGoal.getLocation() + "\n GoalScoringTile: " + getGoalScoringTile();
                return s;
        }

    /**
     * Calculates the optimal speed we should use for movement haivng in mind how far away we are from our goal.
       
     * @return the optimal movement speed.
     */
	public int getOptimalSpeed(Tile goal) {
		double distance = myRobot.getLocation().distanceTo(goal);
			distance -= 80;
			int speed = (int)((distance / Math.sqrt(800 + Math.pow(distance, 2)) + 1) * 30 + 20) * bot.getSpeedMax() / 100;
			print("Speed test " + speed);
			return speed;
	}

    /**
     * Get's a line and tells us if it's obstructed. I.e a shooting line and if
     * the opp robot is in our way.
     * @param toShoot tile we want to shoot at.
     * @return if opponent is in the way.
     */
    public boolean isShootingObstructed(Tile toShoot) {
        Tile ballLocation = ball.getLocation();
		Line2D.Double line = new Line2D.Double(ballLocation, toShoot);
		if(line.intersects(oppRobot.getShape())) {
			return true;
		}
        return false;
    }
    
    /**
     * Gets a shooting point on the goal line.
     * Takes both robots and goals as this may see where they will shoot.
     * @return
     */
    public Tile getShootingPoint() {
    	Tile[] shootingTiles = oppGoal.getGoalLine();

    	if(!isShootingObstructed(oppGoal.loc))
    		return oppGoal.loc;
    	
    	for(Tile t : shootingTiles) {
    		if(!isShootingObstructed(t))
    			return t;
    	}
    	return null;	   	
    }

	/**
	* Gets the shooting tile that is closest to us taking direction into account.	
	*/
	public Tile getClosestShootingPoint() {
		Tile[] shootingTiles = oppGoal.getGoalLine();
		ArrayList<Tile> potentialTiles = new ArrayList<Tile>();
		for(Tile t : shootingTiles) {
			if(!isShootingObstructed(t)) // add unobstructed tiles to arraylist
				potentialTiles.add(t);		
		}

		if(potentialTiles.size() == 0) 
			return null; // all tiles are obstructed
		
		Tile closest = potentialTiles.get(0);
		double currentBearing = myRobot.bearingTo(closest);
		for(Tile t : potentialTiles) {
			double bearing = myRobot.bearingTo(t);
			if(bearing < currentBearing && (bearing < 90)) {
				closest = t;
				currentBearing = bearing;			
			}
		}
		return closest;
	
	}
	
	/**
	*Returns the best tile to shoot at 
	* This is done by finding the biggest gap in the goal and returning the tile in the middle of the gap 
	*if there is no possible place to shoot returns null
	*/
	public Tile getBestTileToShootAt(){
		Tile[] shootingTiles = oppGoal.getGoalLine();
		ArrayList<Tile> potentialTiles = new ArrayList<Tile>();
		for(Tile t : shootingTiles) {
			if(!isShootingObstructed(t)) // add unobstructed tiles to arraylist
				potentialTiles.add(t);		
		}
		
		int size = potentialTiles.size();
		if(size == 0) 
			return null; 
		
		
		double x = (potentialTiles.get((int) Math.ceil(size/2)).getX() + potentialTiles.get((int)Math.floor(size/2)).getX())/2;
		double y = (potentialTiles.get((int) Math.ceil(size/2)).getY() + potentialTiles.get((int)Math.floor(size/2)).getY())/2;
	   	
	   	
	   	if (shootingTiles.length == potentialTiles.size()){
	   	
	   		return new Tile (x,y);
	   	}
		
										
		if(oppRobot.getLocation().distanceTo(shootingTiles[0]) >= oppRobot.getLocation().distanceTo(shootingTiles[shootingTiles.length-1])){
			
			double x2 = (shootingTiles[0].getX() + x)/2;
			double y2 = (shootingTiles[0].getY() + y)/2;
			Tile t1 = new Tile (x2,y2);
		
	
			return t1;
		
		}
			
		double x2 = (shootingTiles[shootingTiles.length-1].getX() + x)/2;
		double y2 = (shootingTiles[shootingTiles.length-1].getY() + y)/2;
		
		Tile t1 = new Tile (x2,y2);
		
		return t1;
			
	
	
	}
	
	
		
	/*
	*returns true if our robot can get into a position to shoot without hitting a wall
	*Need to check if the rectangle is drawn correctly
	*/
	
	public boolean canGetInPositionToShot(){
			
			Tile t = getGoalScoringTile();
			
			Line2D.Double bottomWall = new Line2D.Double(bottomLeftCorner, bottomRightCorner); 
			Line2D.Double topWall = new Line2D.Double(topLeftCorner, topRightCorner); 
			Line2D.Double leftWall = new Line2D.Double(topLeftCorner, bottomLeftCorner); 
			Line2D.Double rightWall = new Line2D.Double(topRightCorner, bottomRightCorner); 
			
			
			Rectangle2D.Double imaginaryRobot = new Rectangle2D.Double(t.x - 8, t.y - 8, 18, 18);
			

			
			if (bottomWall.intersects(imaginaryRobot) || topWall.intersects(imaginaryRobot) || leftWall.intersects(imaginaryRobot) ||		rightWall.intersects(imaginaryRobot) || !(wholePitch.inArea(t))){
				return false;
			}
			
			return true;
		
	
	
	}


    // todo (need to be in labs to do this though)
    public boolean ballInGoal() {
        
		if (shootLeft) return (ball.getLocation().x > myGoal.getLocation().x || ball.getLocation().x < oppGoal.getLocation().x);
		else return (ball.getLocation().x > myGoal.getLocation().x || ball.getLocation().x < oppGoal.getLocation().x);
		/*
        if (shootLeft == true){
        	if(ball.getLocation().x > myGoal.getLocation().x || ball.getLocation().x < oppGoal.getLocation().x){
        		return true;
        	}
        }
        if (shootLeft == false){
        	if (ball.getLocation().x < myGoal.getLocation().x || ball.getLocation().x > oppGoal.getLocation().x){
        		return true;
        	}
        }
            
        return false;
		*/
    }

    public boolean outOfBounds(GameObject go) {
        if(wholePitch.inArea(go.getLocation()))
            return false;
        return true;
    }

    /**
     * TODO: Handle this as a shape and not getting the objects location point (since the point represents the center
     * of and object and the center of a robot probs wont get to 10cm of the wall).
     * @param go
     * @return
     */
    public boolean nextToWall(GameObject go) {
        return (!outOfBounds(go) && gamePlayArea.inArea(go.getLocation()));
    }




    public void steer(int turnRatio , int radius){
    	bot.steer(turnRatio, radius);
	}
	/**
	 * Returns an array consisting of two tiles on the top and bottom walls (i.e y=0 and 244, respectively).
	 * Shooting at these tiles will result in a rebound towards the center tile off the opponent's goal.
	 * TODO: This method assumes an exact reflection (i.e angles of inflection and reflection are equal)
	 * 		 This could be improved by using a "bounce coefficient", which could changing on both which direction
	 *		 we are shooting and the wall which the tile is on (which side of the pitch we are bouncing off of).
	 * TODO: This method will only aim at the center of the goal. It should check the balls path is clear
	 * 		 and if not should use other points on the goal. Alternatively, it could return the tiles with
	 *		 the closest shooting position
	 * @return An array of two tiles to aim at for shots bouncing off the walls 
	 */
	public Tile[] getBounceTiles() {
		Tile[] bounceTiles = new Tile[2];
		Tile ball = WorldState.ball.getLocation();
		Tile goalCenter = WorldState.oppGoal.getLocation();

		//Project the goal center's y coordinate up and down by the height of the pitch	
		//This is the point which we have to aim at.
		Tile goalUp = new Tile(goalCenter.x, (goalCenter.y - 122.0));
		Tile goalDown = new Tile(goalCenter.x, (goalCenter.y + 122.0));
		
		double bounceX, bounceY, slope;

		//Bouncing off top wall of pitch
		bounceY = 0.0;
		slope = ((ball.y - goalUp.y) / (ball.x - goalUp.x));
		bounceX = (bounceY - ball.y) / slope + ball.x;
		bounceTiles[0] = new Tile(bounceX, bounceY);

		//Bouncing off bottom wall of pitch
		bounceY = 122.0;
		slope = ((ball.y - goalDown.y) / (ball.x - goalDown.x));
		bounceX = (bounceY - ball.y) / slope + ball.x;
		bounceTiles[1] = new Tile(bounceX, bounceY);

		return bounceTiles;
	}
    	

}

