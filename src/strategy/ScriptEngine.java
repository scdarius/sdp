package strategy;

import commander.ControllerGUI;

public class ScriptEngine implements Runnable {
        Script script;
        // Script parameter.
        String param;
        public static boolean runScript = false;
        public static boolean executionFinished = false;

        public ScriptEngine(Script script, String param) {
                this.script = script;
                this.param = param;
        }

        public void run() {		
                executionFinished = false;
                while(runScript) {
                        int wait = script.run(param);
                        if(wait == -1) { 
                                runScript = false;
                                ControllerGUI.terminal.print("Script stopped");
                                ControllerGUI.bot.stop();
                                break;
                        }

                        try {
                                Thread.sleep(wait);
                        }catch(Exception e) {
                                e.printStackTrace();
                        }
                } 
                executionFinished = true;
        }
}
