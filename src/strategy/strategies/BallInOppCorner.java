package strategy.strategies;

import java.awt.geom.Line2D;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.agents.Strategy;
import strategy.world.WorldState;
import strategy.world.Tile;

import commander.ControllerGUI;

public class BallInOppCorner extends AccessorMethods implements Script, Strategy {


	public double getUtility() {
        if(takePenalty || defendPenalty)
            return 0;
        Tile ball = WorldState.ball.getLocation();
	    if ( Math.abs(ball.x - WorldState.oppGoal.getLocation().x) <= 22 && (ball.y < 30 || ball.y > 92))
            return 1;
        return 0;
	}

	public int run(String s) {
        Tile ball = WorldState.ball.getLocation();
        double targetX = WorldState.oppGoal.getLocation().x - ((WorldState.oppGoal.getLocation().x - WorldState.myGoal.getLocation().x) / 20);
        if (targetX > 230) targetX = 230;
        if (targetX < 14) targetX = 14;        

        Tile target = new Tile(targetX, ball.y);

        ControllerGUI.clear();
        ControllerGUI.setPoint(target);

        // TODO: Move to target tile
        
        // Turn to face oppGoal wall
        // faceTile(WorldState.oppGoal);
        return 100;
	}

}
