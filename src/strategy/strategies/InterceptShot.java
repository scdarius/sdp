package strategy.strategies;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.agents.Strategy;
import strategy.world.Tile;

public class InterceptShot extends AccessorMethods implements Script, Strategy {
	
	
	
	public boolean isAttacking(){
		
		return false;
	
	}
	
	
	//Returns a higher utility if they are facing our goal aswell. 
	public double getUtility() {
        if(takePenalty || defendPenalty)
            return 0;
		if(hasBall(oppRobot) && isMoving(oppRobot)){
			if(isFacingTile(myGoal.getLocation(), oppRobot)){ 
				return 2; 
			}
			
			return 1;
		}
		return 0;
		
	}
	
	public int run(String param) {
		
		
		return 0;
	}

}
