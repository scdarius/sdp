package strategy.strategies;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.agents.Strategy;
import strategy.world.Tile;
import strategy.world.WorldState;
import strategy.world.Tile;

import commander.ControllerGUI;

public class BallInOurCorner extends AccessorMethods implements Script, Strategy {

	
	public double getUtility() {
        if(takePenalty || defendPenalty)
            return 0;
        Tile ball = WorldState.ball.getLocation();
	    if ( Math.abs(ball.x - WorldState.myGoal.getLocation().x) <= 22 && (ball.y < 30 || ball.y > 92))
            return 1;
        return 0;
	}

	
	public int run(String s) {
        Tile ball = WorldState.ball.getLocation();
        double targetX = WorldState.myGoal.getLocation().x - ((WorldState.myGoal.getLocation().x - WorldState.oppGoal.getLocation().x) / 20);
        double targetY;
        if (targetX > 230) targetX = 230;
        if (targetX < 14) targetX = 14;

        if (ball.y < 61.0) targetY = 30;
        else targetY = 92;  

        Tile target = new Tile(targetX, targetY);

        ControllerGUI.clear();
        ControllerGUI.setPoint(target);

        // TODO: Move to target tile
        
        // Turn to face oppGoal wall
        // faceTile(new Tile (WorldState.myGoal.getLocation().x, targetY));
        return 100;
	}

}
