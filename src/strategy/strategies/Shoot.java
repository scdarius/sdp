package strategy.strategies;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.agents.Strategy;
import strategy.world.*;

/**
 *Simply handles the actual shooting - not the methods of facing the goal/goal scoring location.
 */
public class Shoot extends AccessorMethods implements Script, Strategy {	

	public boolean isAttacking() {
	
		return true;
	
	}
	

	public double getUtility() {
        	if(takePenalty || defendPenalty)
           		return 0;

		if(canKick(myRobot) && isFacingGoalDirection() && canShoot())
			return 1;

		return 0;		
	}
	
	
	public int run(String param) {
		return shoot();
	}

    /**
     * If our orientation intersects the goal line.
     * @return if we are in a position to shoot.
     */
	boolean canShoot() {
		Tile[] bouncingTiles = getBounceTiles();
		for(Tile t : bouncingTiles) {
			if(!isShootingObstructed(t) && isFacingTile(t, myRobot))
				return true;
		}

		Vector v = new Vector(myRobot.getLocation(), myRobot.getAngle()); // check this later

		Vector shootingGoal = new Vector(oppGoal.top, oppGoal.bottom);
		Tile t = v.getIntersectionPt(shootingGoal);
		if(t != null && !isShootingObstructed(t)) {
			if(t.y < 90 && t.x > 31) // the intersection occurs in the goal region
				return true;
		}
		return false;		
	}
	
	/**
	 * Returns if we are in a general goal facing direction (the angle to the goal is < 90 and so can either bounce shoot or
	 * shoot some other way.)
	 * Needs testing to make sure robot orientations are correct.
	 */
	boolean isFacingGoalDirection() {
		if(shootLeft) {
			return (myRobot.getAngle() > 90 && myRobot.getAngle() < 270);
		} else {
			return (myRobot.getAngle() < 90 || myRobot.getAngle() > 270);
		}
	}

	/**
	* Need to think of a way of not using 'wait' so we don't blast off, miss the ball only to realise a few seconds later...
	*/
	public int shoot() { 
        	int speedMax = bot.getSpeedMax();
	        bot.setSpeed(speedMax / 2);
	        bot.go();
	        wait(500);
	        bot.setSpeed(speedMax);
	        wait(300);
		kick();
		return 150;
	}



}
