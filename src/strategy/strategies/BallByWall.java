package strategy.strategies;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.agents.Strategy;
import strategy.world.WorldState;
import strategy.world.Tile;

import commander.ControllerGUI;

/**
 * Created by IntelliJ IDEA.
 * User: Ashley
 * Date: 27/03/12
 * Time: 10:41
 */
public class BallByWall extends AccessorMethods implements Script, Strategy {



	public double getUtility() {
        if(takePenalty || defendPenalty)
            return 0;
        Tile ball = WorldState.ball.getLocation();
	    if ((ball.y < 22 || ball.y > 100) && (ball.x > 22 || ball.x < 220))
            return 1;
        return 0;
    }

    @Override
    public int run(String param) {
        Tile ball = WorldState.ball.getLocation();
        //double targetX = ball.x - ((WorldState.oppGoal.getLocation().x - WorldState.myGoal.getLocation().x) / 20);
        double targetY = ball.y;
        if (targetY > 108) targetY = 108;
        if (targetY < 14) targetY = 14;
        Tile target = new Tile(ball.x, targetY);

        ControllerGUI.clear();
        ControllerGUI.setPoint(target);

        

        // TODO: Move to target tile
        
        // Turn to face oppGoal wall
        //faceTile(new Tile( WorldState.oppGoal.getLocation().x, target.y);
        return 100;
    }
}
