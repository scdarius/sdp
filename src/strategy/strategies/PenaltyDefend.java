package strategy.strategies;

import strategy.AccessorMethods;
import strategy.Script;
import strategy.agents.Strategy;
import strategy.world.Tile;
import java.util.Random;

public class PenaltyDefend extends AccessorMethods implements Script, Strategy {
        Random rand = new Random();
        
        boolean rotated = false;
        Tile ballPositionOrig;
        
        public PenaltyDefend()
        {
            ballPositionOrig = new Tile(ball.loc.x, ball.loc.y);
        }
        
        public int randomInt(int start, int end)
        {
                return rand.nextInt(end - start) + start;
        }
        
        public boolean ballMoved() {
            if (ballPositionOrig.distanceTo(ball.loc) > 4)
                return true;
            return false;
        }

        @Override
        public double getUtility() {
            if(defendPenalty)
                return 1;
            return 0;
         }
        
	    public int run(String param) {
	        /*if (ballMoved())
	        {
	            return -1;
	        }*/
	        print("hello");
            //bot.setSpeed(bot.getSpeedMax());
            //bot.setTurnSpeed(bot.getTurnSpeedMax());
            int distanceRange = 15; // from 0 to 120
            int position = distanceRange / 2;
            //int movementTimes = randomInt(3, 10);
            print("running");
            System.out.println("oppRobot angle: " + oppRobot.angle);
            double robotDistance = Math.abs(myRobot.loc.x - oppRobot.loc.x);
            System.out.println("robot distance: " + robotDistance);
            double angle = oppRobot.angle;
            if (oppRobot.angle < -90) angle = (-180)-angle;
            else if (oppRobot.angle > 90) angle = (180)-angle;
            int destination = (int)Math.round(robotDistance * Math.tan(Math.toRadians(angle)) + oppRobot.loc.y);
            System.out.println("current location: " + (int)Math.round(myRobot.loc.y));
            System.out.println("destination: " + destination);
            int travelArg = destination - (int)Math.round(myRobot.loc.y);
            System.out.println("travel: " + travelArg);
            if (20 <= destination && destination <= 100) {
                bot.travel((int)Math.round(travelArg * 4 / 5));
                System.out.println("NEW LOCATION: " + (int)Math.round(myRobot.loc.y));
            }
		    return 50;
	}


}
