package strategy.strategies;

import strategy.AccessorMethods;
import strategy.Movement;
import strategy.Script;
import strategy.agents.Strategy;
import strategy.world.Tile;

public class NavigateToGoalScoringTile extends AccessorMethods implements Script, Strategy {
	
	
	
	public double getUtility() {
        if(takePenalty || defendPenalty)
            return 0;
		if(!inOurPitchCorner(ball) && myRobot.distanceTo(getGoalScoringTile()) > 10)
			return 1;
		return 0;
	}

	
	public int run(String s) {		
		Tile[] path = Movement.getPath(getGoalScoringTile());
		return walkPath(path);
	}

}
