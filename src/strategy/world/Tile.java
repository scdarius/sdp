package strategy.world;
import java.awt.geom.Point2D;

public class Tile extends Point2D.Double {
	public double x, y;

	public Tile(){};

	public Tile(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	/*
	 * @param Tile to calculate distance too.
	 * @TODO - Needs to deal with measurements once we know what the co-ordinate system will be.
	 */
	public double distanceTo(Tile to) {
		return Math.sqrt(Math.pow((to.x - x), 2)+ Math.pow((to.y - y),2));
	}
	
	public String toString() {
		return x + ", " + y;
	}

    public double bearingTo(Tile to) {
		double xDifference =  to.x - x;
		double yDifference =  to.y - y;
		double bearing = Math.toDegrees(Math.atan2(yDifference,xDifference));
		if (bearing < 0)
			bearing += 360;
        if (bearing > 360)
            bearing -= 360;
		return bearing;
    }
    
    public Tile addVector(double addX, double addY) {
    	double newX = x + addX;
    	double newY = y + addY;
    	return new Tile(newX, newY);
    }

    public boolean isOccupied() {
        if(WorldState.myRobot.getArea().inArea(this) || WorldState.oppRobot.getArea().inArea(this) || WorldState.ball.getArea().inArea(this))
            return true;

        return false;
    }

}
