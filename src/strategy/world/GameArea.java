package strategy.world;

import commander.ControllerGUI;
import strategy.AccessorMethods;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * TODO: Define an area bu 4 lines.
 * Define areas of the pitch. Corners, etc.
 * Helps with Geometry/seeing if objects are obstructing us etc.
 */
public class GameArea extends Area {

    // top right corner of the square
    Tile max_corner;
    // bottom left corner of square
    Tile min_corner;

    /**
     * Declares an area between two tiles.
     * If the tiles are in a straight line(that is not diagonal) then a straight lined
     * area will be produced. Otherwise the result is a rectangle.
     * @param point1
     * @param point2
     */
    public GameArea(Tile point1, Tile point2) {
        this(getRectangle(point1, point2));
    }

    public GameArea(Shape s) {
        super(s);
        max_corner = new Tile(s.getBounds2D().getMaxX(), s.getBounds2D().getMaxY());
        min_corner = new Tile(s.getBounds2D().getMinX(), s.getBounds2D().getMinY());
    }

    // before
    public boolean inArea(Tile t) {
        //return (t.x <= max_corner.x && t.x > min_corner.x && t.y < max_corner.y && t.y > min_corner.y);
        return contains(t.x, t.y);
    }

    /**
     * Get the rectangle between two points.
     * @param point1
     * @param point2
     * @return
     */
    public static Rectangle2D.Double getRectangle(Tile point1, Tile point2) {
        Tile max_corner = new Tile(Math.max(point1.x, point2.x), Math.max(point1.y, point2.y));
        Tile min_corner = new Tile(Math.min(point1.x, point2.x), Math.min(point1.y, point2.y));
		
        return new Rectangle2D.Double(min_corner.x, min_corner.y, (int)(max_corner.x - min_corner.x), (int)(max_corner.y - min_corner.y));
    }

    /**
     * When given an area will return with occupied tiles removed.
     * Takes into account ball, opprobot, our robot.
     * @return Tiles within area that are not occupied.
     */
    public ArrayList<Tile> getUnoccupiedTiles() {
        int max_x = (int) max_corner.x;
        int min_x = (int) min_corner.x;
        int max_y = (int) max_corner.y;
        int min_y = (int) min_corner.y;

        ArrayList<Tile> unoccupied = new ArrayList<Tile>();
        for(int i = min_x; i <= max_x; i++) {
            for(int j = min_y; j < max_y; j++) {
                Tile t = new Tile(i, j);
                if(!t.isOccupied())
                    unoccupied.add(t);
            }
        }

        return unoccupied;
    }

    public GameArea getUnoccupiedArea() {
        GameArea area = this;
        if(area.contains(WorldState.oppRobot.getShape()))
            area.subtract(WorldState.oppRobot.getArea());

        if(area.contains(WorldState.myRobot.getShape()))
            area.subtract(WorldState.myRobot.getArea());

        if(area.contains(WorldState.ball.getShape().getBounds2D()))
            area.subtract(WorldState.ball.getArea());

        return area;

    }

    public String toString() {
        return ("Max: " + max_corner.toString() + ", Min: " + min_corner.toString());
    }

	public Tile[] corners(){
		Tile[] out = {min_corner,max_corner};
		return out;
	}
	

}
