package strategy.world;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Robot extends GameObject {
	public double angle = 0;
	public Tile direction = new Tile(0,0);
	public Robot(Tile loc) {
		super(loc);
	}

    public double getAngle() {
        return angle;
    }

    public void updateAngle(double angle) {
        this.angle = angle;
    }
    
    public void updateDirection(Tile direction) {
        this.direction = direction;
    }

    public Rectangle2D.Double getShape() {
    	//ControllerGUI.terminal.print("Robot at " + loc.toString());
        return new Rectangle2D.Double(loc.x - 8, loc.y - 8, 18, 18);
    }

    public GameArea getArea() {
        return new GameArea(getShape());
    }

       public double bearingTo(Tile t) {
		double newBearing = this.loc.bearingTo(t);
		double angleTo = (((360 - angle) + newBearing) % 360);
		if (angleTo>180)
			angleTo -= 360;

		return angleTo;	
       }

    public Robot() { }
	
	
}
