package strategy.world;

/**
 * Vector defined as a point on the pitch it goes through, and the direction.
 * It's lines are bounded by the pitch.
 */
public class Vector {
    // x direction of vector, can be negative
    public double dirX;
    // y direction of vector, can be negative
    public double dirY;

    // a point on the vector so we can use it as a line.
    public Tile point = new Tile(0, 0);

    /**
     * Defines a vector between two points.  Don't really care about the 'other point' in that
     * you can just give it any point that's on the line so we  can get the direction.
     * @param start
     * @param other
     */
    public Vector(Tile start, Tile other) {
        this.dirX = other.x - start.x;
        this.dirY = other.y - start.y;
        this.point = start;
    }
    
    // TODO: Needs checking
    public Vector(Tile start, double angle) {
         this.dirX = Math.cos(Math.toRadians(angle));
        this.dirY = Math.sin(Math.toRadians(angle));
        this.point = start;
    }

    /**
     * Creates a vector for a given direction as a tile.
     * @param direction
     */
    public Vector(Tile direction) {
        this.dirX = direction.x;
        this.dirY = direction.y;
        point = new Tile(0, 0);
    }

    /**
     * Gives the direction of this vector as a tile.
     * @return
     */
    public Tile getDirection() {
          return new Tile(dirX, dirY);
    }
    
    public Tile getUnitVector() {
        double len = getLength();
        return new Tile(dirX / len, dirY/ len);
    }
    
    public void setDirection(Tile t) {
        System.out.println("Called with " + t.x + ", " + t.y);
          this.dirX = t.x;
          this.dirY = t.y;
    }

    /**
     * Gets the midpoint of the resultant vector
     * TODO: Just spotted a potential bug in this so look later.
     * @param other
     * @return mid point of resultant vector
     */
    public Tile getMidpoint(Vector other) {
        this.add(other);
        return new Tile(dirX / 2, dirY / 2);
    }
    
    public double getLength() {
        return Math.sqrt(Math.pow(dirX, 2) + Math.pow(dirY, 2));
    }
    
    public double getAngleBetween(Vector other) {
        double dot = (this.dirX * other.dirX) + (this.dirY * other.dirY);
        double mod = (this.getLength() * other.getLength());
        return Math.acos(dot / mod);
        
    }
    
    public void add(Vector toAdd) {
        this.dirX = this.dirX + toAdd.dirX;
        this.dirY = this.dirY + toAdd.dirY;
    }
    
    public double dot(Vector other) {
        return (this.dirX * other.dirX) + (this.dirY * other.dirY);
    }
    
    public double mod(Vector other) {
        return (this.getLength() * other.getLength());
    }

    public void subtract(Vector toSub) {
        this.dirX = this.dirX - toSub.dirX;
        this.dirY = this.dirY - toSub.dirY;
    }
    
    public void setPoint(Tile point) {
        this.point = point;
    }


    /**
     * Find the point of intersection between two vectors.
     * According to wikipedia this works for infinite lines soo could posible return a line
     * bigger than both line segments - which works well for vectors I guess.
     * @param other
     * @return
     */
    public Tile getIntersectionPt(Vector other) {
        if(point == null || other.point == null) {
            System.out.println("One of the vectors does not have a point and thus cannot get a point of intersection");
            return null;
        }
        Tile point1 = this.point;
        // grab our point and add the direction onto it to get another point
        Tile point2 = this.point.addVector(this.dirX, this.dirY);

        Tile point3 = other.point;
        // grab our point and add the direction onto it to get another point
        Tile point4 = other.point.addVector(other.dirX, other.dirY);
        return getIntersectionPt(point1, point2, point3, point4);

    }

    /**
     * Gets the point of intersection between two lines (line1 = point1 and point2, line2 = point3 and point 4)
     * From wikipedia.
     * One the few tests i've done it's worked well :).
     * NOTE - THIS IS NOT LINE SEGMENTS. It works even if the lines do not intersect but would it they
     * were long enough. For this reason, check the returned tile is a point in a specified area (i.e the pitch)
     * @return Point of intersection between two lines.
     */
    public static Tile getIntersectionPt(Tile point1, Tile point2, Tile point3, Tile point4) {
        double x1 = point1.x;
        double y1 = point1.y;
        double x2 = point2.x;
        double y2 = point2.y;
        double x3 = point3.x;
        double y3 = point3.y;
        double x4 = point4.x;
        double y4 = point4.y;

        double line1Width = (x1 - x2);
        double line2Width = (x3 - x4);

        double line1Height = (y1 - y2);
        double line2Height = (y3 - y4);

        double det = (line1Width * line2Height) - (line1Height * line2Width);

        if(det == 0)  // lines are parrallel
            return null;

        double xPoint = (line2Width * ((x1 * y2) - (y1 * x2))) - (line1Width * ((x3 * y4) - (y3 * x4)));
        double yPoint = (line2Height * ((x1 * y2) - (y1 * x2))) - (line1Height * ((x3 * y4) - (y3 * x4)));

        return new Tile(xPoint / det, yPoint / det);

    }

    /**
     * Returns if two vectors intersect within an area (since all vectors will intersect somewhere
     * unless they are parrallel)
     * @param area
     * @param other
     * @return
     */
    public boolean intersects(GameArea area, Vector other ) {
          Tile point = this.getIntersectionPt(other);
        if(area.inArea(point))
            return true;
        return false;
    }
    
  
    public String toString() {
        return "Direction " + dirX + ", " + dirY + " point " + point.toString();
    }


}
