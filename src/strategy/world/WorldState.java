/**
 * Idea for this to have a map of the world and the states of everything.
 * This will effectively, be a re-write of the 'CurrentGlobalState'.
 * TODO: handling of which is our goal and which is opponents
 */
package strategy.world;
import strategy.Calculations;
import java.lang.*;
import commander.WorldPanel;
import strategy.BallMonitor;

public class WorldState {
	
	public static Ball ball = new Ball();
	public static Robot myRobot = new Robot();
	public static Robot oppRobot = new Robot();
	public static Goal myGoal = new Goal();
	public static Goal oppGoal = new Goal();
	
	public static boolean isOurRobotBlue = true; // will need to be changed later so it's better handled.
	public static boolean shootLeft = true;

   public static boolean takePenalty = false;
   public static boolean defendPenalty = false;

    // not sure if we really need these, however moved here so we can finally remove GlobalCurrentState stuff.
   public static boolean visionConnected = false;
   public static boolean quitVision = false;

    public static Tile bottomLeftCorner = new Tile(0, 122);
    public static Tile bottomRightCorner = new Tile(244, 122);
    public static Tile topLeftCorner = new Tile(0, 0);
    public static Tile topRightCorner = new Tile(244, 0);

	public static double blueRobotAngle;
	public static double yellowRobotAngle;

    public static Tile blueRobotLocation;
    public static Tile yellowRobotLocation;
    
    public static Tile blueRobotDirection;
    public static Tile yellowRobotDirection;

	
	 /* Processes information from vision server and updates objects.
	 * Syntax obtained from vision server:
	 * socket.send(ball[0], ball[1], blue_pos[0], blue_pos[1], blue_dir[0], blue_dir[1], yellow_pos[0], yellow_pos[1], yellow_dir[0], yellow_dir[1], 		 * our_goal[0], our_goal[1], their_goal[0], their_goal[1])
	 */
	public static void update(String message) {
		String[] information = message.split(";"); // information from vision system.
		Tile[] objectCoordinates = new Tile[information.length];
		
		for(int i = 0; i < information.length; i++) {
			String[] coordinate = information[i].split(",");
			objectCoordinates[i] = new Tile(Double.parseDouble(coordinate[0]), Double.parseDouble(coordinate[1]));
		}
		
		

		ball.updateLocation(objectCoordinates[0]);

		blueRobotAngle = Calculations.angleInDegrees(objectCoordinates[1], objectCoordinates[2]);
		yellowRobotAngle = Calculations.angleInDegrees(objectCoordinates[3], objectCoordinates[4]);
		
  		blueRobotLocation = objectCoordinates[1];
       	yellowRobotLocation = objectCoordinates[3];

       	blueRobotLocation = objectCoordinates[1];
    	yellowRobotLocation = objectCoordinates[3];
    	blueRobotDirection = objectCoordinates[2];
    	yellowRobotDirection = objectCoordinates[4];


		if(isOurRobotBlue) {
			    myRobot.updateLocation(objectCoordinates[1]); // blue
			    oppRobot.updateLocation(objectCoordinates[3]); // yellow.
			
        		myRobot.updateAngle(blueRobotAngle); 
        		oppRobot.updateAngle(yellowRobotAngle);
        		
        		myRobot.updateDirection(blueRobotDirection); 
        		oppRobot.updateDirection(yellowRobotDirection);

        	} else {
			    myRobot.updateLocation(objectCoordinates[3]); // yellow
			    oppRobot.updateLocation(objectCoordinates[1]); // blue

        		myRobot.updateAngle(yellowRobotAngle); 
        		oppRobot.updateAngle(blueRobotAngle);
        		
        		myRobot.updateDirection(yellowRobotDirection); 
        		oppRobot.updateDirection(blueRobotDirection);
		}
		BallMonitor.maintainArrays();		
	}
	
	public static String worldToString() {
		String s = "Ball: " + ball.getLocation() + "\nOur robot: " + myRobot.getLocation() + " Facing: " + myRobot.getAngle() + "\nOpponent robot: " 
				   + oppRobot.getLocation() + " Facing: " + oppRobot.getAngle() + "\n Our goal: " + myGoal.getLocation() + "\nOpponent goal: " 
				   + oppGoal.getLocation();
		return s;
	}
	
	
}
