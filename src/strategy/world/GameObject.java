package strategy.world;
import java.awt.*;
import java.awt.geom.Point2D;


public abstract class GameObject {
	public Tile loc;
    static double DIAM = 10;
    static double CHARGE = -1;
    static double MASS = 10;
    
    public double diam;
    public double mass;
    public double charge;
     
	public GameObject() { 
	}

	public GameObject(Tile loc) {
		this.loc = loc;
        this.diam = DIAM;
        this.charge = CHARGE;
        this.mass = MASS;
	}

	abstract Shape getShape();
	
	public GameObject(Tile loc, double charge, double diam) {
	     this.loc = loc;
         this.diam = diam;
         this.charge = charge;
         this.mass = MASS;
   	 }
	
	public Tile getLocation() {
		return loc;
	}
	
	public void updateLocation(Tile loc) {
		this.loc = loc;
	}
	
	/**
	 * @param go GameObject we want to calculate distance to.
	 */
	public double distanceTo(GameObject go) {
		Tile otherObject = go.getLocation();
		return Math.sqrt(Math.pow((loc.x - otherObject.x), 2)+Math.pow((loc.y - otherObject.y),2));
	}

	public double distanceTo(Tile tiley) {
		return Math.sqrt(Math.pow((loc.x - tiley.x), 2) + Math.pow((loc.y - tiley.y), 2));
	}
	
     public double distanceDiamSq(GameObject r) {
         double d = distanceDiam(r);
         return d * d;
     }
     
     public double distanceDiam(GameObject r) {
         double d = loc.distanceTo(r.loc) - (diam + r.diam) / 2;
         return d > 0?  d: 0.0000001;
     }


	
}
