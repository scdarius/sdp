package strategy.world;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Ellipse2D;
public class Ball extends GameObject {
	
	public Ball() { }
	
	public Ball(Tile loc) {
		super(loc);
	}
    
    public Ellipse2D getShape() {
        return new Ellipse2D.Double(loc.x - 3, loc.y - 3, 6, 6);
    }
    
    public GameArea getArea() {
        return new GameArea(getShape());
    }

}
