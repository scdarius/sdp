package strategy.world;
import java.awt.*;
import java.awt.geom.Line2D;

import commander.ControllerGUI;
public class Goal extends GameObject {
	public Tile top;
	public Tile bottom;
	
	public Goal(Tile loc) {
		super(loc);
		System.out.println(loc.toString());
		top = new Tile(loc.x, loc.y + 30);
		bottom = new Tile(loc.x, loc.y - 30);
	}
	
	public Goal() { 
		super(new Tile(0, 0));
	}

    public Line2D getShape() {
        return new Line2D.Double(top.x, top.y, bottom.x, bottom.y);
    }
    
    public GameArea getArea() {
        return new GameArea(getShape());
    }
    
    /**
     * Overrides GameObject's updateLocation so to deal with top/bottom.
     */
    public void updateLocation(Tile t) {
    	loc = t;
    	top = new Tile(loc.x, loc.y + 30);
    	bottom = new Tile(loc.x, loc.y - 30);
    }

    /**
     * Gives us a line of tiles on the goal line at points along the line of 10, 20, 30, 40, 50.
     * @return goal line.
     */
    public Tile[] getGoalLine() {
        int y_line = (int) (top.y - bottom.y);
      //  System.out.println(loc.toString());
        Tile[] goalTiles = new Tile[5];
        for(int i = 0; i < 5; i++) {
              goalTiles[i] = new Tile(loc.x, (bottom.y + 10*i) + 10);
        }
      return goalTiles;
    }

	
    
}
