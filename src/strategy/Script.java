package strategy;

public interface Script {
	
	/**
	 * @return waiting time for game loop. Return -1 to make script stop.
	 */
	public int run(String s);
}