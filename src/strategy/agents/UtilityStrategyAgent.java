package strategy.agents;

import java.util.ArrayList;


import commander.ControllerGUI;

/**
 * One of the 'Agent' strategies.
 */

public class UtilityStrategyAgent extends Agent {
	ArrayList <Strategy> strategies = new ArrayList<Strategy>();
	
	public UtilityStrategyAgent() {
		strategies = loadStrategies();
	}
	
	public String getAgentName() {
		return "UtilityStrategyAgent";
	}
	
	public Strategy getChosenStrategy() {
		
		double highestUtility = 0;
		Strategy currentStrategy = null;
		
		if(ControllerGUI.attack) {
			for(Strategy s : strategies) {
				if(s.getUtility() < 0 || s.getUtility() > 100) {
					System.out.println("Utility out of range");
					return null;
				}
					
				if(currentStrategy == null)
					currentStrategy = s;
				
				if(s.getUtility() > highestUtility) {
					currentStrategy = s;
					highestUtility = s.getUtility();
				}
			}
			
		} 	
		return currentStrategy;
	}


}
