package strategy.agents;

import commander.ControllerGUI;

public class AgentEngine implements Runnable {
	Agent agent;
	public static boolean runAgent = true;
	public AgentEngine(Agent a) {
		ControllerGUI.terminal.print("Running Agent " + a.getAgentName());
		this.agent = a;
	}

	@Override
	public void run() {
		while(runAgent) {
		Strategy chosen = agent.getChosenStrategy();
		ControllerGUI.terminal.print("Selected strategy " + chosen.getClass().getCanonicalName());
		int wait = chosen.run("");
		try {
			Thread.sleep(wait);
		} catch(Exception e ) {
			System.out.println("Error running strategy");
			e.printStackTrace();
		}
		}
	}

}
