package strategy.agents;

import java.io.File;
import java.util.ArrayList;

import commander.ControllerGUI;
import strategy.AccessorMethods;
import strategy.strategies.*;

import javax.swing.*;

/**
 * Provides methods that can be used with all agents.
 */

public abstract class Agent extends AccessorMethods {
	
	public abstract Strategy getChosenStrategy();
	public abstract String getAgentName();
	
	public ArrayList<Strategy> strategies;
	
	public ArrayList<Strategy> loadStrategies() {
		String currentdir = System.getProperty("user.dir");	
		ArrayList<Strategy> strategies = new ArrayList<Strategy>();
		try {
			File f = new File(currentdir + File.separator + "strategy" + File.separator + "strategies");
            String[] names = f.list();
            if(names == null) {
                f =  new File(currentdir + File.separator + "src" + File.separator + "strategy" + File.separator + "strategies");
                names = f.list();
            }
			for(String s : names) { 
				String name = s.split("\\.")[0];
				Strategy strategy = (Strategy)Class.forName("strategy.strategies."+name).newInstance();
				strategies.add(strategy);
                ControllerGUI.strategyDisplay.append("Loaded Strategy " + name + "\n");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.strategies = strategies;
		return strategies;
	}
	

	
}
