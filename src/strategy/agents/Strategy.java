package strategy.agents;

import strategy.Script;

public interface Strategy extends Script {
	
	public double getUtility(); //how good an action is given the world sate

}
