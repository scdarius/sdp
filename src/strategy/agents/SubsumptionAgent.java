package strategy.agents;

import commander.ControllerGUI;

import javax.swing.*;
import java.util.ArrayList;

public class SubsumptionAgent extends Agent {
	
	ArrayList<Strategy> potentialStrategies = new ArrayList<Strategy>();
	public SubsumptionAgent() {
		loadStrategies();
	}	
	
	public String getAgentName() {
		return "SubsumptionAgent";
	}
	
	/**
	 * Goes through all the strategies and decides which one can run (utility does not return 0)
	 * @return
	 */
	public ArrayList<Strategy> getPotentialStrategies() {
		ArrayList<Strategy> canRun = new ArrayList<Strategy>();
        ControllerGUI.strategyDisplay.setText("");
		for(Strategy s : strategies) {
			Class c = s.getClass();
			if(s.getUtility() != 0) {
                ControllerGUI.strategyDisplay.append("Can run strategy " + c.getCanonicalName());
				canRun.add(s);
			}
		}
		this.potentialStrategies = canRun;
		return canRun;
	}

	@Override
	public Strategy getChosenStrategy() {
		if(getPotentialStrategies().size() > 1) {
            ControllerGUI.strategyDisplay.append("Precondition conflicts for: ");
            for(Strategy s : potentialStrategies) {
                ControllerGUI.strategyDisplay.append(s.getClass().getCanonicalName());
            }
		} else {
            Strategy toRun = potentialStrategies.get(0);
            ControllerGUI.strategyDisplay.append("Running strategy " + toRun.getClass().getCanonicalName());
			return toRun;
		}
        ControllerGUI.strategyDisplay.append("No strategy can be run");
		return null;
	}

	
}
