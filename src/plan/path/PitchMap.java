package plan.path;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import strategy.world.Tile;
import plan.path.pathfinding.Robot;
import plan.path.pathfinding.WorldMap;
import commander.ControllerGUI;

/**
 * A class which defines the abstract representation of the real pitch, and reflects
 * all the changes of that occur in the real world. 
 *
 * @author Daniel Stanoescu
 */

public class PitchMap implements WorldMap{

	/** Setting the size of the search space for the A* algorithm  */

	public static final int width = 244;
	public static final int height = 122;
	
	/** We define all the different types of terrains we are going to use on the pitch  */

	public static final int grass = 0;
	public static final int water = 1;
	
	/** Define all the objects on the pitch, this is used to mark the existance of our bot and different obstacles */	
	public static final int ourRobot = 2;
	public static final int opponent = 3;
	public static final int ballPad = 5;
	public static final int opponentCenter = 4;
	
	/** The terrain matrix which holds all the different terrain information */
	private int[][] terrain = new int[width][height];
	
	/** The unit matrix which holds information about the position of differen units */
	private int[][] units = new int[width][height];
	
	/** The visited matrix which keeps track of all the visited nodes throughout the PitchMap */
	private boolean[][] visited = new boolean[width][height];
	
			/**
			 * Method for filling an area with a constrain type on the map
			 * 
			 * @param x : The x coordinate where you want to start filling
			 * @param y : The y coordinate where you want to start filling
			 * @param witdh : The width in tiles of the size of the area you want to fill
			 * @param height : The height in tiles of the size of the area you want to fill
			 * @param type : The type of field you want to use. Use 1 by default
			 * @return Fills the appropiate area with the specified type of terrain
			 */
	
			public void fillArea(int x, int y, int width, int height, int type) {
				for (int xp=x;xp<x+width;xp++) {
					for (int yp=y;yp<y+height;yp++) {
						terrain[xp][yp] = type;
					}
				}
			}

			public void fillArea(Tile tile, int width, int height, int type){
				int x = (int) ((tile.x/244.0)* this.width);
				int y = (int) ((tile.y/122.0) * this.height);
				for (int xp=x;xp<x+width && xp < this.width;xp++) {
					for (int yp=y;yp<y+height && yp < this.height;yp++) {
						terrain[xp][yp] = type;
					}
				}
			}

			public void fillFromCentre(Tile tile, int width, int height, int type){
				//Fills from a given centre point out to width and height values on every side
				int xC = (int) ((tile.x/244.0)* this.width);
				int yC = (int) ((tile.y/122.0) * this.height);
				int x0 = xC - width;
				int y0 = yC - height;

				System.out.println("Output variables " + xC + " " + yC + " " + x0 + " " + y0);

				if(x0 < 0) {
					x0 = 0;
				}
				if(y0 < 0) {
					y0 = 0;
				}
				if(xC >= this.width - width){
					xC = this.width - width - 1;
				}
				if(yC >= this.height - height){
					yC = this.height - height - 1;
				}

				for (int x = x0; x <= xC + width; x++){
					for(int y=y0; y<= yC + height; y++){
						//System.out.println("Firing at : " + x + " " + y);
						terrain[x][y] = type;
					}
				}
			}

				

			/**
			 * Method used for surrounding the center of the robot with water so we don't
			 * bump into it and plan arround it.
			 *
			 * @param center The center of the robot is used to pad the area around it with a terrain type.
			 * @return Fills the robot area on the terrain matrix.
			 */
			
			public void fillEnemyRobot(Tile center){
				//This is still inadequate, assumes robot is facing completely vertically/ does not take into account the exact size
				//of this year's robot. What if robot is at say 45 degree angle?
				int xC = (int) ((center.x/244.0) * width); //Normalize location in terms of grid size
				int yC = (int) ((center.y/122.0) * height); 
				int x0 = xC-24;
				int y0 = yC- 24;
				
				if (x0 < 0){
					x0 = 0;
				}
				if (xC >= width - 30){
					xC = width - 31;
				}
				if (y0 < 0){
					y0 = 0;
				}
				if (yC >= height - 30){
					yC = height - 31;
				}


				for (int x=x0; x<xC+31; x++){
					for (int y=y0; y<yC+31; y++){
						terrain[x][y] = 3;
					}
				}
				terrain[xC][yC] = 4;
			}
			
			/** Method used to clear the visited tiles on the map */
			public void clearVisited() {
				for (int x=0;x<getWidthInTiles();x++) {
					for (int y=0;y<getHeightInTiles();y++) {
						visited[x][y] = false;
					}
				}
			}
					
			/** Checks if the tile at coordinates (x,y) has been visited
			 *
			 * @param x The x coordinates of the tile 
			 * @param y The y coordinates of the tile
			 * @return The tile at (x,y)
			 */

			public boolean visited(int x, int y) {
				return visited[x][y];
			}
			
			/** Returns the terain type at coordinates (x,y)
			 *
			 * @param x The x coordiante of the tile
			 * @param y The y coordinate of the tile
			 * @return The type of the tile (x,y)
			 */

			public int getTerrain(int x, int y) {
				return terrain[x][y];
			}
			
			/** Returns the unit type at coordinates (x,y)
			 *
			 * @param x The x coordinate of the unit
			 * @param y The y coordinate of the unit
			 * @return The unit at coordinates (x,y)
			 */
			public int getUnit(int x, int y) {
				return units[x][y];
			}
			
			/** Sets a tile to a unit of a mentioned type at (x,y)
			 *
			 * @param x The x coordinate of the unit
			 * @param y The y coordinate of the unit
			 * @param unit The specified unit type
			 * @return Sets the unit at coordinate(x,y) on the unit map
			 */

			public void setUnit(int x, int y, int unit) {
				units[x][y] = unit;
			}
			//For if you pass in real word values as opposed to grid locations
			public void setUnit(Tile tile, int unit){
				int x = (int) ((tile.x/244.0) * width);
				int y = (int) ((tile.y/122.0) * height);
				units[x][y] = unit;
			}

			/**
			 * Constraints the robot from moving onto that tile and specifies
			 * the type of terrain that the robot can't move onto.
			 *
			 * @param robot The unit which you want to move on a tile
			 * @param x The x coordinates of the tile where you want to move the robot
			 * @param y The y coordinates of the tile where you want to move the robot
			 */

			public boolean blocked(Robot robot, int x, int y) {
				// if there's a unit at the location, then it's blocked
				if (getUnit(x,y) != 0) {
					return true;
				}
					
				int unit = ((Unit) robot).getType();
				
				// constrains ourRobot to only walk on grass
				if (unit == ourRobot) {
					return terrain[x][y] != grass;
				}
				
				// unknown unit so everything blocks
				return true;
			}
			
			/** Sets the cost of the robot moving from one point to the other */
			public float getCost(Robot robot, int sx, int sy, int tx, int ty) {
				return 1;
			}
			
			/** Gets the hight of the pitch map */	
			public int getHeightInTiles() {
				return height;
			}

			/** Gets the width of the pitch map */
			public int getWidthInTiles() {
				return width;
			}
			
			/** Marks the tile at (x,y) as visited */
			public void pathFinderVisited(int x, int y) {
				visited[x][y] = true;
			}		


			public void printMap(){
				ArrayList<Tile> enemyPoints = new ArrayList<Tile>();
				ArrayList<Tile> ballPoints = new ArrayList<Tile>();
				Tile[] tilesToPrint;
				for (int y = 0; y < height; y++){
					for(int x = 0; x < width; x++){
						if(terrain[x][y] == opponent){
							enemyPoints.add(new Tile(x, y));
						}
						else if(terrain[x][y] == ballPad){
							ballPoints.add(new Tile(x, y));
						}
						
					}

				}


				


				if(enemyPoints.size() != 0 && ballPoints.size() != 0){
					tilesToPrint = new Tile[4];
					tilesToPrint[0] = enemyPoints.get(0);
					tilesToPrint[1] = enemyPoints.get(enemyPoints.size() - 1);
					tilesToPrint[2] = ballPoints.get(0);
					tilesToPrint[3] = ballPoints.get(ballPoints.size() - 1);

				}else if(ballPoints.size() != 0){
					tilesToPrint = new Tile[2];
					tilesToPrint[0] = ballPoints.get(0);
					tilesToPrint[1] = ballPoints.get(1);
				}else{
					tilesToPrint = new Tile[2];
					tilesToPrint[0] = new Tile(0, 0);
					tilesToPrint[1] = new Tile(10, 10);
				}

				//for(int i = 0 ; i < tilesToPrint.length; i++){
				//	System.out.println("tile " + i + " is: " + tilesToPrint[i]);
				//}





				ControllerGUI.paintRect(tilesToPrint);
			}

		
	
	
}
