package plan.path;

import java.awt.geom.Point2D;
import java.awt.geom.Line2D;
import plan.path.pathfinding.AStar;
import plan.path.pathfinding.Path;
import plan.path.pathfinding.PathFinder;
import strategy.world.*;

/**
 * Computes an A* Search path and simplifies it to a set of waypoints such taht
 * it can send the robot a smaller number of points.
 *
 * @author Daniel Stanoescu
 */

public class Finder {

	/** Creates an abstract representation of the pitch on which the robots must move.*/
	public PitchMap map = new PitchMap();
	
	/** Creates a Pathfinder which uses A* search to find a path.*/
	private PathFinder finder;
	
	/** Creates a data structure that holds all the information about the path. */
	private Path path;
		
	/**
	 * Fill the area of the map with a type of terrain with height h and width w
	 * starting from coordinates (x,y)
	 *
	 * @param x The x coordinate of the point used to start the fill-up
	 * @param y The y coordinate of the point used to start the fill-up
	 * @param w The width of the rectangle that will fill the map
	 * @param h The height of the rectangle that will fill the map
	 * @param t The type of terrain used to fill the map
	 * @return Fills the map with the specified terrain
	 */
	public void fill(int x, int y, int w, int h, int t){
		map.fillArea(x, y, w, h, t);
	}

	public void fill(Tile tile, int w, int h, int t){
		map.fillArea(tile, w, h, t);
	}
	
	/**
	 * Runs an A* search on the pitchmap given an origin and a destination
	 *
	 * @param selectedx The x coordinate of the origin of the path
	 * @param selectedy The y coordinate of the origin of the path
	 * @param dx The x coordinate of the path destination
	 * @param dy The x coordinate of the path destination
	 * @return The steps which make up the path
	 */

	public Path answer(int selectedx, int selectedy, int dx, int dy){
		
		finder = new AStar(map, 300, true); 
		path = finder.findPath(new Unit(map.getUnit(selectedx, selectedy)), selectedx, selectedy, dx, dy);
		return path;
	}
	
// =================================================================================
// ====================== Path Simplification (Waypoints) =========================
// =================================================================================

	/**
	 * Takes a full path computed by the A* Search and calculates points
	 * where the vectors that make up the path are changing
	 *
	 * @param pC Takes as input an A* search found path\
	 * @return Returns a Point2D.Float list of waypoints. 
	 */

	public Tile[] simplePath(Path pC, double oppRad,  double ballRad) {	
	

		Line2D line;
		double oppX = WorldState.oppRobot.getLocation().getX();
		double oppY = WorldState.oppRobot.getLocation().getY();
		double ballX = WorldState.ball.getLocation().getX();
		double ballY = WorldState.ball.getLocation().getY();

		boolean ballOverLap = false;
		boolean oppOverLap = false;

		int c = 0;
		

		while(c < pC.getLength() - 2){
			int aX = pC.getStep(c).getX();
			int aY = pC.getStep(c).getY();
			int bX = pC.getStep(c + 1).getX();
			int bY = pC.getStep(c + 1).getY();
			int cX = pC.getStep(c + 2).getX();
			int cY = pC.getStep(c + 2).getY();
			double oppDistToLine = getDistToLine(aX, aY, cX, cY, oppX, oppY);
			double ballDistToLine = getDistToLine(aX, aY, cX, cY, ballX, ballY);

			System.out.println("opponent distance to line is:" + oppDistToLine + " and radius is: " + oppRad);
			System.out.println("ball distance to line is: " + ballDistToLine + " and radius is: " + ballRad);

			if((oppDistToLine > oppRad) && (ballDistToLine > ballRad)){
				pC.removeStep(c+1);
				System.out.println("REMOVING");
			}else{
				c++;
				System.out.println("OK");
			}
		}	

		c = 0;

		while(c < pC.getLength() - 2){
			double aX = pC.getStep(c).getX();
			double aY = pC.getStep(c).getY();
			double bX = pC.getStep(c + 1).getX();
			double bY = pC.getStep(c + 1).getY();
			double cX = pC.getStep(c + 2).getX();
			double cY = pC.getStep(c + 2).getY();

			Line2D.Double lineSeg = new Line2D.Double(aX, aY, cX, cY);

			if(lineSeg.ptLineDist(bX, bY) < 0.1){
				pC.removeStep(c+1);
				System.out.println("ACTUALLY FIRES");
			}else{
				c++;
			}
		}

		pC.removeStep(0);

			
		


		if(pC.getLength() < 1){
			System.out.println("Path only consisted of a single point!!!");
			return null;
		}

		Tile[] wayPoints = new Tile[pC.getLength()];

		System.out.println("The number of simplified steps: " + (pC.getLength()));
		
		for (int k = 0; k< pC.getLength(); k++){
		
			wayPoints[k] = new Tile(pC.getStep(k).getX() , pC.getStep(k).getY()); //Hard coded, please generalise
		//	System.out.println("Corected (x,y) [" + k + "] : " + wayPoints[k].x + " | " + wayPoints[k].y);
		}
		return wayPoints;
		
	}

	private double getDistToLine(double aX, double aY, double bX, double bY, double oppX, double oppY){
		/*double lineSegX = bX - aX;
		double lineSegY = bY - aY;
		double origLineDistSquared = (lineSegX * lineSegX + lineSegY * lineSegY);
		double u = ((oppX - aX) * (lineSegX) + (oppY - bY)* (lineSegY)) / origLineDistSquared;
		double poiX = aX + u * lineSegX;
		double poiY = aY + u * lineSegY;
		return Math.sqrt(Math.pow((poiX - oppX), 2) + Math.pow((poiY - oppY),2));
		*/

		Line2D.Double lineseg = new Line2D.Double(aX, aY, bX, bY);
		return lineseg.ptSegDist(oppX, oppY);
	}
		
	
}
