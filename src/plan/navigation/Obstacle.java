/**
 * 
 */

/**
 * @author alexandru
 *
 */
 package plan.navigation;

public interface Obstacle {
	public double distanceTo(Grid to) ;
	public double distanceTo(Robot to) ;
	public String toString();
}
