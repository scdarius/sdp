/**
 * 
 */

/**
 * @author alexandru
 *
 */
package plan.navigation;

import java.util.ArrayList;
import java.awt.geom.Line2D;

public class ExtraPointsFilter {

	/**
	 * 
	 */
	public ExtraPointsFilter() {
		// TODO Auto-generated constructor stub
	}
	private static double distanceTo(Grid l1, Grid l2, RoundObstacle o, double radius) {
		return Line2D.ptSegDist(l1.x, l1.y, l2.x, l2.y, o.pos.x, o.pos.y) - o.radius - radius;
	}
	
	
	public static Grid[] filter(Grid[] data, RoundObstacle[] obstacles, double radius) {
		if (data.length < 3)
			return data;
		ArrayList<Grid> newPath = new ArrayList<Grid>();
		int position = 0;
		newPath.add(data[0]);
		
		for (int i =1; i+1 < data.length; i++) {
			Boolean valid = true;
			for (RoundObstacle o : obstacles) {
				if (distanceTo(data[position], data[i+1] , o, radius) < 0) {
					valid = false;
					break;
				}
			}
			if (!valid){ //otherwise we can remove data[i]
				newPath.add(data[i]);
				position = i;
			}
			
		}
		
		newPath.add(data[data.length - 1]);
		return newPath.toArray(new Grid[newPath.size()]);
		
		
		
		
	}
}
