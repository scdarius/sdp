/**
 * 
 */

/**
 * @author alexandru
 *
 */
 
package plan.navigation;


public class PathPoint implements Comparable<PathPoint>{
	public Robot robot;
	public Grid goal;
	/**
	 * 
	 */
	public PathPoint(Robot robot, Grid goal) {
		this.robot = robot;
		this.goal = goal;
	}
	
	
	public int compareTo( PathPoint that) {
	    final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    
	    if (this.robot.distanceTo(goal) < that.robot.distanceTo(goal)) 
	    	return BEFORE;
	    
	    if (this.robot.distanceTo(goal) > that.robot.distanceTo(goal)) 
	    	return AFTER;
	    
	    return EQUAL;
	}
	
}
