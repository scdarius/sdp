package plan.navigation;

public class Grid {
    public int x, y;

    public Grid(int x, int y) {
            this.x = x;
            this.y = y;
    }
    
    public int getX() {
            return x;
    }
    
    public int getY() {
            return y;
    }
    
    public Grid add(Grid operand) {
    	return new Grid(x + operand.x, y + operand.y);
    }
    
    /*
     * @param Grid to calculate distance too.
     * @TODO - Needs to deal with measurements once we know what the co-ordinate system will be.
     */
    public double distanceTo(Grid to) {
            return Math.sqrt(Math.pow((to.x - x), 2)+ Math.pow((to.y - y),2));
    }
    
    public String toString() {
            return "(" +x + ", " + y + ")";
    }
    

}
