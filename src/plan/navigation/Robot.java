/**
 * 
 */

/**
 * @author alexandru
 *
 */
package plan.navigation;

public class Robot {
	public Grid pos;
	public double radius;
	
	public Robot(Grid pos, double radius){
		this.pos = pos;
        this.radius = radius;
	}
	
	public double distanceTo(Grid to) {
        double dist =  Math.sqrt(Math.pow((to.x - pos.x), 2)+ Math.pow((to.y - pos.y),2)) - radius;
        //yes the distance can be negative
        return dist;
	}
	
	public double distanceTo(Obstacle to) {
        return to.distanceTo(this);
	}
	
	public String toString() {
        return "robot: (" + pos.x + ", " + pos.y + "), r = " + radius;
}

	
}
