/**
 * 
 */

/**
 * @author alexandru
 *
 */
 
package plan.navigation;


public class Runner {

	/**
	 * 
	 */
	public Runner() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Obstacle top = new LineObstacle(new Grid(0, 0), new Grid(244, 0));
		Obstacle bottom = new LineObstacle(new Grid(0, 122), new Grid(244, 122));
		Obstacle left = new LineObstacle(new Grid(0, 0), new Grid(0, 122));
		Obstacle right = new LineObstacle(new Grid(244, 0), new Grid(244, 122));
		
		Obstacle opponent = new RoundObstacle(new Grid(170, 21), 15);
		Obstacle ball = new RoundObstacle(new Grid(50, 21), 2.5);
		
		Obstacle obstacles[] = {opponent, ball, top, bottom, left, right};
		RoundObstacle robstacles[] = {(RoundObstacle) opponent, (RoundObstacle) ball};
		Grid goal = new Grid(30, 21);
		Robot robot = new Robot(new Grid(200, 21), 15);
		
		PathFinder aStar = new PathFinder(robot, goal, obstacles, 244, 122);
		
		Grid path[] = aStar.findPath();
		
		int last_path_length = 0;
		
		do {
			last_path_length = path.length;
			path = RamerDouglasPeuckerFilter.filter(path, 1);
			path = ExtraPointsFilter.filter(path, robstacles, robot.radius);
		} while (path.length != last_path_length);
		
		
		
		System.out.println("Path:");
		for (Grid point : path) {
			System.out.print(point + ",");
		}
		
		

	}

}
