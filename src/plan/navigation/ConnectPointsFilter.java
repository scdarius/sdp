/**
 * 
 */

/**
 * @author alexandru
 *
 */
package plan.navigation;
import java.util.ArrayList;
import java.util.Arrays;

public class ConnectPointsFilter {

	/**
	 * 
	 */
	public ConnectPointsFilter() {
		// TODO Auto-generated constructor stub
	}
	
	public static Grid[] filter(Grid[] data, double epsilon) {
		if (data.length < 3)
			return data;
		ArrayList<Grid> newPath = new ArrayList<Grid>(Arrays.asList(data));
		int i = 0;
		
		
		while (i + 2 < newPath.size()) {
			if (newPath.get(i).distanceTo(newPath.get(i+1)) <= epsilon ){
				Grid inter = new Grid( (newPath.get(i).x + newPath.get(i+1).x)/2,  (newPath.get(i).y + newPath.get(i+1).y)/2);
				newPath.set(i, inter);
				newPath.remove(i+1);
			}
			else {
				i++;
			}
		}
		
		return newPath.toArray(new Grid[newPath.size()]);
		
		
		
		
	}
}
