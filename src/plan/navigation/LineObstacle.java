/**
 * 
 */

/**
 * @author alexandru
 *
 */
package plan.navigation;
import java.awt.geom.Line2D;

public class LineObstacle implements Obstacle{

	public Grid pos1, pos2;
	
	public LineObstacle(Grid pos1, Grid pos2) {
		this.pos1 = pos1;
		this.pos2 = pos2;
	}
		
	public double distanceTo(Grid to) {
        return  Line2D.ptLineDist(pos1.x, pos1.y, pos2.x, pos2.y, to.x, to.y);
	}
	
	public double distanceTo(Robot to) {
        double dist =  this.distanceTo(to.pos) - to.radius;
        //yes the distance can be negative if the robot is outside the pitch
        return dist;
	}
	
	public String toString() {
        return "(" + pos1.x + ", " + pos1.y + "),("  + pos2.x + ", " + pos2.y + ")";
}
	
}
