
/**
 * @author alexandru
 *
 */

package plan.navigation;

import java.util.PriorityQueue;
import java.util.ArrayList;
import java.util.Collections;

public class PathFinder {
	Robot robot;
	Obstacle obstacles[];
	Grid goal;
	int width;
	int height;
	/**
	 * 
	 */
	public PathFinder(Robot robot, Grid goal, Obstacle obstacles[], int width, int height) {

	    if (robot.pos.x + robot.radius > width)
	        robot.pos.x = width - (int) Math.ceil(robot.radius);
	    if (robot.pos.x - robot.radius < 0)
	        robot.pos.x = (int) Math.ceil(robot.radius);   
        if (robot.pos.y + robot.radius > height)
	        robot.pos.y = height - (int) Math.ceil(robot.radius);
	    if (robot.pos.y - robot.radius < 0)
	        robot.pos.y = (int)Math.ceil(robot.radius);   
		this.robot = robot;

		this.goal = goal;
        if (goal.x  > width)
	        goal.x = width;
	    if (goal.x  < 0)
	        goal.x = 0;   
        if (goal.y  > height)
	        goal.y = height;
	    if (goal.y < 0)
	        goal.y = 0; 		
		
		this.obstacles = obstacles;
		this.width = width;
		this.height = height;
	}
	
	private Grid[] getPath(Grid comingFrom[][], Grid to){
		ArrayList<Grid> path = new ArrayList<Grid>();
		
		Grid position = to;
		while ((position.x != this.robot.pos.x) || (position.y != this.robot.pos.y)) {
			path.add(position);
			position = comingFrom[position.x][position.y];
			if (position == null) {
				//we failed to find a path, it should not happend as long as we call getPath only when we got 
				//TODO: solve this case
			}
		}
		path.add(this.robot.pos);
		Collections.reverse(path);
		return path.toArray(new Grid[path.size()]);
	}
	
	public Grid[] findPath() {
		PriorityQueue<PathPoint> queue = new PriorityQueue<PathPoint>();	
		Grid comingFrom[][] = new Grid[width][height];
		Grid bestPosition = robot.pos; // we will return a path to this position if we can't find a full path.
		double min_dist = Double.MAX_VALUE;
		
		Grid directions[] = new Grid[8];
		int index = 0;
		for (int i = -1; i<2; i++){
			for (int j = -1; j<2; j++) {
				if (i!=0 || j!=0) {
					directions[index] = new Grid(i, j);
					index++;
				}
			}
		}
		
		queue.add(new PathPoint(this.robot, this.goal));
		comingFrom[this.robot.pos.x][this.robot.pos.y] = this.robot.pos;
		
		while (queue.size() > 0){
			PathPoint p = queue.poll();
			
			for (Grid direction : directions){
				Robot newRobot = new Robot(p.robot.pos.add(direction), p.robot.radius);
				if ( comingFrom[newRobot.pos.x][newRobot.pos.y] != null)
					continue;
				Boolean valid = true;
				for (Obstacle obstacle : obstacles){
					if (newRobot.distanceTo(obstacle) < 0){
						valid = false;
						break;
					}
				}
				if (valid) {
					queue.add(new PathPoint(newRobot, this.goal));
					comingFrom[newRobot.pos.x][newRobot.pos.y] = p.robot.pos;
					if (newRobot.distanceTo(goal) < min_dist) {
						
						bestPosition = newRobot.pos;
						min_dist = newRobot.distanceTo(goal);
					}
					if ((newRobot.pos.x == goal.x) && (newRobot.pos.y == goal.y)) {
						return getPath(comingFrom, goal);
					}
						
				}	
			}
			
		}
		//if we reached this point we did not managed to find a path
		//
		return getPath(comingFrom, bestPosition);
	
	}
	
}
