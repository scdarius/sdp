package plan.navigation;
import java.awt.geom.Line2D;

/**
 * Filters data using Ramer-Douglas-Peucker algorithm with specified tolerance
 * 
 * @author Alexandru
 * @see <a href="http://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm">Ramer-Douglas-Peucker algorithm</a>
 */
public class RamerDouglasPeuckerFilter {

       
        /**
         * 
         *
         */
        public RamerDouglasPeuckerFilter() {
        	
        }

        public static Grid[] filter(Grid[] data, double epsilon) {
                return ramerDouglasPeuckerFunction(data, 0, data.length - 1, epsilon);
        }


        protected  static Grid[] ramerDouglasPeuckerFunction(Grid[] points,
                        int startIndex, int endIndex, double epsilon) {
                double dmax = 0;
                int idx = 0;
                for (int i = startIndex + 1; i < endIndex; i++) {
                        double distance = Line2D.ptLineDist(points[startIndex].x, points[startIndex].y, points[endIndex].x, points[endIndex].y, points[i].x, points[i].y);
                        if (distance > dmax) {
                                idx = i;
                                dmax = distance;
                        }
                }
                if (dmax >= epsilon) {
                        Grid[] recursiveResult1 = ramerDouglasPeuckerFunction(points,
                                        startIndex, idx, epsilon);
                        Grid[] recursiveResult2 = ramerDouglasPeuckerFunction(points,
                                        idx, endIndex, epsilon);
                        Grid[] result = new Grid[(recursiveResult1.length - 1)
                                        + recursiveResult2.length];
                        System.arraycopy(recursiveResult1, 0, result, 0,
                                        recursiveResult1.length - 1);
                        System.arraycopy(recursiveResult2, 0, result,
                                        recursiveResult1.length - 1, recursiveResult2.length);
                        return result;
                } else {
                        return new Grid[] { points[startIndex], points[endIndex] };
                }
        }


}

