/**
 * 
 */

/**
 * @author alexandru
 *
 */
 
package plan.navigation;

public class RoundObstacle implements Obstacle{
	public Grid pos;
	public double radius;
	
	public RoundObstacle(Grid pos, double radius) {
		this.pos = pos;
        this.radius = radius;
	}
		
	public double distanceTo(Grid to) {
        double dist =  Math.sqrt(Math.pow((to.x - pos.x), 2)+ Math.pow((to.y - pos.y),2)) - radius;
        //yes the distance can be negative if the the tile entered the obstacle
        return dist;
	}
	
	public double distanceTo(Robot to) {
        double dist =  this.distanceTo(to.pos) - to.radius;
        //yes the distance cane be negative if the robot and the obstacle are overlapping
        return dist;
	}
	
	public String toString() {
        return "roundObstacle: (" + pos.x + ", " + pos.y + "), r = " + radius;
}
	
}
