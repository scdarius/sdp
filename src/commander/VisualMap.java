package commander;

import strategy.world.Robot;
import strategy.world.Tile;
import strategy.world.WorldState;

import javax.swing.*;
import java.awt.*;

/**
 * Java graphics actually does my head in.
 * Very, very start of the world drawer. Committed only because I'm going to labs and
 * may work on it there.
 */
public class VisualMap extends JPanel {
    double pitchWidth = WorldState.bottomRightCorner.x - WorldState.bottomLeftCorner.x;
    double pitchHeight = WorldState.bottomRightCorner.y - WorldState.topRightCorner.y;
    
    double panelWidth = 400;
    double panelHeight = 300;

    Point topLeftCorner = normalisePoint(WorldState.topLeftCorner);
    Point topRightCorner = normalisePoint(WorldState.topRightCorner);
    Point bottomLeftCorner = normalisePoint(WorldState.bottomLeftCorner);
    Point bottomRightCorner = normalisePoint(WorldState.bottomRightCorner);
    
    public VisualMap() {
       setSize((int)panelWidth, (int)panelHeight);


    }

    /**
     * Overrides paint. x + 10 so we can see the pitch a bit more clearly(just for now)
     * @param g
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.WHITE);
        g.fillRect(10, 0, (int)panelWidth, (int)panelHeight);
        g.setColor(Color.GREEN);
        // left pitch line
        g.drawLine(topLeftCorner.x + 10, topLeftCorner.y, bottomLeftCorner.x + 10, bottomLeftCorner.y);

        // top pitch line
        g.drawLine(topLeftCorner.x + 10, topLeftCorner.y, topRightCorner.x + 10, topRightCorner.y);

        // right pitch line
        g.drawLine(topRightCorner.x + 10, topRightCorner.y, bottomRightCorner.x + 10, bottomRightCorner.y);

        // bottom pitch line
        g.drawLine(bottomLeftCorner.x + 10, bottomLeftCorner.y, bottomRightCorner.x + 10, bottomRightCorner.y);

        Point myRobot = normalisePoint(WorldState.myRobot.getLocation());
        Point oppRobot = normalisePoint(WorldState.oppRobot.getLocation());
        Point ball =   normalisePoint(WorldState.ball.getLocation());

        // makes these an image later rather than a shitty oval :D
        if(myRobot != null) 
            g.drawRect(myRobot.x + 10, myRobot.y, 20, 20);
        
        if(oppRobot != null)
            g.drawRect(oppRobot.x + 10, oppRobot.y, 20, 20);
        
        if(ball != null) {
            g.drawOval(ball.x + 10, ball.y, 10, 10);
        }
    }
    
     /**
     * @param t to normalise
     * @return gui point we can draw it.
     */
    public Point normalisePoint(Tile t) {
        if(t == null) {
            return null;
        }
        int x =  (int)((t.x / pitchWidth) * panelWidth);
        int y = (int)((t.y / pitchHeight) * panelHeight);
        //System.out.println(t + " -> (" + x + ", " + y + ")");
        return new Point(x, y);
    }



}
