package commander;

import strategy.world.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.Polygon;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import strategy.world.*;



public class WorldPanel extends JPanel implements MouseListener{


    private Graphics2D g2d;

    double pitchWidth = WorldState.bottomRightCorner.x - WorldState.bottomLeftCorner.x;
    double pitchHeight = WorldState.bottomRightCorner.y - WorldState.topRightCorner.y;
    Tile[] rectangles;
    
    double panelWidth = 488;
    double panelHeight = 244;

    int arcCircRad = 0;
    int[] arcCircTile = null;

  	int[] pitchDim = {(int) panelWidth, (int) panelHeight};
  	int[] robotDim = {40,26};	  	
  	int ballDim = 8;
	

	public static Tile click;
	public static ArrayList<Tile> drawnPoints;
    public static ArrayList<Tile[]> drawnLines;
	public static String pathText;
	
    public WorldPanel(){
		super(new GridBagLayout());
		drawnPoints = new ArrayList<Tile>();
		drawnLines = new ArrayList<Tile[]>();
        Thread startWorldInfo = new Thread(){
                public void run() {
                    while (true) {
                        repaint();
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {
                            System.err.println("Exception: " + e.getMessage());
                        }
                    }
                }
        };
        startWorldInfo.start();
		this.addMouseListener(this);
    }

    public int[] normalisePoint(Tile t) {
        if(t == null)
            return null;
        int x =  (int)((t.x / pitchWidth) * panelWidth);
        int y = (int)((t.y / pitchHeight) * panelHeight);
        return new int[] {x, y};
    }



    public void paintComponent( Graphics g ) {  
			g2d = (Graphics2D)g;
		  
			//pitch
			g2d.setColor(Color.GREEN);
			g2d.drawRect(0, 0, pitchDim[0],pitchDim[1] ); 
			g2d.fillRect(0, 0, pitchDim[0],pitchDim[1]); 
			paintGoals(pitchDim);
			g2d.setColor(Color.white);
			g2d.drawRect(0,pitchDim[1],pitchDim[0],41);
			g2d.fillRect(0,pitchDim[1],pitchDim[0],41);
			if (WorldState.visionConnected){
				paintRobot("blue", WorldState.blueRobotLocation);
				paintRobot("yellow", WorldState.yellowRobotLocation);
				paintBall();
			}	
			if (click != null)
				paintPoint(click);
			if (drawnPoints != null)
				paintPoints(drawnPoints);
			if (drawnLines != null)
				paintLines(drawnLines);
			if(rectangles != null){
				paintRectangles(rectangles);
			}
    }


    private void paintBall() {
	  	int[] ballLoc = normalisePoint(WorldState.ball.getLocation());
		if(ballLoc[0] < panelWidth && ballLoc[1] <panelHeight){ 
		    g2d.setColor(Color.red);
		    g2d.drawOval(ballLoc[0] - ballDim/2,ballLoc[1] - ballDim/2,ballDim,ballDim);
		    g2d.fillOval(ballLoc[0] - ballDim/2,ballLoc[1] - ballDim/2,ballDim,ballDim);
		}
    }

    public void paintPoint(Tile tile) {
        int[] t = normalisePoint(tile);
        Color purple = new Color	(223, 0, 255);
	    g2d.setColor(purple);
	    g2d.drawOval(t[0],t[1],4,4);
	    g2d.fillOval(t[0],t[1],4,4);
    }

	/**
	 * Paints an ArrayList of tiles as points on the GUI
	 * @param tiles	ArrayList of tiles to be drawn
	 */
	private void paintPoints(ArrayList<Tile> tiles){
		for (int i = 0; i < tiles.size(); i++){
			paintPoint(tiles.get(i));
		}
	}
	private void paintLines(ArrayList<Tile[]> lines){
		for (int i = 0; i < lines.size(); i++){
			paintLine(lines.get(i)[0], lines.get(i)[1]);
		}
	}

    private void paintLine(Tile tile1, Tile tile2) {
		if (tile1 != null && tile2 != null) {
			int[] normal1 = normalisePoint(tile1);
			int[] normal2 = normalisePoint(tile2);
			g2d.setColor(Color.white);
			g2d.drawLine(normal1[0],normal1[1],normal2[0],normal2[1]);
		}
    }
	/**
	 * The set methods are all used to add:
	 * 1. Tiles representing single points
	 * 2. Arrays of tiles representing a group of points
	 * 3. Arrays of tiles representing a path
	 **/

	/**
	 * Stores the given tile to be painted on each repaint
	 * @param tiles Array of tiles to be painted
	 */
	public void setPoint(Tile tile){
		if (tile != null) drawnPoints.add(tile);
	}

	/**
	 * Stores the given array of tiles to be painted on each repaint
	 * @param tiles Array of tiles to be painted
	 */
	public void setPoints(Tile[] tiles){
		for (Tile tile : tiles) {
			setPoint(tile);
		}
	}

 	/**
	 * Stores the given array of tiles in a series of segmented lines
	 * @param tiles Array of tiles to be drawn as a path
	 */
	public void setPath(Tile[] tiles){
		setPoints(tiles);
		pathText = "Yes, there are two paths you can go by. But in the long run. \n There's still time to change the road you're on” Led Zeppelin.";
		Tile[] segment1 = {null, tiles[0]};
		if (WorldState.myRobot.getLocation() != null) segment1[0] = WorldState.myRobot.getLocation();
		drawnLines.add(segment1);
		for (int i = 0 ; i< tiles.length-1; i++){
			Tile[] segment = {tiles[i], tiles[i+1]};
			drawnLines.add(segment);
		}
	}

	public void setLine(Tile tile1, Tile tile2) {
		if (tile1 != null && tile2 != null) {
		        Tile[] segment = {tile1, tile2};
		        drawnLines.add(segment);
		}
	}

	private void paintRobot(String color, Tile t) {
		
	  	int[] robotLoc = normalisePoint(t);
		if(robotLoc[0] < panelWidth && robotLoc[1] < panelHeight){ 
		    double robotOrient;
			if (color.equals("blue")) {
		  	    robotOrient = WorldState.blueRobotAngle;
		        g2d.setColor(Color.blue);
		    } else {
		  	    robotOrient = WorldState.yellowRobotAngle;
		        g2d.setColor(Color.yellow);
		    }
			Rectangle robot = new Rectangle(robotLoc[0] - (robotDim[0] / 2), (robotLoc[1] - (robotDim[1] / 2)), robotDim[0], robotDim[1]);
		  	g2d.rotate(Math.toRadians(robotOrient),robotLoc[0],robotLoc[1]);
			g2d.draw(robot);
			g2d.fill(robot);
			if (color.equals("blue") && WorldState.isOurRobotBlue || (color.equals("yellow") && !WorldState.isOurRobotBlue)){
				Color lightPurple = new Color(255,100,255);
				g2d.setColor(lightPurple);
			} else{
				g2d.setColor(Color.orange);
			}
			int[] xs = { robotLoc[0], robotLoc[0]+20, robotLoc[0] };
			int[] ys = {robotLoc[1]-13, robotLoc[1], robotLoc[1]+13};
			Polygon triangle = new Polygon(xs, ys, xs.length);
			g2d.fillPolygon(triangle);
			g2d.rotate(Math.toRadians(-robotOrient),robotLoc[0],robotLoc[1]);
		}
    }

	private void paintGoals(int[] dim) {

		Rectangle rgoal = new Rectangle(dim[0]-5 ,(dim[1]/2) -60, 5,(int)120);
		Rectangle lgoal = new Rectangle((int)0 ,(dim[1]/2) -60, 5,(int)120);
		if((WorldState.isOurRobotBlue && WorldState.shootLeft) || (!WorldState.isOurRobotBlue && !WorldState.shootLeft)){
		g2d.setColor(Color.BLUE);
		g2d.draw(rgoal);
	    g2d.fill(rgoal);
		g2d.setColor(Color.YELLOW);
		g2d.draw(lgoal);
	    g2d.fill(lgoal);
		}else{
		g2d.setColor(Color.YELLOW);		
		g2d.draw(rgoal);
	    g2d.fill(rgoal);
		g2d.setColor(Color.BLUE);
		g2d.draw(lgoal);
	    g2d.fill(lgoal);	
		}
    }

	public void paintPathNotFound(){
		pathText = " I don't know what my path is yet. I'm just walking on it. Olivia Newton-John";
	}
	
	
	public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }
	
	 public void mouseEntered(MouseEvent e) {
       
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
		int[] clickPos = {e.getX(), e.getY()};
		if(clickPos[0] < panelWidth && clickPos[1] <panelHeight){  
			int x =  (int)((clickPos[0]/panelWidth )* pitchWidth);
			int y =  (int)((clickPos[1]/panelWidth )* pitchWidth);
			click = new Tile(x,y);
		}
    }

	public void clear(){
		click = null;
		drawnPoints.clear();
		drawnLines.clear();
		pathText = null;
	}

	public void setRectangles(Tile[] tiles){
			rectangles = tiles;
	}

	public void paintRectangles(Tile[] tiles){
		int[] point1;
		int[] point2;
		for(int i = 0 ; i < tiles.length - 1; i+=2){
			point1 = normalisePoint(tiles[i]);
			point2 = normalisePoint(tiles[i+1]);
			int width = Math.abs(point2[0] - point1[0]);
			int height = Math.abs(point2[1] - point1[1]);
			g2d.setColor(new Color(143, 20, 223, 100));
			g2d.fillRect(point1[0], point1[1], width, height);
		}
			
			
			
		
	}
	

}
