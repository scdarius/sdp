package commander;
import java.io.IOException;
import java.net.InetAddress;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import server.*;
import strategy.*;
import robot.BotComm;

public class ConsoleCommander {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		/**
		 * Start the bluetooth (or fake BT) server.
		 */
		ServerCommunication server = new RealServerCommunication();
                Bot bot = new Bot(server);

		System.out.println("I am alive!");
		InetAddress addr = InetAddress.getLocalHost();
		System.out.println("Host is: "+addr.getHostAddress());


                System.out.println("Send commands to the robot ('k' to kick, 'r' to roll across the pitch):");
                boolean quit = false;
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                while (!quit) {
                        System.out.print("> ");
                        String input = reader.readLine();
                        input = input.trim();

                        if (input.equals("q")) {
                                quit = true;
                        } else if (input.equals("k")) {
                                bot.kick();
                        } else if (input.equals("r")) {
                                bot.go();
	                	try { 
                                        Thread.sleep(5000);
                                } catch (Exception e) { }
                                bot.stop();
                        } else if (input.equals("d90")) {
                                bot.rotate(90);    
                        } else if (input.equals("d60")) {
                                bot.rotate(60);    
                        } else if (input.equals("d30")) {
                                bot.rotate(30);    
                        } else {
                                System.out.println("Unrecognised command");
                        }

                }
		server.close();
	}

}
