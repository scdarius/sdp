package commander;

import strategy.*;
import strategy.agents.Agent;
import strategy.agents.AgentEngine;
import strategy.agents.SubsumptionAgent;
import strategy.old.SingleClientServer;
import strategy.world.Tile;
import strategy.world.WorldState;
import server.*;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader; 
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Set;
import java.net.*;

/**
 * Unfinished atm, but it's the start of a new Controller GUI which should be a bit
 * more useful than the current.
 *** Addition of Combo box for scripts.
 * Tabs, one for development tools, one for in-game stuffs.
 * TODO: Sort out tabbedpane layout manager shite and why it's adding everything to one line and not changing the positions of stuff.
 */
public class ControllerGUI implements ActionListener {

    public static Bot bot;
    public static String infoFrom;
    //public static boolean runScript = true;
    public static Terminal terminal;
    static String[] connectionOptions = {"Real server", "Fake server", "Simulation server"};
    
    static ServerCommunication server;
    Thread scriptThread;
    Thread strategyThread;

    public JFrame mainFrame = new JFrame("Controller GUI");
    public static JTabbedPane tabbedPane;
    
    // Stuff for things such as kick and in game options.
    static JButton kickButton;
    JButton stopButton;
    JButton moveButton;
    JButton captureBackgroundButton;
    JButton startVisionButton;
    JCheckBox robotColor;
    JCheckBox robotGoal;
    JCheckBox mainPitch;
    JComboBox gameMode;
	JButton clearButton;

    String[] gameModes = {"Game mode", "Take penalty", "Defend penalty"};
    
    // Stuff for tools for debugging and testing.
    JButton stopScript;
    JButton startScript;
    JComboBox scriptList;
    JButton refreshScript;
    
    // panel for strategy
    JButton startStrategyButton;
    JButton stopStrategyButton;
    public static JTextArea strategyDisplay;

    public static VisualMap vMap;
    public static boolean attack = true;
   
    public boolean mainPitchVision = true;
    public String saveEmptyCommand = "python ../vision/saveEmpty.py";
    public String visionCommand = "python ../vision/vision.py";
    public String otherPitchArguments = " -p";
	public String parentpath;
	public URL urlList[];
    
	public JTextArea worldLog;
    public JTextArea drawLog;
 	public JTextArea visionLog;

    public static WorldPanel worldPanel;

	public static void main(String args[]) throws Exception {
		selectServer();
		bot = new Bot(server);
		terminal = new Terminal(bot);
		try {
			InetAddress addr = InetAddress.getLocalHost();
			System.out.println("Host is: "+addr.getHostAddress());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// start vision server.
		System.out.println("Getting socket...");
		Thread t = new Thread (new SingleClientServer(infoFrom));
		t.start();

         new Interpreter();
		// start GUI
        new ControllerGUI();
		
	        Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("");
				System.out.println("GUI shutting down");
				bot.shutdown(); // Tell the bot to close the socket
			}
	        });	
	}

    public ControllerGUI() {
        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Game mode", getGamePanel());
        tabbedPane.addTab("Development", getDevelopmentPanel());
        tabbedPane.addTab("World", getWorldStatePanel());
        tabbedPane.addTab("Draw World", getDrawWorldPanel());
	    tabbedPane.addTab("Vision Log", getVisionLogPanel());		
        mainFrame.add(tabbedPane, BorderLayout.CENTER);
        mainFrame.setSize(525, 460);
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    int num = 10;
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == startScript) {
            startScript(scriptList.getSelectedItem().toString());
        } else if(e.getSource() == stopScript) {
            stopScript();
        } else if(e.getSource() == stopButton) {
            if (ScriptEngine.runScript)
                stopScript();
            else
                ControllerGUI.bot.stop();
        } else if(e.getSource() == kickButton) {
            ControllerGUI.bot.kick();
        } else if(e.getSource() == moveButton) {
            ControllerGUI.bot.go();
        } else if(e.getSource() == mainPitch) {
            mainPitchVision = mainPitch.isSelected();
        } else if(e.getSource() == startStrategyButton) {
        	AgentEngine.runAgent = true;
        	Agent a = new SubsumptionAgent();
        	strategyThread = new Thread(new AgentEngine(a));
        	strategyThread.start();
        } else if(e.getSource() == stopStrategyButton) {
        	AgentEngine.runAgent = false;
        	strategyThread.stop();
        } else if(e.getSource() == robotColor) {
            WorldState.isOurRobotBlue = robotColor.isSelected();
        } else if(e.getSource() == robotGoal) {
            WorldState.shootLeft = robotGoal.isSelected();
            if(WorldState.shootLeft) {
                WorldState.oppGoal.updateLocation(new Tile(0, 61));
                WorldState.myGoal.updateLocation(new Tile(244, 61));
            } else {
                WorldState.myGoal.updateLocation(new Tile(0, 61));
                WorldState.oppGoal.updateLocation(new Tile(244, 61));
            }
        } else if(e.getSource() == gameMode) {
            if(gameMode.getSelectedItem().toString().contains("Take")) {
                WorldState.takePenalty = true;
                WorldState.defendPenalty = false;
            } else if(gameMode.getSelectedItem().toString().contains("Defend")) {
                WorldState.takePenalty = false;
                WorldState.defendPenalty = true;
            }
	} else if(e.getSource() == captureBackgroundButton){
		try{
                        String command;
                        if (mainPitchVision)
                                command = saveEmptyCommand;
                        else
                                command = saveEmptyCommand + otherPitchArguments;
                        System.out.println("Running command: " + command);
			Runtime.getRuntime().exec(command);
		}catch(IOException err){
			err.printStackTrace();
		}
        } else if(e.getSource() == startVisionButton) {
            Thread startVision = new Thread(){
                public void run() {
                    try{
                        String command;
                        if (mainPitchVision) 
                                command = visionCommand;
                        else
                                command = visionCommand + otherPitchArguments;
                        System.out.println("Running command: " + command);
                        Process proc = Runtime.getRuntime().exec(command);
                        BufferedReader bri = new BufferedReader( new InputStreamReader(proc.getInputStream()));
                        BufferedReader bre = new BufferedReader( new InputStreamReader(proc.getErrorStream()));
                        String line = null;
                        while (((line = bri.readLine()) != null) || ((line = bre.readLine()) != null)) {
                            while((line = bri.readLine()) != null){
                				String currentText = visionLog.getText();
			                	visionLog.setText(currentText + line + "\n");
                            }
                            while((line = bri.readLine()) != null){
                				String currentText = visionLog.getText();
			                	visionLog.setText(currentText + line + "\n");
                            }
                        }
                    } catch(IOException err){
                        err.printStackTrace();
                    }
                }
            };
            startVision.start();
            startVisionButton.setEnabled(true);
	    captureBackgroundButton.setEnabled(true);
        } else if (e.getSource() == clearButton) {
				worldPanel.clear();
		} else if(e.getSource() == refreshScript){
			String[] scriptNames = getScriptNames();
			scriptList.removeAllItems();
			for(String s:scriptNames){
				scriptList.addItem(s);
			} 
			scriptList.revalidate(); 
		}
    }
    
    private JPanel getGamePanel() {
        JPanel gamePanel = new JPanel();

        kickButton = new JButton("Kick");
        moveButton = new JButton("Move forward");
        stopButton = new JButton("Stop robot");
        startStrategyButton = new JButton("Start Strategy");
        stopStrategyButton = new JButton("Stop Strategy");
        startVisionButton = new JButton("Start Vision");
		captureBackgroundButton = new JButton("Capture Background");
        strategyDisplay = new JTextArea(15, 43);
        strategyDisplay.setBorder(new TitledBorder("Strategy log"));

        mainPitch = new JCheckBox("Main Pitch");
        mainPitch.setSelected(mainPitchVision);
        robotColor = new JCheckBox("Team color: Blue");
        robotColor.setSelected(WorldState.isOurRobotBlue);
        robotGoal = new JCheckBox("Shoot left");
        robotGoal.setSelected(WorldState.shootLeft);
        WorldState.oppGoal.updateLocation(new Tile(0, 61));
        WorldState.myGoal.updateLocation(new Tile(244, 61));

        gameMode = new JComboBox(gameModes);
        
        gamePanel.add(kickButton, BorderLayout.NORTH);
        gamePanel.add(moveButton, BorderLayout.NORTH);
        gamePanel.add(stopButton, BorderLayout.NORTH);
		gamePanel.add(captureBackgroundButton, BorderLayout.CENTER);
        gamePanel.add(startVisionButton, BorderLayout.CENTER);
        gamePanel.add(strategyDisplay, BorderLayout.SOUTH);
        
        gamePanel.add(mainPitch, BorderLayout.CENTER);
        gamePanel.add(robotColor, BorderLayout.CENTER);
        gamePanel.add(robotGoal, BorderLayout.CENTER);
        gamePanel.add(gameMode, BorderLayout.CENTER);
        
        gamePanel.add(startStrategyButton, BorderLayout.SOUTH);
        gamePanel.add(stopStrategyButton, BorderLayout.SOUTH);

        kickButton.addActionListener(this);
        startStrategyButton.addActionListener(this);
        stopStrategyButton.addActionListener(this);
        stopButton.addActionListener(this);
        moveButton.addActionListener(this);
		captureBackgroundButton.addActionListener(this);
        startVisionButton.addActionListener(this);
        
        mainPitch.addActionListener(this);
        robotColor.addActionListener(this);
        robotGoal.addActionListener(this);
        gameMode.addActionListener(this);
        
        return gamePanel;
    }
    
    private JPanel getDevelopmentPanel() {
        JPanel developmentPanel = new JPanel();
        scriptList = new JComboBox(getScriptNames());
        startScript = new JButton("Start script");
        stopScript = new JButton("Stop script");
        refreshScript = new JButton("Refresh script list");

        developmentPanel.add(terminal, BorderLayout.SOUTH);
        developmentPanel.add(scriptList, BorderLayout.NORTH);
        developmentPanel.add(startScript, BorderLayout.NORTH);
        developmentPanel.add(stopScript, BorderLayout.NORTH);
		developmentPanel.add(refreshScript, BorderLayout.NORTH);

        scriptList.addActionListener(this);
        startScript.addActionListener(this);
        stopScript.addActionListener(this);
        refreshScript.addActionListener(this);

        return developmentPanel;
    }

	private JPanel getVisionLogPanel() {
		JPanel visionPanel = new JPanel();
		visionLog = new JTextArea("", 15, 43);
		visionLog.setEditable(false);
		JScrollPane logPane = new JScrollPane(visionLog);
		logPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		visionPanel.add(logPane, BorderLayout.NORTH);
		return visionPanel;
	}

    /**
     * This panel will display all the world state information.
     * @return
     */
    private JPanel getWorldStatePanel() {
		JPanel worldStatePanel = new JPanel();
		worldLog = new JTextArea("", 15, 43);
        
		worldLog.setEditable(false);
		JScrollPane logPane = new JScrollPane(worldLog);
		logPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		worldStatePanel.add(logPane, BorderLayout.NORTH);

        Thread startWorldInfo = new Thread(){
                public void run() {
                    while (true) {
                        worldLog.setText(WorldState.worldToString());
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {
                            System.err.println("Exception: " + e.getMessage());
                        }
                    }
                }
        };
        startWorldInfo.start();
		return worldStatePanel;
	}

    private JPanel getDrawWorldPanel() {
        worldPanel = new WorldPanel();

		//The drawing log text area        
        drawLog = new JTextArea("", 2, 30);
		drawLog.setEditable(false);
        drawLog.setText("TEST");
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_END; //bottom of space
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        c.weighty = 1;
        c.weightx = 1;
		JScrollPane logPane = new JScrollPane(drawLog);
		logPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        worldPanel.add(logPane, c);


		 Thread startWorldInfo = new Thread(){
                public void run() {
                    while (true) {
                        drawLog.setText(WorldPanel.pathText);
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {
                            System.err.println("Exception: " + e.getMessage());
                        }
                    }
                }
        };
        startWorldInfo.start();
        //The clear button
		clearButton = new JButton("Clear Point");
   		clearButton.addActionListener(this);
        c.gridx = 1;
        c.weightx = 0.1;
		worldPanel.add(clearButton, c);

        return worldPanel;
    }

    public static void setPoint(Tile tile) {
        worldPanel.setPoint(tile);
    }
	public static void setPoints(Tile[] tiles) {
        worldPanel.setPoints(tiles);
    }
	public static void setPath(Tile[] tiles){
		worldPanel.setPath(tiles);

	}
	public static void paintPathNotFound(){
		worldPanel.paintPathNotFound();
	}

	public static void paintRect(Tile[] tiles){
		worldPanel.setRectangles(tiles);
	}
	public static void setLine(Tile tile1, Tile tile2){
		worldPanel.setLine(tile1, tile2);
	}
	public static void clear(){
		worldPanel.clear();
	}	
		
    /**
     * Get script names. This SHOULD be system independent, but needs checking.
     * TODO: Do this more cleanly
     */
    private String[] getScriptNames() {
		File scripts;
		String currentdir = System.getProperty("user.dir");
		File dir = new File(currentdir);
		parentpath = dir.getParent();
		File parentdir = new File(parentpath);
		ArrayList alist = new ArrayList();
		String[] stringarray = parentdir.list();
		for (int i = 0; i < stringarray.length; i++)
			alist.add(stringarray[i]);
		if(alist.contains("scripts")) {
			scripts = new File(parentpath + File.separator + "scripts");	
		} else {
			scripts = new File("src" + File.separator + "scripts");	
		}
		if(scripts != null) {
			String[] scriptNames = scripts.list();
			for(int i = 0; i < scriptNames.length; i++)
				scriptNames[i] = scriptNames[i].split("\\.")[0];
			java.util.Arrays.sort(scriptNames);
			try {
				URL url = new URL("file:" + File.separator + File.separator + parentpath + File.separator);
				urlList = new URL[1];
				urlList[0] = url;
			} catch(Exception e) {
				e.printStackTrace();
			}
			return scriptNames;
		} else
			System.out.println("Cannot find scripts");

		return null;

	}

	private void startScript(String scriptName) {
		try {
			Class script;
			ClassLoader loader = new URLClassLoader(urlList);
			script = loader.loadClass("scripts." + scriptName);
			ScriptEngine.runScript = true;
			Script s = (Script) script.newInstance();
			scriptThread = new Thread(new ScriptEngine(s, ""));
			scriptThread.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}


    private void startGame() {
    	try {
    		
    	
    	
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    static void selectServer() throws Exception {
        String input = (String) JOptionPane.showInputDialog(null, "Choose option", "Connection selection", JOptionPane.QUESTION_MESSAGE, null,
                connectionOptions, connectionOptions[0]);

        if(input.contains("Fake")){
            server = new FakeServerCommunication();
	    infoFrom = "vision";
	    }
            else if(input.contains("Real")){
                server = new RealServerCommunication();
	        infoFrom = "vision";
	    }
            else{ 
	       server = new SimulationServerCommunication();
	       infoFrom = "sim";
	    }
    }


    static void stopScript() {
            ScriptEngine.runScript = false;
            while (!ScriptEngine.executionFinished) {
                    try {
                            Thread.sleep(100);
                    } catch (Exception e) {
                            System.out.println("Waiting for script to finish executing...");
                    }
            }
            System.out.println("Script execution finished");
            bot.stop();
    }
} 
