package commander;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;

import strategy.AccessorMethods;

/**
 * Extends AccessorMethods so we can deal with the fact the class is abstract.
 * Currently only deals with AccessorMethod invocation rather than all classes (which is actually easier to do -.-).
 * Committed for now so if i'm in labs tomorrow can continue with it there.
 * TODO: Cleanup, now i've had time to play around with it.
 * TODO: Print results of methods in terminal.
 * TODO: integrate with terminal
 * TODO: make more generic
 * - Ashley
 */
public class Interpreter extends AccessorMethods {

    // change this later and use different keys.
    Hashtable<String, Method> accessorMethods;
    Hashtable<String, Field> accessorFields;
    Hashtable<String, Object> createdObjects; // objects we create in the interpreter for use later.

    public Interpreter() {
        accessorMethods = new Hashtable();
        accessorFields = new Hashtable();
        loadAccessorMethods();
        loadAccessorFields();
        listFields(this.getClass().getSuperclass());
        
        try {
            // just testing it works
            
            // took ages to figure out why the robot was kicking when started :-D -- Darius
            // Method m = (Method) accessorMethods.get("kick"); 
            
            //Object[] params = m.getParameterTypes();
            //m.invoke(this, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    // input = Class method (args)
    // i.e AM methodName args
    // i.e
    public void parseInput(String input) {
    	StringTokenizer st = new StringTokenizer(input);
    	System.out.println(input);
    	if(st.nextToken().equals("AM")) {
    		String invocation = st.nextToken();
    		String method = invocation.split("\\(")[0];
    		
    		try {
    			String args = invocation.split("\\(")[1].split("\\)")[0];
    			System.out.println("Args " + args);
    			if(args.contains("\\,"))
    				invokeAccessorMethod(method, args.split("\\,"));
    			else if(args.length() < 2) {
    				invokeAccessorMethod(method);
    			} else {
    				ControllerGUI.terminal.print("Invoking accessor method: " + method + " with args: " + invocation.split("\\(")[1].split("\\)")[0]);
    				invokeAccessorMethod(method, new String[] {args});
    			}
    			
    		}catch(Exception e) { 
    			ControllerGUI.terminal.print("Attempting to invoke method without parameters");
    			invokeAccessorMethod(method);
    		}	
    		
    	}
    }


    /**
     * As AccessorMethods is abstract, it needs to be handled differently. Hence the seperate
     * methods for it. I'll do a more Universal way of doing it once i've got the basics
     * ironed out.
     */
    private void loadAccessorMethods() {
        Method[] methods = this.getClass().getSuperclass().getDeclaredMethods();
        for (int i = 0; i < methods.length; i++)
            accessorMethods.put(methods[i].getName(), methods[i]);
    }

    /**
     * Load the fields from the AccessorMethods class.
     */
    private void loadAccessorFields() {
        Class accessorClass = this.getClass().getSuperclass();
        Field[] fields = accessorClass.getSuperclass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            accessorFields.put(fields[i].getName(), fields[i]);
            //System.out.println(fields[i].getName() + " Type: " + fields[i].getGenericType());
            
        }
    }

    /**
     * Currently handles methods without parameters
     */
    private void invokeAccessorMethod(String name, String[] parameters) {
        Method m = (Method) accessorMethods.get(name);
        try {
            m.invoke(this, getParams(parameters));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void invokeAccessorMethod(String name) {
    	System.out.println("Invoking " + name);
        Method m = (Method) accessorMethods.get(name);
        try {
            m.invoke(this, new Object[] { null });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listMethods(Class c) {
        Method[] methods = c.getClass().getSuperclass().getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            //System.out.println(methods[i].toString());
            print("" + methods[i].getName());
        }
    }

    private void listFields(Class c) {
        Field[] fields = c.getClass().getSuperclass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            System.out.println(fields[i].toString());
            print("" + fields[i].getName());
        }
    }
   
    private Object[] getParams(String[] params) {
    	ArrayList<Object> types = new ArrayList<Object>();
    	for(int i = 0; i < params.length; i++) {
    		types.add(getType(params[i]));
    	}
    	
    	Object[] array = new Object[types.size()];
    	return types.toArray(array);
    }

    /**
     * Returns an object type from a string.
     * @param param
     * @return type
     */
    private Object getType(String param) {
    	if(accessorFields.get(param) != null) {
    		return accessorFields.get(param); 
    	}
    	if(param.equals("")) 
    		return null;
        try {
            return Integer.valueOf(param);
        } catch (NumberFormatException nfe) {
        }

        try {
            return Double.valueOf(param);
        } catch (NumberFormatException nfe) {
        }

        if (param.equalsIgnoreCase("true")) {
            return Boolean.TRUE;
        } else if (param.equalsIgnoreCase("false")) {
            return Boolean.FALSE;
        }

        return param;
    }

    private Object[] getTypes(String[] params, int startIndex) {
        Object narrowed[] = new Object[params.length - 1];
        for (int i = 0; i < narrowed.length; i++) {
            narrowed[i] = getType(params[startIndex + 1]);
        }
        return narrowed;
    }

}
