package commander;
/**
 * Handles terminal/
 * TODO: reflection to handle other methods and implementation. Maybe put this in another tab?
 */


import javax.swing.*;
import scripts.*;
import server.*;
import strategy.Script;
import strategy.ScriptEngine;

import java.awt.event.*;
import java.awt.*;
import java.net.URL;
import java.io.File;
import java.net.*;

public class Terminal extends JPanel implements KeyListener, ActionListener {
	static ServerCommunication server;
	JTextArea terminalLog;
	JTextField terminalInput;
	JScrollPane scrollPane;
	Bot bot;
	Thread scriptThread;
	String lastCommand = "";
    boolean interactiveMode = false;
    Interpreter i;
    public URL[] urlList;
    public String parentpath;



    public Terminal(Bot bot) {
        this.setLayout(new BorderLayout());
		this.bot = bot;
		String currentdir = System.getProperty("user.dir");
		File dir = new File(currentdir);
		parentpath = dir.getParent();
		URL url;
		try{
		     url = new URL("file:" + File.separator + File.separator + parentpath + File.separator);
		     urlList = new URL[1];
		     urlList[0] = url;
		} catch(Exception e) {
			e.printStackTrace();
		}

		terminalLog = new JTextArea("Welcome to the terminal. /help for a list of commands \n", 15, 43);
		terminalInput = new JTextField(43);
		terminalLog.setEditable(false);

		scrollPane = new JScrollPane(terminalLog);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		terminalInput.addActionListener(this);
		terminalInput.addKeyListener(this);

		add(scrollPane, BorderLayout.NORTH);
		add(terminalInput, BorderLayout.SOUTH);
	}


	public void keyReleased(KeyEvent evt){ //required to override for abstract type. Doesnt actually do anything
		return;
	}

	public void keyTyped(KeyEvent evt){ //Same here
		return;
	}

	public void keyPressed(KeyEvent evt){

		if(evt.getKeyCode() == KeyEvent.VK_UP){
			terminalInput.setText(lastCommand);
		}
	}

	public void actionPerformed(ActionEvent evt) {
		String text = terminalInput.getText();
		lastCommand = text;
		processInput(text);
		terminalLog.setCaretPosition(terminalLog.getDocument().getLength());
	}

	/**
	 * Processes the input from the JTextField (where we type commands)
	 */
	public void processInput(String input) {
		if (input.startsWith("/")) {
			String command = input.split("/")[1];
			processCommand(command);
		} else {
			print("Incorrect command. Type /help for more information.");
		}
		terminalInput.setText("");
	}

	/**
	 * Processes the actual command.
	 */
	public void processCommand(String command) {
		if (command.equals("help")) {
			print("/run(ScriptName, param)");
            print("/i - Starts the reflection/interactive mode");
            print("/quit - quits interactive mode");

		} else if (command.contains("run")) {
			String scriptName = command.split("\\(")[1].split("\\,")[0];
            String scriptParam = command.split("\\,")[1].split("\\)")[0].trim();
			print("Starting " + scriptName + " with parameter " + scriptParam);
			try {
				Class script;
			    ClassLoader loader = new URLClassLoader(urlList);
			    script = loader.loadClass("scripts." + scriptName);
			    ScriptEngine.runScript = true;
				Script s = (Script) script.newInstance();
				scriptThread = new Thread(new ScriptEngine(s, scriptParam));
				scriptThread.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (command.equals("i")) {
			print("-- Starting interactive mode which currently just works with AccessorMethods ");
            interactiveMode = true;
            i = new Interpreter();
		} else if (command.equals("quit")) {
			interactiveMode = false;
		} else if(interactiveMode == true) {
			i.parseInput(command);
		} else {
			print("Not a command... Moron");
		}
	}

	public void print(String s) {
		terminalLog.append(s + "\n");
        terminalLog.setCaretPosition(terminalLog.getDocument().getLength());
	}

}
