package server;

import robot.BotComm;

public abstract class ServerCommunication {
    public int query(BotComm.Message message)
    {
        return query(message.code);
    }

	public int query(Integer message)
	{
		return BotComm.COMMAND_FAILURE;
	}

    public void queryNoResponse(BotComm.Message message)
    {
        queryNoResponse(message.code);
    }

	public void queryNoResponse(Integer message)
	{
	}

	public boolean sendInt(Integer message){
        return false;
	}

	public void close() {
		
	}
}
