package server;
import java.io.*;
import java.net.*;



/**
 * For sending commands to the robot in the webots simulator
 * @author Craig Innes/James McLaughlin/Chris Wait
 *
 */
public class SimulationServerCommunication extends ServerCommunication {
	Socket clientSocket;
	BufferedWriter out;
	BufferedReader in;
	int lastMaxSpeedCheck = 0;

	public SimulationServerCommunication(){
		//Opening a connection to the simulation server
		try{
		    	Socket clientSocket = new Socket("localhost" , 4562);
		    	out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
			in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		
		}catch(IOException ex){
			ex.printStackTrace();
		}
	
	}


    public int query(Integer message){
        try{
		System.out.println(message);
		if (message == 12) {
			if (lastMaxSpeedCheck==0) {
				while (true) {
					out.write(Integer.toString(message));
					out.newLine();
					out.flush();
					lastMaxSpeedCheck = Integer.parseInt(in.readLine());
					return lastMaxSpeedCheck;
				}
			} else return lastMaxSpeedCheck;
		} else {
			out.write(Integer.toString(message));
			out.newLine();
			out.flush();
			return 0;
		}	
        }catch(IOException ex){
            ex.printStackTrace();
	return -1;
        }
        
    }

public void close(){
	try{
		out.close();
		in.close();
		clientSocket.close();
    	}catch(IOException ex){
		ex.printStackTrace();
	}

	System.out.println("Connection closed");
    }
}
