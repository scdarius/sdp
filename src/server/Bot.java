package server;

import robot.BotComm;

public class Bot {
        ServerCommunication server;
        public final double DISTANCE_ADJUSTMENT = 1;//0.74;

        public Bot(ServerCommunication server)
        {
                this.server = server;
        }

        public void go()
        { server.query(BotComm.Message.GO); }

        public void reverse()
        { server.query(BotComm.Message.REVERSE); }

        public void stop()
        { server.query(BotComm.Message.STOP); }

        public void kick()
        { server.query(BotComm.Message.KICK); }

        public void floatMotors()
        { server.query(BotComm.Message.FLOAT); }

        public void accelerateOn()
        { server.query(BotComm.Message.ACCELERATE_ON); }

        public void accelerateOff()
        { server.query(BotComm.Message.ACCELERATE_OFF); }

        public void defend()
        { server.query(BotComm.Message.DEFEND); }

        public void travel(int value)
        {
                server.query(BotComm.Message.TRAVEL);
                server.query((int) Math.round(value * DISTANCE_ADJUSTMENT));
        }

        /*public void disableSpeedRegulation() 
        {
                server.query(BotComm.Message.DISABLE_SPEED_REGULATION);
        }

        public void enableSpeedRegulation() 
        {
                server.query(BotComm.Message.ENABLE_SPEED_REGULATION);
        }*/

        public void rotate(int degrees)
        {
				// Nasty hack to use the old rotation method if using the sim. Anyone have any better ideas?
		/*		if (server.getClass().getName().equals("server.SimulationServerCommunication")) {
		            server.query(BotComm.Message.ROTATE);
		            server.query(degrees);
				} else {
				    disableSpeedRegulation();
				    rotateAndReturn(degrees);
				    int checks = 0;
				    while (isMoving()) {
				        if (checks >= 10) { 
				          stop(); 
				          stop(); 
				          break;
				        }
				        System.out.println("Bot is rotating!");
				        try { Thread.sleep(50); } catch (Exception ex) { }
				        checks++;
				    }
				    enableSpeedRegulation();
				}
                */
		server.query(BotComm.Message.ROTATE);
		server.query(degrees);
        }
        public void rotateImmediate(int degrees)
        {
                server.query(BotComm.Message.ROTATE_IMMEDIATE);
                server.query(degrees);
        }

        public void setSpeed(int value)
        {
                server.query(BotComm.Message.SET_SPEED);
                server.query(value);
        }

        public void setTurnSpeed(int value)
        {
                //server.query(BotComm.Message.SET_TURN_SPEED);
                //server.query(value);
        }
        
        public void arc(int radius, int angle)
        {
                server.query(BotComm.Message.ARC);
                server.query((int) Math.round(radius * DISTANCE_ADJUSTMENT));
                server.query(angle);
        }

	public int getSpeedMax()
	{
		return server.query(BotComm.Message.GET_MOVE_SPEED_MAX);
		
	}

	public int getTurnSpeedMax()
	{
		//return server.query(BotComm.Message.GET_TURN_SPEED_MAX);
                return 40;
	}

	public boolean leftSensorPressed()
	{
		return server.query(BotComm.Message.GET_LEFT_SENSOR_STATUS) != 0;
	}

	public boolean rightSensorPressed()
	{
		return server.query(BotComm.Message.GET_RIGHT_SENSOR_STATUS) != 0;
	}

	public boolean isMoving(){
		return server.query(BotComm.Message.IS_MOVING) != 0;
	}

	public void steer(int turnRatio, int radius){
		server.query(BotComm.Message.STEER);
		server.query(turnRatio);
		server.query(radius);
	}

	public void shutdown()
	{
        server.queryNoResponse(BotComm.Message.QUIT);
	}
}
