package server;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.pc.comm.*;

/**
 * The BlueTooth server module. Can send command to the robot.
 * Create it, and it initializes connections. You then set the command
 * to send with setMessage(command), and send it with send().
 * @author shearn89
 *
 */
public class RealServerCommunication extends ServerCommunication {
	NXTConnector conn;
	DataInputStream dis;
	DataOutputStream dos;
	int m = 5;
	int m2 = 0;
	
	/**
	 * Constructor method, initializes connections.
	 */
	public RealServerCommunication() throws Exception {
		conn = connect();
		dos = new DataOutputStream(conn.getOutputStream());
		dis = new DataInputStream(conn.getInputStream());
	}
	/**
	 * Connect method, initializes bluetooth connection to the NXT.
	 * @return The connection object.
	 */
	private NXTConnector connect() throws Exception {
	        //conn = NXTCommFactory.createNXTComm(NXTCommFactory.BLUETOOTH);
	        //boolean connected = conn.open(new NXTInfo(NXTCommFactory.BLUETOOTH, "Group1", "00:16:53:08:DC:7F"));
                //NXTCommConnector connector = Bluetooth.getConnector();
                //RemoteNXT nxt = new RemoteNXT("Group1", connector);

		NXTConnector conn = new NXTConnector();
		
        System.out.println("Connecting to Stewie...");
		boolean connected = conn.connectTo("btspp://NXT");
			
		if (!connected) {
			System.err.println("Failed to connect to any NXT");
			System.exit(1);
			return null;
		} else {
			System.out.println("Connection initialized. Good morning, Dave... \n");
			return conn;
		}
	}
	
	/**
	 * The public sender method. Sends commands and returns output
	 * @param message The command to send, 1=go, 2=stop, 3=travel (with arg),
	 * 4=rotate (with arg), 5=kick.
	 */
	public int query(Integer message){
		try {
			System.err.println("sending: " + message);
			int out = sendreceive(dis, dos, message);
			System.err.println("received: " + out);
			return out;
		} catch (Exception e){
			System.err.println("Sending failed, Exception received: ");
			System.err.println(e.getMessage());
			e.printStackTrace();
			// close(); // Don't close the socket and hope things are ok
			return -1; // TODO: throw exception!
		}
	}
	
	public void queryNoResponse(Integer message){
		try {
			System.err.println("sending: " + message);
			send(dos, message);
		} catch (Exception e){
			System.err.println("Sending failed, Exception received: ");
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Sends and receives data, checking for whether the robot processed everything correctly.
	 * @param dataIn The DataInputStream from the bluetooth connection.
	 * @param dataOut The DataOutputStream from the bluetooth connection.
	 * @param message The integer string to send to the robot.
	 * @return A boolean value for okay or not: true if all went okay and the robot is waiting
	 * for another command, and false if it closed.
	 * @throws IOException if something went wrong.
	 */
	private int sendreceive(DataInputStream dataIn, DataOutputStream dataOut, int message) throws IOException{
		dataOut.writeInt(message);
		dataOut.flush();
		int code = dataIn.readInt();
		// System.err.println("receive code is: "+code);
		return code;
	}
	
	private void send(DataOutputStream dataOut, int message) throws IOException{
		dataOut.writeInt(message);
		dataOut.flush();
	}
	
	/**
	 * Closes the data connection, required if the robot is still waiting for a command.
	 * @param dataIn The DataInputStream from the bluetooth connection.
	 * @param dataOut The DataOutputStream from the bluetooth connection.
	 */
	public void close(){
		try {
			dis.close();
			dos.write(-1);
			dos.flush();
			dos.close();
			conn.close();
		} catch (IOException e){
			System.err.println(e);
		}
	}
}
