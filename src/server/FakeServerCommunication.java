package server;
import java.io.*;
import java.net.*;



/**                                                                                                                                                     
 * Server for communicating with the simulator                                                                                                          
 * @author James/Chris                                                                                                                                  
 *                                                                                                                                                      
 */
public class FakeServerCommunication extends ServerCommunication {

        public int query(Integer message) {
        //System.out.println("Sending message: " + Integer.toString(message));
	//System.out.println("Its not real though");
                return 0;
        }

        public void close(){
                System.out.println("Connection closed");
        }
}

