Deep Thought (Group 1) : System Design Project 2012
===========================================================

## Members:

* Sean Bowie
* Boris Hristov 
* Craig Innes
* Alexandru Ionita 
* Altynbek Kalitanov 
* Andrew McCrae  
* Jack McKay Fletcher
* James McLaughlin 
* Ashley Peacock 
* Darius Scerbavicius
* Chris Wait

Mentor: Oliver Wilson

## Usage

1. Grab a terminal.
2. Compile and link all the Java packages: `ant`
3. Upload the program to the robot: `./upload.sh`
