SDP_DIR=/group/teaching/sdp/sdp1

export JAVA_HOME=/usr/lib/jvm/java-openjdk

export NXJ_HOME=$SDP_DIR/lejos_nxj_091
export PATH=$PATH:$JAVA_HOME/bin:$NXJ_HOME/bin  

export PYTHONPATH=$PYTHONPATH:$SDP_DIR/libraries/lib/python2.6/site-packages:$SDP_DIR/libraries/lib64/python2.6/site-packages
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SDP_DIR/bluez/lib:$SDP_DIR/libraries/lib
