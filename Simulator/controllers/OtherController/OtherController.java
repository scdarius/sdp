import java.util.*;
import java.io.*;
import java.net.*;
import com.cyberbotics.webots.controller.*;

public class OtherController extends DifferentialWheels {



	private GPS theGPS;
	private Compass theCompass;
	private Servo kicker;
	private Receiver receiver;
  private TouchSensor leftTouchSensor;
  private TouchSensor rightTouchSensor;
  
  
	public final int timeStep = 32;
	private final double pitchLength = 2.438;
	private final double pitchHeight = 1.219;
	private final double robotLength = 0.5;
	private final double resolution = 10000.0;
	private final double turningRadius = 0.135878;
  
	private ArrayList<Integer> currentCommand;
	private int numArgs;
	private ArrayList<Integer> commandArgs;
	//    private final double wheelRadius = 0.031;
	private final double wheelRadius = 3.1;
	private final double turningSpeed = 50.0;
	private final double maxSpeed = 50.0;
	private double speed = 50.0;
	int kicked = 0;
	private static ServerSocket servSocket;
	private static Socket s;
	private static BufferedReader r;
	//private final String[] status = {"WAITING!","MOVING!","STOPPING!","KICKING!"};
  
	public void activate() {
		theGPS = getGPS("gps");
		theGPS.enable(timeStep);
		theCompass = getCompass("compass");
		theCompass.enable(timeStep);
		kicker = getServo("kicker");
		kicker.enablePosition(timeStep);
		enableEncoders(timeStep);    
		currentCommand = new ArrayList<Integer>();
		currentCommand.add(0);
    leftTouchSensor = getTouchSensor("leftTouchSensor");
    leftTouchSensor.enable(timeStep);
    rightTouchSensor = getTouchSensor("rightTouchSensor");
    rightTouchSensor.enable(timeStep);
    
	}

	public void startServer() {
		int commandNum;
		try {
			while(true) {
				commandNum = Integer.parseInt(r.readLine()); 
				//System.out.println("Received command: " + commandNum);

				switch(commandNum){
				case -1:
					numArgs = 0;
					break;
				case 1:
					numArgs = 0; //go
					break;
				case 2:
					numArgs = 0; //stop
					break;
				case 3: 
					numArgs = 0; //kick
					break;
				case 5: numArgs = 1; //set speed
				break;
				case 6: numArgs = 1; //rotate x degrees
				break;
				case 10: numArgs = 1; //move forward x amount
				break;
				case 12: numArgs = 0; //get max speed
				break;
				}

				//Reads in appropriate amount of arguments for current command

				ArrayList<Integer> tempCommand = new ArrayList<Integer>();
				tempCommand.add(commandNum);
				int newArg;
				for(int i = 0; i < numArgs; i++){
					newArg = Integer.parseInt(r.readLine());
					//System.out.println("Recieved new argument: " + newArg);
					tempCommand.add(newArg);
				}

				//Since we are using threads, it is important that all the arguments are given at the exact same time		
				currentCommand = (ArrayList<Integer>)tempCommand.clone();
				try{
					Thread.sleep(10); //So server doesnt read in two commands before robot has had a chance to execute first
				}catch(Exception ex){
					ex.printStackTrace();
				}


			}
		} catch (IOException e) {   

			System.err.println(e.toString());   
		}
	}



	public void commands(){

		while(true) {
			//System.out.println(getCompassBearing()  );
			switch (currentCommand.get(0)){
			case -1:
				break;
			case 1:
				System.out.println("MOVING!!");
				go();
				break;
			case 2:
				System.out.println("STOPPING!!");
				stop();
				currentCommand.set(0,0);
				break;
			case 3:
				System.out.println("KICKING!!");
				kick(); 
				break;
			case 5:
				System.out.println("Setting new speed: " + currentCommand.get(1));
				setRobotSpeed(currentCommand.get(1));
				break;
			case 6:
				//System.out.println("Rotating " + currentCommand.get(1) + " degrees");
				//turnToBearing(currentCommand.get(1));
				rotate(currentCommand.get(1)); //our "turn to" function needs to be modified somewhat to accomodate this
				break;
			case 10:
				System.out.print("Travelling distance of: " + currentCommand.get(1));
				travel(currentCommand.get(1));
				break;
			case 12:
				System.out.println("Finding max move speed");
				int thingToFind = getMaxSpeed(); //This is nonsense filler, need to fix
				break;

			}if (step(timeStep) == -1) break;
		}
	}

	public int getMaxSpeed(){
		return (int) maxSpeed; //doesnt really do anything, need a way to communicate this back to original strategy script that asked for it
	}

	public void go(){
		while(currentCommand.get(0) == 1) {
			setSpeed(speed,speed);
			if (step(timeStep) == -1) break;
		}   
	}
	public void stop() {
		setSpeed(0.0, 0.0);
	}
	public void setRobotSpeed(int sp){
		speed = sp;
		System.out.println("Speed changed to: " + speed);
	}



	public void kick() {

		while(true) {

			if (kicked == 0 && (kicker.getPosition() > -0.7)) {
				kicker.setPosition(-0.7);
				kicked = 1;
			}
			if (kicker.getPosition() < -0.7 && kicked == 1) {
				kicker.setPosition(0);
				currentCommand.set(0,0);
				kicked = 0;
				break;
			}
			if (step(timeStep) == -1) break;
		}
	}




	public void reverse() {
		while(true) {
			setSpeed(-speed,-speed);
			if (step(timeStep) == -1) break;
		}    
	}
  
  public void react() {
  while(true) {
    if (leftTouchSensor.getValue() == 1) {
    //System.out.println(leftSensor.getValue());
    System.out.println("touched left");
    travel(-10);
    }
    if (rightTouchSensor.getValue() == 1)  {
    System.out.println("touched right");
    travel(-10);
    //System.out.println(rightSensor.getValue());
    }
    if (step(timeStep) == -1) break;
  } 
  }

	public void travel(double distance) {
		double currentEncoder = getLeftEncoder();
		double encoderDifference = ((resolution * distance) / wheelRadius) + currentEncoder;
		if (distance < 0) {
			setSpeed(-speed, -speed);
			while (getLeftEncoder() > encoderDifference) {
				if (step(timeStep) == -1) break;
			}
		} else {
			setSpeed(speed,speed);
			while (getLeftEncoder() < encoderDifference) {
				if (step(timeStep) == -1) break;
			}
		}
		stop();
	}


	public double getCompassBearing() {
		double x = theCompass.getValues()[0];
		double y = theCompass.getValues()[2];
		double angle = Math.toDegrees(Math.atan2(y,x)) + 180;
		return angle;
	}

	public double[] getGPSPosition() {    
		double x = theGPS.getValues()[0]+ pitchLength/2;
		double y = theGPS.getValues()[2]+ pitchHeight/2;
		double[] gps = {x,y};
		return gps;
	}

	public void turnToBearing(double newBearing) {
		double currentBearing = getCompassBearing();
		double angle = (((360 - currentBearing) + newBearing) % 360);
		if (angle > 180) {
			angle = angle - 360;	
		}
		if (angle < 0) {
			setSpeed(turningSpeed,-turningSpeed);
		} else if (angle > 0) {
			setSpeed(-turningSpeed,turningSpeed);
		}
		while (Math.abs(getCompassBearing() - Math.abs(newBearing)) > 1) {
			if (step(timeStep) == -1) break;
		}
		stop();
	}

	public void rotate(double byAngle) {
		double currentBearing = getCompassBearing();


		if (byAngle > 0) {

			double newBearing = (currentBearing + byAngle);
			if (newBearing < 360) { 
				setSpeed(-turningSpeed,turningSpeed);

				while ((Math.abs(newBearing) > getCompassBearing())) {


					if (step(timeStep) == -1) break;
				}
			} else {
				newBearing = newBearing % 360;
				setSpeed(-turningSpeed,turningSpeed);

				while (Math.abs(getCompassBearing() - Math.abs(newBearing)) > 1) {

					if (step(timeStep) == -1) break;
				}

			}

		} else if (byAngle < 0) {
			double newBearing = (currentBearing + byAngle);
			if (newBearing > 0) { 
				setSpeed(turningSpeed,-turningSpeed);

				while ((Math.abs(newBearing) < getCompassBearing())) {


					if (step(timeStep) == -1) break;
				}
			} else {
				newBearing = 360 + newBearing;

				setSpeed(turningSpeed,-turningSpeed);

				while (Math.abs(getCompassBearing() - Math.abs(newBearing)) > 1) {

					if (step(timeStep) == -1) break;
				}

			}
		}

		stop();

	}

	public static void main() {
		final OtherController bot = new OtherController();

		bot.activate();


		/*try{
			System.out.println("Connecting to Strategy");
			servSocket = new ServerSocket(4562);
			s = servSocket.accept();
			r = new BufferedReader(new InputStreamReader(s.getInputStream()));
			System.out.println("Connected. Waiting for instructions...");
		}catch(IOException ex){
			ex.printStackTrace();
		}*/


		/*Thread serverThread = new Thread() {
			public void run() {

				//bot.startServer();


			}
		};*/
    
    Thread reactThread = new Thread() {
			public void run() {

				bot.react();


			}
		};

    reactThread.start();
		//serverThread.start();
		bot.commands();

	}
}
