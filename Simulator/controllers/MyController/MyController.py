from controller import *
import math
import string
pitchLength = 2.438
pitchHeight = 1.219
robotLength = 0.5
resolution = 10000
turningRadius = 0.135878
wheelRadius = 0.031
turningSpeed = 50
speed = 110

class point():
	x = 0.0
	y = 0.0
	
	def __init__(self, x, y):
		self.x = x
		self.y = y
  
	def distanceToPoint(self, point):
		xDifference = self.x - point.x
		yDifference = self.y - point.y
		return math.sqrt(math.pow(xDifference,2)+math.pow(yDifference,2))

	def toString(self):
		selfString = str(self.x) + "," + str(self.y)
		return selfString
  
	def bearingToPoint(self, point):
		xDifference = self.x - point.x
		yDifference = self.y - point.y
		bearing = math.degrees(math.atan2(xDifference,yDifference))
		if bearing < 0:
			bearing += 360
		return bearing


class robot (DifferentialWheels):
	def activate(self,):
		global speed
		global turningSpeed
		self.turningSpeed = turningSpeed
		self.speed = speed
		self.theGPS = self.getGPS('gps')
		self.theGPS.enable(32)
		self.enableEncoders(960)
		self.leftEncoder = self.getLeftEncoder()
		self.rightEncoder = self.getRightEncoder()
		self.theCompass = self.getCompass('compass')
		self.theCompass.enable(32)
		self.theReceiver = self.getReceiver("receiver")
		self.theReceiver.enable(32)
		self.theServo = self.getServo("kicker")
        	self.theServo.enablePosition(32)
		#Skip the first gps packet from the ball

	def stop(self):
		self.setSpeed(0,0)

	def turnToBearing(self, newBearing):
		currentBearing = self.getCompassBearing()
		angle = (((360 - currentBearing) + newBearing) % 360)
		if angle>180:
			angle -= 360
		if angle < 0:
			self.setSpeed(-self.turningSpeed,self.turningSpeed)
		elif angle > 0:
			self.setSpeed(self.turningSpeed,-self.turningSpeed)
		while abs(self.getCompassBearing() - abs(newBearing)) > 1:
			if self.step(32) == -1: break
		self.stop()

	def goToPoint(self, point):
		currentLocation = self.getGPSPosition()
		bearing = currentLocation.bearingToPoint(point)
		distance = currentLocation.distanceToPoint(point)
		self.turnToBearing(bearing)
		while distance > 0.25:
			self.moveForward(0.01)
			lastDistance = distance
			currentLocation = self.getGPSPosition()
			distance = currentLocation.distanceToPoint(point)
			if self.step(64) == -1: break
			if abs(lastDistance - distance) < 0.01:
				print "FUCK"
				self.moveBackwards(0.25)
				break
		goalBearing = self.calculateGoalBearing(point)
		self.turnToBearing(goalBearing)
		self.theServo.setPosition(-0.7)
		self.stop()

	def distanceToPoint(self, point):
		return self.getGPSPosition().distanceToPoint(point)

	def getGPSPosition(self):
		global pitchLength
		global pitchHeight
		x = self.theGPS.getValues()[0] + pitchLength/2
		y = -self.theGPS.getValues()[2] + pitchHeight/2
		return point(x,y)

	def getCompassBearing(self):
		x = self.theCompass.getValues()[0]
		y = self.theCompass.getValues()[2]
		angle = math.degrees(math.atan2(x,y)) + 180
		return angle

	def getEncoderValues(self):
		return self.getLeftEncoder(),self.getRightEncoder()

	def moveForward(self, distance):
		global wheelRadius, resolution
		currentEncoding = self.getLeftEncoder()
		encoderDifference = ((resolution * distance) / wheelRadius) + currentEncoding
		self.setSpeed(self.speed,self.speed)
		while self.getLeftEncoder() < encoderDifference:
			if self.step(64) == -1: break
		self.stop()
			
	def moveBackwards(self, distance):
		global wheelRadius, resolution
		currentEncoding = self.getLeftEncoder()
		encoderDifference = ((resolution * distance) / wheelRadius) - currentEncoding
		self.setSpeed(-self.speed,-self.speed)
		while self.getLeftEncoder() < encoderDifference:
			if self.step(64) == -1: break
		self.stop()

	def calculateGoalBearing(self, ballPoint):
		theirGoalCenter = point(2.41238121476,0.6037403164999999)
		return ballPoint.bearingToPoint(theirGoalCenter)

	def followBall(self):
		global pitchLength
		global robotLength
		while True:
			if (self.theReceiver.getQueueLength() != 0):
				self.theReceiver.nextPacket()
				message = self.theReceiver.getData()
				ballLocation = message.split(",")
				ballLocation = point(float(ballLocation[0]),float(ballLocation[1]))
				self.goToPoint(ballLocation)
			if self.step(64) == -1: break

	def goToBall(self):
		global pitchLength
		global robotLength
		while True:
			if (self.theReceiver.getQueueLength() == 2):
				self.theReceiver.nextPacket()
				message = self.theReceiver.getData()
				ballLocation = message.split(",")
				ballLocation = point(float(ballLocation[0]),float(ballLocation[1]))
				self.goToPoint(ballLocation)
				goalBearing = self.calculateGoalBearing(ballLocation)
				self.turnToBearing(goalBearing)
				self.theServo.setPosition(-0.7)
				break
			if self.step(64) == -1: break

robot = robot()
robot.activate()
#robot.followBall()
robot.goToBall()

