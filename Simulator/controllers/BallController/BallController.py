from controller import *
import math
import struct
pitchLength = 2.438
pitchHeight = 1.219
ourGoal = [0.0264638,0.30644487,0.901035763]
theirGoal = [2.41238121476,0.30644487,0.901035763]

class point():
	x = 0.0
	y = 0.0
	
	def __init__(self, x, y):
		self.x = x
		self.y = y
  
	def distanceToPoint(self, point):
		xDifference = self.x - point.x
		yDifference = self.y - point.y
		return math.sqrt(math.pow(xDifference,2)+math.pow(yDifference,2))
		
	def bearingToPoint(self, point):
		xDifference = self.x - point.x
		yDifference = self.y - point.y
		bearing = math.degrees(math.atan2(xDifference,yDifference))
		if bearing < 0:
			bearing += 360
		return bearing
    
	def toString(self):
		selfString = str(self.x) + "," + str(self.y)
		return selfString

class ball (Robot):
  def activate(self):
      self.theGPS = self.getGPS('gpsBall')
      self.theGPS.enable(32)
      self.theEmitter = self.getEmitter("emitterBall")
      
  def getGPSPosition(self):
        global pitchLength
        global pitchHeight
        x = self.theGPS.getValues()[0] + pitchLength/2
        y = self.theGPS.getValues()[2] + pitchHeight/2
        return point(x,y)  
        
        
  def run(self):
      global pitchLength
      global robotLength
      global ourGoal
        
      while True:
        currentPosition = self.getGPSPosition()
        message =  str(currentPosition.x) + ", " + str(currentPosition.y)
        self.theEmitter.send(message)
        #print "Ball Sending: " + message
        x = self.getGPSPosition().x
        y = self.getGPSPosition().y
        #if x < ourGoal[0] and ourGoal[1] < y < ourGoal[2]:
          #print "GOOOOOAAALLLL"
        #if x > theirGoal[0] and theirGoal[1] < y < theirGoal[2]:
          #print "SHIT"
        if self.step(32) == -1: break

ball = ball()
ball.activate()
ball.run()
