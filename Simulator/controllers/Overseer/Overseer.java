import java.util.*;
import java.io.*;
import java.net.*;
import com.cyberbotics.webots.controller.*;


// Here is the main class of your controller.
// This class defines how to initialize and how to run your controller.
// Note that this class derives Robot and so inherits all its functions
public class Overseer extends Supervisor {
  private final int timeStep = 32;
  private final double tDistance = 8;
  Socket c;
  BufferedWriter outToServer;
  
  Node theRobot;
  Field RobotFieldRef;
  double[] robotPos;
  
  Field RobotBearingRef;
  double[] robotBearing;
  
  Node otherRobot;
  Field otherRobotFieldRef;
  double[] otherRobotPos;
  
  Field otherRobotBearingRef;
  double[] otherRobotBearing;
  
  Node theBall;
  Field BallFieldRef;
  double[] ballPos;
  


  public Overseer(){
  	super();
	 //Gets essentially a pointer to each of the objects (No need for emmiters/receivers!)
	 theRobot = getFromDef("VACU1");
   otherRobot = getFromDef("VACU2");
	 theBall = getFromDef("ball");
   
  }

  	
  public void activate() {
  step(timeStep);
    step(timeStep);
     step(timeStep);
  	try{
		c = new Socket("localhost", 4561);
		outToServer = new BufferedWriter(new OutputStreamWriter(c.getOutputStream()));
		System.out.println("Simulator Vision is also connected");
    System.out.println("");
    System.out.println("To move robot or ball: Hold down Shift key, and Left click on object.");
    System.out.println("");
    System.out.println("To rotate robot or ball: Hold down Shift key, and Right click on object.");
	}catch(IOException ex){
    
		ex.printStackTrace();
	}

  }
  
  

  
  public double[] getDirection(double angleInRad, double robotX, double robotY) {
  
  double angleInDeg = Math.toDegrees(angleInRad)+90;
  double bearing = angleInDeg % 360;
      if (bearing < 0)
      {
        bearing += 360;
        }
      
  bearing = Math.abs(bearing - 360);
  
  double newx = robotX + (tDistance * (Math.cos(Math.toRadians(bearing)))); 
  double newy = robotY + (tDistance * (Math.sin(Math.toRadians(bearing))));
  
  double[] dir = {newx,newy};
  return dir;
  }
  
  
  
  public void run() {
  	String message;
	    
	while(true) {
		//Extracts the specific field of that object that you want the values of
		RobotFieldRef = theRobot.getField("translation");
		robotPos = RobotFieldRef.getSFVec3f(); //Field comes out encapsulated, have to extract numerical values
    
    RobotBearingRef = theRobot.getField("rotation");
    robotBearing = RobotBearingRef.getSFRotation();
    
    otherRobotFieldRef = otherRobot.getField("translation");
		otherRobotPos = otherRobotFieldRef.getSFVec3f(); 
    
    otherRobotBearingRef = otherRobot.getField("rotation");
    otherRobotBearing = otherRobotBearingRef.getSFRotation();
    
    BallFieldRef = theBall.getField("translation");
		ballPos = BallFieldRef.getSFVec3f();
    
    
    
    //Normalise coords: translate so it starts at zero
    //Then scale so its in correct range
    robotPos[0] = (robotPos[0] + 1.219) * 100;
    robotPos[2] = (robotPos[2] + 0.6095) * 100;
    double[] robotDirection = getDirection(robotBearing[3],robotPos[0],robotPos[2]);
    
    otherRobotPos[0] = (otherRobotPos[0] + 1.219) * 100;
    otherRobotPos[2] = (otherRobotPos[2] + 0.6095) * 100;
    double[] otherRobotDirection = getDirection(otherRobotBearing[3],otherRobotPos[0],otherRobotPos[2]);

    ballPos[0] = (ballPos[0] + 1.219) * 100;
    ballPos[2] = (ballPos[2] + 0.6095) * 100;
    
    
    
    
    
    
    //double y = ((Math.cos(Math.toRadians(r)))+ 0.6095) * 100;
    //double x = ((Math.sin(Math.toRadians(r)))+ 1.219) * 100;
    
    
    

		try{
			message = ballPos[0] + "," + ballPos[2] + ";"
      + robotPos[0] + "," + robotPos[2] + ";"
      + robotDirection[0] + "," + robotDirection[1] + ";" 
      + otherRobotPos[0] + "," + otherRobotPos[2] + ";" 
      + otherRobotDirection[0] + "," + otherRobotDirection[1] + ";" 
      + "0" + "," + "60.95" 
      + ";" + "244" + "," + "60.95" + ";";
     
			outToServer.write(message);
      outToServer.newLine();
			outToServer.flush();
		}catch(IOException ex){
    System.out.println("Webots has lost connection, please reconnect by clicking on the refresh button");
			System.exit(0);
		}
  
		if (step(timeStep) == -1) break;
	  }
  }

  public static void main(){
	Overseer superv = new Overseer();

    superv.activate();
    superv.run();
    
  }
  

}